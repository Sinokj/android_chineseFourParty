package cn.sinokj.party.chineseFourParty.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.IndexImageWebActivity;
import cn.sinokj.party.chineseFourParty.adapter.ArticleListAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.ArticleBean;
import cn.sinokj.party.chineseFourParty.fragment.base.BaseFragment;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;
import cn.sinokj.party.chineseFourParty.view.xlist.XListView;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * Created by l on 2018/5/4.
 */

public class ArticleFragment extends BaseFragment {
    public static final int TO_CHANGE_READ_STATUS = 3;
    private static final int REFRESH = 0;
    private static final int INIT_DATA = 1;
    private static final int LOAD_MORE = 2;
    private static final int QUERY_SIZE = 10;
    @BindView(R.id.xlist_listview)
    XListView xlistListview;
    Unbinder unbinder;
    private String vcType;
    private ArticleBean.ObjectsBean article;
    private ArticleListAdapter articleAdapter;
    private ArrayList<ArticleBean.ObjectsBean> dataList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        unbinder = ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        xlistListview.setXListViewListener(ixListViewListener);
        initData();
    }

    private void initData() {
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        if (getUserVisibleHint()) {
            if (what == TO_CHANGE_READ_STATUS) {
                if (article != null) {
                    Log.e("article.nId", "article.nId" + article.nId);
                    return HttpDataService.getArticleReadStatus(String.valueOf(article.nId));
                } else {
                    return null;
                }
            }
        }
        return HttpDataService.getPartyArticleList(App.nCommitteeId, vcType);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        if (msg.what == TO_CHANGE_READ_STATUS) {
            article.nStatus = (jsonObject.optInt(HttpConstants.RESULT));
            articleAdapter.notifyDataSetChanged();
            return;
        }
        String json = jsonObject.toString();
        ArticleBean articleBean = new Gson().fromJson(json, ArticleBean.class);
        List<ArticleBean.ObjectsBean> list = articleBean.objects;
        switch (msg.what) {
            case REFRESH:
                xlistListview.stopRefresh();
            case INIT_DATA:
                if (articleBean.objects.size() == 0 && getUserVisibleHint()) {
                    Toast.makeText(getActivity(), "没有查到相关信息", Toast.LENGTH_SHORT).show();
                    return;
                }
                dataList = new ArrayList<>();
                dataList.addAll(list);
                articleAdapter = new ArticleListAdapter(getActivity(), dataList);
                xlistListview.setOnItemClickListener(onItemClickListener);
                xlistListview.setAdapter(articleAdapter);
                break;
            case LOAD_MORE:
                dataList.addAll(list);
                articleAdapter.notifyDataSetChanged();
                break;
        }
        xlistListview.setPullLoadEnable(list.size() == QUERY_SIZE);
        DialogShow.closeDialog();
    }


    private XListView.IXListViewListener ixListViewListener = new XListView.IXListViewListener() {
        @Override
        public void onRefresh() {
            DialogShow.showRoundProcessDialog(getActivity());
            new Thread(new LoadDataThread(REFRESH)).start();
        }

        @Override
        public void onLoadMore() {
        }
    };

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            article = dataList.get(position - 1);
            Intent intent = new Intent(getContext(), IndexImageWebActivity.class);
            intent.putExtra("url", HttpConstants.GET_PARTY_ARTICLE_CONTENT + article.nId);
            intent.putExtra("content", article.vcTitle);
            intent.putExtra("isRush", true);
            intent.putExtra("topTitle", "三会一课");
            intent.putExtra("nShared", 2);
            startActivityForResult(intent, TO_CHANGE_READ_STATUS);
        }
    };

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEvent(Message msg) {
        switch (msg.what) {
            case 001:
                new Thread(new LoadDataThread(TO_CHANGE_READ_STATUS)).start();
                break;
        }
    }


    public void setNtype(String vcType) {
        this.vcType = vcType;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }
}
