package cn.sinokj.party.chineseFourParty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;

public class PartyResultActivity extends BaseActivity {
	private TextView titleText;
	private View topLeftImage;
	private TextView tvResult;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		titleText.setText("支付结果");
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
		Intent intent = getIntent();
		String stringExtra = intent.getStringExtra("nMoney");
		
		tvResult.setText("成功交纳党费"+stringExtra+"元");
		
		
	}
	@OnClick({ R.id.topbar_left_img })
	public void OnClick(View view) {
		switch (view.getId()) {
		case R.id.topbar_left_img:
			finish();
			break;

		default:
			break;
		}
	}


}
