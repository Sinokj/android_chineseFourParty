package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/4/26.
 */

public class ArticleTagListInfo {

    public List<ResultBean> result;

    public static class ResultBean {
        /**
         * nModuleId : 1
         * vcTagName : 十八大
         * nId : 100
         */

        public int nModuleId;
        public String vcTagName;
        public int nId;
    }
}
