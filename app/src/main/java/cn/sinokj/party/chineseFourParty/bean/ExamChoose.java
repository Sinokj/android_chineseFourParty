/** 
 * @Title ExamChoose.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月13日 下午5:29:30
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName ExamChoose
 * @Description 选项Model
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月13日 下午5:29:30
 **/
@SuppressWarnings("serial")
public class ExamChoose implements Serializable {
	@Expose
	private String vcAnswer;
	@Expose
	private int nContentId;
	@Expose
	private int nId;

	public String getVcAnswer() {
		return vcAnswer;
	}

	public void setVcAnswer(String vcAnswer) {
		this.vcAnswer = vcAnswer;
	}

	public int getnContentId() {
		return nContentId;
	}

	public void setnContentId(int nContentId) {
		this.nContentId = nContentId;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	@Override
	public String toString() {
		return "ExamChoose [vcAnswer = " + vcAnswer + ", nContentId = "
				+ nContentId + ", nId = " + nId + "]";
	}
}
