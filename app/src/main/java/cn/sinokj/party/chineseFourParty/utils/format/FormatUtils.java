package cn.sinokj.party.chineseFourParty.utils.format;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by azzbcc on 16-5-16.
 */
public class FormatUtils {

    @SuppressLint("SimpleDateFormat")
	public static String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }
    @SuppressLint("SimpleDateFormat")
    public static String formatDate(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }
}
