package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserLevel implements Serializable {

	@Expose
	private String nLevel;
	@Expose
	private String vcLevel;

	public String getnLevel() {
		return nLevel;
	}

	public void setnLevel(String nLevel) {
		this.nLevel = nLevel;
	}

	public String getVcLevel() {
		return vcLevel;
	}

	public void setVcLevel(String vcLevel) {
		this.vcLevel = vcLevel;
	}
}
