/** 
 * @Title AnswerSheetAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月8日 下午5:11:15
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.ExamQuestion;
import cn.sinokj.party.chineseFourParty.bean.ExamQuestionAnswered;
import cn.sinokj.party.chineseFourParty.utils.display.DisplayUtil;
import cn.sinokj.party.chineseFourParty.view.round.text.RoundTextView;


/**
 * @ClassName AnswerSheetAdapter
 * @Description 答题卡容器
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月8日 下午5:11:15
 **/
public class AnswerSheetAdapter extends BaseAdapter {
	private Context context;
	private boolean examFinish;
	private List<ExamQuestion> list;
	private Map<Integer, ExamQuestionAnswered> map;
	/**
	 * @param context
	 * @param list
	 * @param map
	 * @param examFinish 
	 **/
	public AnswerSheetAdapter(Context context, List<ExamQuestion> list, Map<Integer, ExamQuestionAnswered> map, boolean examFinish) {
		this.examFinish = examFinish;
		this.context = context;
		this.list = list;
		this.map = map;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View, ViewGroup)
	 **/
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (list != null && list.size() > 0) {
			final ExamQuestion examQuestion = list.get(position);
			TextView textView = new RoundTextView(context);
			AbsListView.LayoutParams params = new AbsListView.LayoutParams(
					DisplayUtil.dp2px(context, 50), DisplayUtil.dp2px(context, 50));
			textView.setLayoutParams(params);
			textView.setTextAppearance(context, R.style.textview);
			textView.setGravity(Gravity.CENTER);
			textView.setText(String.valueOf(examQuestion.getnCodeId()));
			//textView.setBackgroundColor(getTextColor(examQuestion));
			textView.setTextColor(getTextColor(examQuestion));
			textView.setBackgroundResource(getTextBackGround(examQuestion));
			convertView = textView;
		}
		return convertView;
	}

	private int getTextColor(ExamQuestion examQuestion) {
		int color = Color.GRAY;
		ExamQuestionAnswered examQuestionAnswered = map.get(examQuestion.getnId());
		// 未答题统一用白色填充
		if (!examQuestionAnswered.isAnswered()) {
			return color;
		}
		if (examQuestionAnswered.isTrue(examQuestion.getVcAnswer())) {
			color = Color.parseColor("#00a73b");
		} else {
			color = Color.RED;
		}
		// 未完成的试卷只有已答状态
		if (!examFinish) {
			if (!examQuestionAnswered.isAnswered()) {
				return color;
			} else {
				color = Color.parseColor("#00a73b");
			}
		}
		return color;
	}

	private int getTextBackGround(ExamQuestion examQuestion) {
		int imgId = R.drawable.bg_suject_2;
		ExamQuestionAnswered examQuestionAnswered = map.get(examQuestion.getnId());
		// 未答题统一用白色填充
		if (!examQuestionAnswered.isAnswered()) {
			return imgId;
		}
		if (examQuestionAnswered.isTrue(examQuestion.getVcAnswer())) {
			imgId = R.drawable.bg_suject_1;
		} else {
			imgId = R.drawable.bg_suject_3;
		}
		// 未完成的试卷只有已答状态
		if (!examFinish) {
			if (!examQuestionAnswered.isAnswered()) {
				return imgId;
			} else {
				imgId = R.drawable.bg_suject_1;
			}
		}
		return imgId;
	}
}
