/** 
 * @Title VoteActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午4:10:09
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.app.ActionBar.LayoutParams;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.bean.VoteAnswer;
import cn.sinokj.party.chineseFourParty.bean.VoteChoose;
import cn.sinokj.party.chineseFourParty.bean.VoteTopic;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;

/**
 * @ClassName VoteActivity
 * @Description 投票
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午4:10:09
 **/
public class VoteActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public ImageButton topLeftImage;
	@BindView(R.id.vote_title)
	public TextView voteTitleText;
	@BindView(R.id.vote_radio_group)
	public RadioGroup radioGroup;
	@BindView(R.id.vote_check_group)
	public LinearLayout checkGroup;
	@BindView(R.id.vote_submit)
	public Button submitButton;

	private VoteTopic voteTopic;
	private VoteAnswer voteAnswer;
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vote);
		ButterKnife.bind(this);
		titleText.setText("投票");
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
		initializeViews();
	}

	private void initializeViews() {
		voteTopic = (VoteTopic) getIntent().getSerializableExtra("voteTopic");
		voteAnswer = voteTopic.getIsVoted();
		voteTitleText.setText(voteTopic.getVcTopic());
		if (voteAnswer.hasChecked()) {
			submitButton.setVisibility(View.GONE);
		}
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		if (voteTopic.getVcType().contains("单选")) {
			for (VoteChoose voteChoose : voteTopic.getChoices()) {
				RadioButton radioButton = new RadioButton(this);
				radioButton.setLayoutParams(params);
				radioButton.setTextAppearance(this, R.style.radiobutton);
				radioButton.setBackgroundResource(R.drawable.radio_button_click);
				radioButton.setText(voteChoose.getVcOption());
				radioButton.setTag(voteChoose);
				radioGroup.addView(radioButton);
				if (voteAnswer.isChecked(voteChoose.getnOptionId())) {
					radioGroup.check(radioButton.getId());
				}
				radioButton.setClickable(!voteAnswer.hasChecked());
			}
			radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					VoteChoose choose = (VoteChoose) group.findViewById(checkedId).getTag();
					voteAnswer.setVoteOptions(choose.getnOptionId());
				}
			});
			radioGroup.setVisibility(View.VISIBLE);
			checkGroup.setVisibility(View.GONE);
		} else {
			for (VoteChoose voteChoose : voteTopic.getChoices()) {
				CheckBox checkBox = new CheckBox(this);
				checkBox.setLayoutParams(params);
				checkBox.setTextAppearance(this, R.style.checkbox);
				checkBox.setBackgroundResource(R.drawable.radio_button_click);
				checkBox.setText(voteChoose.getVcOption());
				checkBox.setTag(voteChoose);
				if (voteAnswer.isChecked(voteChoose.getnOptionId())) {
					checkBox.setChecked(true);
				}
				checkBox.setClickable(!voteAnswer.hasChecked());
				checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						VoteChoose choose = (VoteChoose) buttonView.getTag();
						if (isChecked) {
							if (voteAnswer.getCheckedCount() >= voteTopic.getnMaxVote()) {
								buttonView.setChecked(false);
								Toast.makeText(VoteActivity.this, "最多可以投" + voteTopic.getnMaxVote() + "票", Toast.LENGTH_SHORT).show();
								return;
							}
							voteAnswer.addVoteOptions(choose.getnOptionId());
						} else {
							voteAnswer.delVoteOptions(choose.getnOptionId());
						}
					}
				});
				checkGroup.addView(checkBox);
			}
			radioGroup.setVisibility(View.GONE);
			checkGroup.setVisibility(View.VISIBLE);
		}
	}

	@OnClick({R.id.topbar_left_img, R.id.vote_submit})
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.topbar_left_img:
			finish();
			break;
		case R.id.vote_submit:
			if (!voteAnswer.hasChecked()) {
				Toast.makeText(this, "请投票", Toast.LENGTH_SHORT).show();
				return;
			}
			new Thread(new LoadDataThread()).start();
			break;
		}
	}
	
	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.submitVote(String.valueOf(voteTopic.getnId()), voteAnswer.getVoteOptions());
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		if (1 == jsonObject.optInt(HttpConstants.ReturnResult.NRES, 0)) {
			Toast.makeText(this, "提交成功", Toast.LENGTH_SHORT).show();
			finish();
		} else {
			Toast.makeText(this, "提交失败", Toast.LENGTH_SHORT).show();
		}
		DialogShow.closeDialog();
	}
}
