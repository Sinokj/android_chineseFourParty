package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class OpinionTopic implements Serializable {
	@Expose
	private String vcRegister;
	@Expose
	private Date dtBegin;
	@Expose
	private Date dtReg;
	@Expose
	private String vcTitle;
	@Expose
	private int nJoined;
	@Expose
	private Date dtEnd;
	@Expose
	private int nId;

	public String getVcRegister() {
		return vcRegister;
	}

	public void setVcRegister(String vcRegister) {
		this.vcRegister = vcRegister;
	}

	public Date getDtBegin() {
		return dtBegin;
	}

	public void setDtBegin(Date dtBegin) {
		this.dtBegin = dtBegin;
	}

	public Date getDtReg() {
		return dtReg;
	}

	public void setDtReg(Date dtReg) {
		this.dtReg = dtReg;
	}

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public int getnJoined() {
		return nJoined;
	}

	public void setnJoined(int nJoined) {
		this.nJoined = nJoined;
	}

	public Date getDtEnd() {
		return dtEnd;
	}

	public void setDtEnd(Date dtEnd) {
		this.dtEnd = dtEnd;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

}