package cn.sinokj.party.chineseFourParty.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.fragment.base.BaseFragment;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;

/**
 * Created by l on 2017/11/12.
 */

public class AboutAsFragment extends BaseFragment {
    @BindView(R.id.title)
    TextView title;
    Unbinder unbinder;
    @BindView(R.id.status_bar_height)
    View statusBarHeight;
    private WebFragment webFragment;
    private String url;
    private View view;
    private int idPos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        view = inflater.inflate(R.layout.about_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        initImmersionBar(statusBarHeight);
        //EventBus.getDefault().register(this);
        idPos = Integer.valueOf(App.nCommitteeId);
        url = HttpConstants.GET_ARTICLE + App.nCommitteeId;
        title.setText("关于我们");
        title.setVisibility(View.VISIBLE);
        webFragment = (WebFragment) getChildFragmentManager().findFragmentById(R.id.about_us_webview);
        webFragment.loadUrl(url);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /*  @Subscribe(threadMode = ThreadMode.MainThread)
      public void onEvent(Message msg) {
          switch (msg.what) {
              case Constans.ANNOUNCEMENT_REFRESH:
                  url = HttpConstants.GET_ARTICLE + App.nCommitteeId;
                  webFragment.loadUrl(url);
                  break;
          }
      }*/

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            if (!TextUtils.isEmpty(App.nCommitteeId)) {
                if (idPos != Integer.valueOf(App.nCommitteeId)) {
                    url = HttpConstants.GET_ARTICLE + App.nCommitteeId;
                    webFragment.loadUrl(url);
                    idPos = Integer.valueOf(App.nCommitteeId);
                }
            }
        }
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        //EventBus.getDefault().unregister(this);
    }
}
