package cn.sinokj.party.chineseFourParty.bean;

/**
 * Created by l on 2017/6/8.
 */

public class LoginInfo {

    /**
     * nRes : 1
     * vcRes :
     * vcName : 菅晓凯
     * partyGroupName : 深万源党支部
     * dtNow : 2017-06-08 17:08:24
     */

    private int nRes;
    private String vcRes;
    private String vcName;
    private String partyGroupName;
    private String dtNow;
    private String nCommitteeId;

    public String getnCommitteeId() {
        return nCommitteeId;
    }

    public void setnCommitteeId(String nCommitteeId) {
        this.nCommitteeId = nCommitteeId;
    }

    public int getNRes() {
        return nRes;
    }

    public void setNRes(int nRes) {
        this.nRes = nRes;
    }

    public String getVcRes() {
        return vcRes;
    }

    public void setVcRes(String vcRes) {
        this.vcRes = vcRes;
    }

    public String getVcName() {
        return vcName;
    }

    public void setVcName(String vcName) {
        this.vcName = vcName;
    }

    public String getPartyGroupName() {
        return partyGroupName;
    }

    public void setPartyGroupName(String partyGroupName) {
        this.partyGroupName = partyGroupName;
    }

    public String getDtNow() {
        return dtNow;
    }

    public void setDtNow(String dtNow) {
        this.dtNow = dtNow;
    }
}
