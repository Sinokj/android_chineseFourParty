/** 
 * @Title ExamResultListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月27日 上午9:50:06
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.ExamResultListAdapter;
import cn.sinokj.party.chineseFourParty.bean.ExamTopic;


/**
 * @ClassName ExamResultListActivity
 * @Description 考试结果列表
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月27日 上午9:50:06
 **/
public class ExamResultListActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public ImageButton topLeftImage;
	@BindView(R.id.exam_result_list_listview)
	public ListView listView;
	
	private List<ExamTopic> examTopics;
	private ExamResultListAdapter examResultListAdapter;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_result_list);
		ButterKnife.bind(this);
        Serializable examTopics = getIntent().getSerializableExtra("examTopics");
        this.examTopics = (List<ExamTopic>) getIntent().getSerializableExtra("examTopics");
		//titleText.setText("答题结果");
		titleText.setText(getIntent().getStringExtra("functionType"));
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
		
		examResultListAdapter = new ExamResultListAdapter(this, this.examTopics);
		listView.setAdapter(examResultListAdapter);
		
		if (this.examTopics == null || this.examTopics.size() == 0) {
			Toast.makeText(this, "当前没有答题成绩", Toast.LENGTH_SHORT).show();
		}
	}
	
	@OnClick(R.id.topbar_left_img)
	public void onClick(View view) {
		finish();
	}

}
