package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2018/11/9.
 */
public class MyBookMoneyBean{

    /**
     * result : true
     * objects : [{"dtReg":"2018-11-08 15:17:00","vcType":"图书购买","vcOrderNo":"jsahflh13safafaw1","nTokenFee":2100,"vcPersonTel":"13523767769","nId":31498},{"dtReg":"2018-11-07 15:17:00","vcType":"图书购买","vcOrderNo":"jsahflh13safafaw2","nTokenFee":2200,"vcPersonTel":"13523767769","nId":31499}]
     */

    public boolean result;
    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * dtReg : 2018-11-08 15:17:00
         * vcType : 图书购买
         * vcOrderNo : jsahflh13safafaw1
         * nTokenFee : 2100
         * vcPersonTel : 13523767769
         * nId : 31498
         */

        public String dtReg;
        public String vcType;
        public String vcOrderNo;
        public int nTokenFee;
        public String vcPersonTel;
        public int nId;
    }
}
