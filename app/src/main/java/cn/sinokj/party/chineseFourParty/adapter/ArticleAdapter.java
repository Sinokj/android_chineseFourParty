package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ArticleListActivity;


public class ArticleAdapter extends BaseAdapter {

	private Context context;
	private List<String> list;
	
	private int[] resources = {R.drawable.business_manage_bg1, R.drawable.business_manage_bg2, R.drawable.business_manage_bg3};

	public ArticleAdapter(Context context, List<String> list) {
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		final String type = list.get(position);
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.article_item, null);
			viewHolder.button = (Button) convertView.findViewById(R.id.article_item_button);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.button.setText(type);
		viewHolder.button.setBackgroundResource(resources[position % resources.length]);
		viewHolder.button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ArticleListActivity.class);
				intent.putExtra("type", type);
				context.startActivity(intent);
			}
		});
		return convertView;
	}

	private class ViewHolder {
		private Button button;
	}
}
