package cn.sinokj.party.chineseFourParty.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.ArticleListAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.Article;
import cn.sinokj.party.chineseFourParty.bean.PartyTypebean;
import cn.sinokj.party.chineseFourParty.fragment.ArticleFragment;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


public class ArticleListActivity extends BaseActivity {
    public static final int TO_CHANGE_READ_STATUS = 3;
    public static final int INIT_DATA = 1;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.topbar_left_img)
    ImageButton topbarLeftImg;
    @BindView(R.id.topbar_left_text)
    TextView topbarLeftText;
    @BindView(R.id.topbar_right_img)
    ImageButton topbarRightImg;
    @BindView(R.id.topbar_right_text)
    TextView topbarRightText;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view)
    View mView;
    @BindView(R.id.vp_view)
    ViewPager vpView;


    private String type;
    private Article article;
    private List<Article> articleList;
    private ArticleListAdapter articleListAdapter;
    private MyPagerAdapter myPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articlelist_tab);
        ButterKnife.bind(this);
        type = getIntent().getStringExtra("type");
        title.setText("三会一课");
        topbarLeftImg.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);

        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    @OnClick(R.id.topbar_left_img)
    public void OnClick(View view) {
        finish();
    }



    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        switch (what) {
            case INIT_DATA:
                return HttpDataService.getPartyTypeList();
        }
        return super.getDataFunction(what, arg1, arg2, obj);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        String json = jsonObject.toString();
        Gson gson = new Gson();
        PartyTypebean partyTypebean = gson.fromJson(json, PartyTypebean.class);
        initViewPager(partyTypebean);
    }

    private void initViewPager(PartyTypebean partyTypebean) {
        List<String> newsClasses = new ArrayList<>();
        List<Fragment> list_fragment = new ArrayList<>();
        List<PartyTypebean.ResultBean> partyTypeList = partyTypebean.result;
        for (int i = 0; i < partyTypebean.result.size(); i++) {
            ArticleFragment articleFragment = new ArticleFragment();
            articleFragment.setNtype(partyTypebean.result.get(i).nTagName);
            list_fragment.add(articleFragment);
            newsClasses.add(partyTypeList.get(i).nTagName);
        }
        //隐藏TabLayout
        if (partyTypeList.size() == 1) {
            mTabLayout.setVisibility(View.GONE);
            mView.setVisibility(View.GONE);
        }
        FragmentManager fm = getSupportFragmentManager();
        myPagerAdapter = new MyPagerAdapter(fm, list_fragment, newsClasses);
        vpView.setAdapter(myPagerAdapter);
        vpView.setOffscreenPageLimit(6);

        mTabLayout.setupWithViewPager(vpView);
        //设置可以滑动
        //mTabLayout.setTabMode(TabLayout.MODE_FIXED);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (App.loginStatus && resultCode == Activity.RESULT_OK && requestCode == TO_CHANGE_READ_STATUS) {
            new Thread(new LoadDataThread(TO_CHANGE_READ_STATUS)).start();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    class MyPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> list_fragment;
        private final List<String> list_Title;

        public MyPagerAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String>
                list_Title) {
            super(fm);
            this.list_fragment = list_fragment;
            this.list_Title = list_Title;
        }

        @Override
        public Fragment getItem(int position) {
            return list_fragment.get(position);
        }

        @Override
        public int getCount() {
            return list_fragment.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list_Title.get(position);
        }
    }
}
