/** 
 * @Title VoteAnswer.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午5:15:22
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName VoteAnswer
 * @Description 投票结果
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午5:15:22
 **/
@SuppressWarnings("serial")
public class VoteAnswer implements Serializable {
	@Expose
	private int isVoted;
	@Expose
	private String voteOptions;
	
	public int getIsVoted() {
		return isVoted;
	}

	public void setIsVoted(int isVoted) {
		this.isVoted = isVoted;
	}

	public String getVoteOptions() {
		return voteOptions;
	}

	public void setVoteOptions(int nOptionId) {
		this.voteOptions = nOptionId + ",";
	}

	@Override
	public String toString() {
		return "VoteAnswer [isVoted = " + isVoted + ", voteOptions = "
				+ voteOptions + "]";
	}

	public boolean hasChecked() {
		return null != voteOptions && !"".equals(voteOptions);
	}

	public boolean isChecked(int num) {
		if (!hasChecked()) {
			return false;
		}
		return voteOptions.contains(String.valueOf(num));
	}

	public void addVoteOptions(int nOptionId) {
		if (hasChecked()) {
			this.voteOptions = this.voteOptions + nOptionId + ",";
		} else {
			setVoteOptions(nOptionId);
		}
	}

	public void delVoteOptions(int nOptionId) {
		this.voteOptions = this.voteOptions.replace(nOptionId + ",", "");
	}

	public int getCheckedCount() {
		if (hasChecked()) {
			return this.voteOptions.split(",").length;
		}
		return 0;
	}
}
