package cn.sinokj.party.chineseFourParty.view.webview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebSettings.LayoutAlgorithm;
import com.tencent.smtt.sdk.WebView;


public class X5WebView extends WebView {

	@SuppressLint("SetJavaScriptEnabled")
	public X5WebView(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		this.setWebViewClientExtension(new X5WebViewEventHandler(this));// 配置X5webview的事件处理
		initWebViewSettings();
		this.getView().setClickable(true);
		this.getView().setOnTouchListener(new OnTouchListener() {
			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return false;
			}
		});
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebViewSettings() {
		WebSettings webSetting = this.getSettings();
		webSetting.setJavaScriptEnabled(true);
		webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
		webSetting.setAllowFileAccess(true);
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
		webSetting.setSupportZoom(true);
		webSetting.setBuiltInZoomControls(true);
		webSetting.setUseWideViewPort(true);
		webSetting.setSupportMultipleWindows(true);
		webSetting.setAppCacheEnabled(true);
		webSetting.setDomStorageEnabled(true);
		webSetting.setGeolocationEnabled(true);
		webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
		webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
		webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);
	}

	/*@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		boolean ret = super.drawChild(canvas, child, drawingTime);
		if (Utils.DEBUG_MODE) {
			canvas.save();
			Paint paint = new Paint();
			paint.setColor(0x7fff0000);
			paint.setTextSize(24.f);
			paint.setAntiAlias(true);
			if (getX5WebViewExtension() != null) {
				canvas.drawText(this.getContext().getPackageName() + "-pid:" + android.os.Process.myPid(), 10, 50, paint);
				canvas.drawText("X5  Core:" + QbSdk.getTbsVersion(this.getContext()), 10, 100, paint);
			} else {
				canvas.drawText(this.getContext().getPackageName() + "-pid:" + android.os.Process.myPid(), 10, 50, paint);
				canvas.drawText("Sys Core", 10, 100, paint);
			}
			canvas.drawText(Build.MANUFACTURER, 10, 150, paint);
			canvas.drawText(Build.MODEL, 10, 200, paint);
			canvas.restore();
		}
		return ret;
	}*/

	public X5WebView(Context arg0) {
		super(arg0);
		setBackgroundColor(85621);
	}
//	
//	/**
//	 * 当webchromeClient收到 web的prompt请求后进行拦截判断，用于调起本地android方法
//	 * 
//	 * @param methodName
//	 *            方法名称
//	 * @param blockName
//	 *            区块名称
//	 * @return true ：调用成功 ； false ：调用失败
//	 */
//	private boolean onJsPrompt(String methodName, String blockName) {
//		String tag = SecurityJsBridgeBundle.BLOCK + blockName + "-" + SecurityJsBridgeBundle.METHOD + methodName;
//
//		if (this.mJsBridges != null && this.mJsBridges.containsKey(tag)) {
//			((SecurityJsBridgeBundle) this.mJsBridges.get(tag)).onCallMethod();
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	/**
//	 * 判定当前的prompt消息是否为用于调用native方法的消息
//	 * 
//	 * @param msg
//	 *            消息名称
//	 * @return true 属于prompt消息方法的调用
//	 */
//	private boolean isMsgPrompt(String msg) {
//		if (msg != null && msg.startsWith(SecurityJsBridgeBundle.PROMPT_START_OFFSET)) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	// TBS: Do not use @Override to avoid false calls
	public boolean tbs_dispatchTouchEvent(MotionEvent ev, View view) {
		return super_dispatchTouchEvent(ev);
	}

	// TBS: Do not use @Override to avoid false calls
	public boolean tbs_onInterceptTouchEvent(MotionEvent ev, View view) {
		return super.super_onInterceptTouchEvent(ev);
	}

	protected void tbs_onScrollChanged(int l, int t, int oldl, int oldt, View view) {
		super_onScrollChanged(l, t, oldl, oldt);
	}

	protected void tbs_onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY, View view) {
		super_onOverScrolled(scrollX, scrollY, clampedX, clampedY);
	}

	protected void tbs_computeScroll(View view) {
		super_computeScroll();
	}

	protected boolean tbs_overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
			int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent, View view) {
		return super_overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
				maxOverScrollY, isTouchEvent);
	}

	protected boolean tbs_onTouchEvent(MotionEvent event, View view) {
		return super_onTouchEvent(event);
	}
}
