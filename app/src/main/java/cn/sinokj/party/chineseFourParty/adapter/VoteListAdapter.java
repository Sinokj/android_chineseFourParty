/** 
 * @Title VoteListAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午3:34:16
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.VoteActivity;
import cn.sinokj.party.chineseFourParty.bean.VoteTopic;
import cn.sinokj.party.chineseFourParty.utils.format.FormatUtils;


/**
 * @ClassName VoteListAdapter
 * @Description 投票列表容器
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午3:34:16
 **/
public class VoteListAdapter extends BaseAdapter {
	private Context context;
	private List<VoteTopic> list;

	/**
	 **/
	public VoteListAdapter(Context context, List<VoteTopic> list) {
		this.context = context;
		this.list = list;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View, ViewGroup)
	 **/
	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final VoteTopic voteTopic = list.get(position);
		convertView = LayoutInflater.from(context).inflate(R.layout.vote_list_item, null);
		
		TextView timeText = (TextView) convertView.findViewById(R.id.vote_list_item_time);
		TextView titleText = (TextView) convertView.findViewById(R.id.vote_list_item_title);
		TextView statusText = (TextView) convertView.findViewById(R.id.vote_list_item_status);
		
		titleText.setText(voteTopic.getVcTopic());
		timeText.setText(FormatUtils.formatDate(voteTopic.getDtEnd()));
		if (voteTopic.getIsVoted().hasChecked()) {
			statusText.setVisibility(View.VISIBLE);
		} else {
			statusText.setVisibility(View.GONE);
		}
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, VoteActivity.class);
				intent.putExtra("voteTopic", voteTopic);
				context.startActivity(intent);
			}
		});
		
		return convertView;
	}
}
