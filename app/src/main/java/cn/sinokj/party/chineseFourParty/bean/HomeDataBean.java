package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2018/4/23.
 */

public class HomeDataBean {

    /**
     * result : true
     * objects : {"moduleList":[{"vcJumpLink":"","nTemplate":0,"nNeedLogin":0,"nUse":1,"nSort":1,"vcModule":"党章党建4","nId":39,"vcRemark":"","vcIconUrl":"http://ouak59mwz.bkt.clouddn.com//ueditor/jsp/upload/image/20170828/1503885123051053375.png"}],"committee":{"nId":1,"vcPartyName":"院党委","vcPartyAppName":"院党委党建App","nModuleType":3,"nLevel":1},"articleList":[{"vcRegister":"admin","nOrder":0,"dtReg":"2017-11-22 00:00:00","nclick":4,"nLogin":0,"vcType":"首页广告","url":"http://localhost:8099/policePartyApp/getCommonPage.do?nId=36","vcMemo":"","vcPlatform":"e党建","nShow":1,"vcTitle":"广丰公安以务实行动学习贯彻十九大精神","vcDescribe":"广丰公安以务实行动学习贯彻十九大精神","bCustom":false,"vcUrl":"","vcPath":"http://otl1isxr7.bkt.clouddn.com/before/adm/arti/1512111295694","nId":36,"vcGroupId":""}]}
     */

    public boolean result;
    public ObjectsBean objects;

    public static class ObjectsBean {
        /**
         * moduleList : [{"vcJumpLink":"","nTemplate":0,"nNeedLogin":0,"nUse":1,"nSort":1,"vcModule":"党章党建4","nId":39,"vcRemark":"","vcIconUrl":"http://ouak59mwz.bkt.clouddn.com//ueditor/jsp/upload/image/20170828/1503885123051053375.png"}]
         * committee : {"nId":1,"vcPartyName":"院党委","vcPartyAppName":"院党委党建App","nModuleType":3,"nLevel":1}
         * articleList : [{"vcRegister":"admin","nOrder":0,"dtReg":"2017-11-22 00:00:00","nclick":4,"nLogin":0,"vcType":"首页广告","url":"http://localhost:8099/policePartyApp/getCommonPage.do?nId=36","vcMemo":"","vcPlatform":"e党建","nShow":1,"vcTitle":"广丰公安以务实行动学习贯彻十九大精神","vcDescribe":"广丰公安以务实行动学习贯彻十九大精神","bCustom":false,"vcUrl":"","vcPath":"http://otl1isxr7.bkt.clouddn.com/before/adm/arti/1512111295694","nId":36,"vcGroupId":""}]
         */

        public CommitteeBean committee;
        public List<ModuleListBean> moduleList;
        public List<ArticleListBean> articleList;

        public static class CommitteeBean {
            /**
             * nId : 1
             * vcPartyName : 院党委
             * vcPartyAppName : 院党委党建App
             * nModuleType : 3
             * nLevel : 1
             */

            public int nId;
            public String vcPartyName;
            public String vcPartyAppName;
            public int nModuleType;
            public int nLevel;
        }

        public static class ModuleListBean {
            /**
             * vcJumpLink :
             * nTemplate : 0
             * nNeedLogin : 0
             * nUse : 1
             * nSort : 1
             * vcModule : 党章党建4
             * nId : 39
             * vcRemark :
             * vcIconUrl : http://ouak59mwz.bkt.clouddn.com//ueditor/jsp/upload/image/20170828/1503885123051053375.png
             */

            public String vcJumpLink;
            public int nTemplate;
            public int nNeedLogin;
            public int nUse;
            public int nSort;
            public String vcModule;
            public int nId;
            public String vcRemark;
            public String vcIconUrl;
            public int unRead;
        }

        public static class ArticleListBean {
            /**
             * vcRegister : admin
             * nOrder : 0
             * dtReg : 2017-11-22 00:00:00
             * nclick : 4
             * nLogin : 0
             * vcType : 首页广告
             * url : http://localhost:8099/policePartyApp/getCommonPage.do?nId=36
             * vcMemo :
             * vcPlatform : e党建
             * nShow : 1
             * vcTitle : 广丰公安以务实行动学习贯彻十九大精神
             * vcDescribe : 广丰公安以务实行动学习贯彻十九大精神
             * bCustom : false
             * vcUrl :
             * vcPath : http://otl1isxr7.bkt.clouddn.com/before/adm/arti/1512111295694
             * nId : 36
             * vcGroupId :
             */

            public String vcRegister;
            public int nOrder;
            public String dtReg;
            public int nclick;
            public int nLogin;
            public String vcType;
            public String url;
            public String vcMemo;
            public String vcPlatform;
            public int nShow;
            public String vcTitle;
            public String vcDescribe;
            public boolean bCustom;
            public String vcUrl;
            public String vcPath;
            public int nId;
            public String vcGroupId;
        }
    }
}
