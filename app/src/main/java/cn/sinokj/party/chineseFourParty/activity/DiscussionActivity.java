/** 
 * @Title DiscussionActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 下午2:23:43
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.DiscussionAdapter;
import cn.sinokj.party.chineseFourParty.bean.DiscussionChoose;
import cn.sinokj.party.chineseFourParty.bean.DiscussionGread;
import cn.sinokj.party.chineseFourParty.bean.DiscussionTopic;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;
import cn.sinokj.party.chineseFourParty.view.dialog.message.MessageDialogInterface;
import cn.sinokj.party.chineseFourParty.view.dialog.message.MessageDialogUtils;


/**
 * @ClassName DiscussionActivity
 * @Description 民主评议
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 下午2:23:43
 **/
public class DiscussionActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.discussion_list_view)
	public ListView listView;
	@BindView(R.id.discussion_submit)
	public Button submitButton;

	private Map<String, String> answerMap;
	private DiscussionTopic discussionTopic;
	private DiscussionGread discussionGread;
	private DiscussionAdapter discussionAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discussion);
		ButterKnife.bind(this);
		topLeft.setVisibility(View.VISIBLE);
		titleText.setVisibility(View.VISIBLE);
		titleText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
		discussionTopic = (DiscussionTopic) getIntent().getSerializableExtra("discussionTopic");
		
		initializeViews(0);
		showDiscussPerson();
	}
	
	private void initializeViews(int which) {
		answerMap = new HashMap<String, String>();
		discussionGread = discussionTopic.getBeGreadPerson().get(which);
		List<DiscussionChoose> answerList = discussionGread.getOptionScore();
		
		discussionAdapter = new DiscussionAdapter(this, discussionTopic.getChoices(), answerList, answerMap);
		
		titleText.setText(discussionGread.getBeGreadPerson());
		listView.setAdapter(discussionAdapter);
		
		if (answerList == null || answerList.size() == 0) {
			submitButton.setVisibility(View.VISIBLE);
		} else {
			submitButton.setVisibility(View.GONE);
		}
		// 评议已结束，不显示按钮
		if (discussionTopic.getnStatus() == 1) {
			submitButton.setVisibility(View.GONE);
		}
	}

	@OnClick({R.id.title,R.id.topbar_left_img,R.id.discussion_submit})
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.title:
			showDiscussPerson();
			break;
		case R.id.topbar_left_img:
			finish();
			break;
		case R.id.discussion_submit:
			if (answerMap.size() < discussionTopic.getChoices().size()) {
				Toast.makeText(this, "请完成所有选项再提交", Toast.LENGTH_SHORT).show();
				return;
			}
			DialogShow.showRoundProcessDialog(this);
			new Thread(new LoadDataThread()).start();
			break;
		}
	}
	
	public void showDiscussPerson() {
		MessageDialogUtils.showMessageDialog(this, "选择评议人员", discussionTopic.getBeGreadPerson(), new MessageDialogInterface<DiscussionGread>() {
			@Override
			public String getDataItem(DiscussionGread data) {
				String text = data.getBeGreadPerson();
				List<DiscussionChoose> answerList = data.getOptionScore();
				if (null != answerList && answerList.size() == discussionTopic.getChoices().size()) {
					text += "  \t" + "(已评)";
				}
				return text;
			}
			@Override
			public void onItemClick(int which) {
				initializeViews(which);
			}
		});
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.submitGread(String.valueOf(discussionTopic.getnId()), discussionGread.getVcTel(), answerMap);
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		DialogShow.closeDialog();
		if (1 == jsonObject.optInt(HttpConstants.ReturnResult.NRES, 0)) {
			Toast.makeText(this, "评议成功", Toast.LENGTH_SHORT).show();
			finish();
		} else {
			Toast.makeText(this, "评议出错了", Toast.LENGTH_SHORT).show();
		}
	}
}
