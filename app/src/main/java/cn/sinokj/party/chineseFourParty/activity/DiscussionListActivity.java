/** 
 * @Title DiscussionListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 下午1:39:56
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.DiscussionListAdapter;
import cn.sinokj.party.chineseFourParty.bean.DiscussionTopic;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.utils.gson.GsonHandler;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


/**
 * @ClassName DiscussionListActivity
 * @Description 民主评议列表
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 下午1:39:56
 **/
public class DiscussionListActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView title;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.discussion_list_list_view)
	public ListView listView;
	
	private DiscussionListAdapter discussionListAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discussion_list);
		ButterKnife.bind(this);
		title.setText("民主评议");
		title.setVisibility(View.VISIBLE);
		topLeft.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onResume() {
		DialogShow.showRoundProcessDialog(this);
		new Thread(new LoadDataThread()).start();
		super.onResume();
	}

	@OnClick(R.id.topbar_left_img)
	public void OnClick(View view) {
		finish();
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.getDiscussionsTopics();
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		JSONArray jsonArray = jsonObject.optJSONArray(HttpConstants.OBJECTS);
		List<DiscussionTopic> discussionTopicList = GsonHandler.getGson().fromJson(jsonArray.toString(),
				new TypeToken<List<DiscussionTopic>>() {}.getType());
		discussionListAdapter = new DiscussionListAdapter(this, discussionTopicList);
		listView.setAdapter(discussionListAdapter);
		DialogShow.closeDialog();
		if (discussionTopicList.size() == 0) {
			Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
		}
	}
}
