package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import de.greenrobot.event.EventBus;


/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName ExamResultActivity
 * @Description 考试结果
 * @date 创建时间 2016年7月15日 下午6:03:36
 **/
public class ExamResultActivity extends BaseActivity {
    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public ImageButton topLeftImage;
    @BindView(R.id.exam_result_score)
    public TextView scoreText;
    @BindView(R.id.exam_result_result)
    public ImageView resultImage;

    private int nScore;
    private String isPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_exam_result);
        ButterKnife.bind(this);
        titleText.setText("答题结果");
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);
        nScore = getIntent().getIntExtra("nScore", 0);
        isPass = getIntent().getStringExtra("isPass");
        if (isPass.equals("不及格！")) {
            resultImage.setBackgroundResource(R.drawable.pic_lose);
        } else {
            resultImage.setBackgroundResource(R.drawable.pic_success);

        }
        scoreText.setText(String.valueOf(nScore));

    }

    @OnClick(R.id.back_home)
    public void onClick(View view) {
        sendMessage();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendMessage();
    }

    private void sendMessage() {
        Message msg = Message.obtain();
        msg.what = 002;
        EventBus.getDefault().post(msg);
    }
}
