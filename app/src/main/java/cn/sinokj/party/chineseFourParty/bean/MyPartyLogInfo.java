package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2017/10/30.
 */

public class MyPartyLogInfo {

    /**
     * result : true
     * objects : [{"vcName":"王可","vcUserId":"15901068663","vcTradeNo":"2017101121001004180207090752","dtReg":"2017-10-11 08:49:58","vcPayMode":"支付宝","vcSource":"iOS","vcAccNo":"15901068663","mFee":36,"nExe":1,"vcMemo":null,"dtExe":"2017-10-11 08:50:15","nBranchId":2,"vcPayKind":"党费缴纳","nPartyId":4,"nId":2882,"mRealFee":36}]
     */

    public boolean result;
    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * vcName : 王可
         * vcUserId : 15901068663
         * vcTradeNo : 2017101121001004180207090752
         * dtReg : 2017-10-11 08:49:58
         * vcPayMode : 支付宝
         * vcSource : iOS
         * vcAccNo : 15901068663
         * mFee : 36
         * nExe : 1
         * vcMemo : null
         * dtExe : 2017-10-11 08:50:15
         * nBranchId : 2
         * vcPayKind : 党费缴纳
         * nPartyId : 4
         * nId : 2882
         * mRealFee : 36
         */

        public String vcName;
        public String vcUserId;
        public String vcTradeNo;
        public String dtReg;
        public String vcPayMode;
        public String vcSource;
        public String vcAccNo;
        public double mFee;
        public int nExe;
        public String vcMemo;
        public String dtExe;
        public int nBranchId;
        public String vcPayKind;
        public int nPartyId;
        public int nId;
        public double mRealFee;
    }
}
