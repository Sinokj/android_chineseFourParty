package cn.sinokj.party.chineseFourParty.activity;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.ArticleTagListInfo;
import cn.sinokj.party.chineseFourParty.fragment.NewsFragment;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;


/**
 * Created by Administrator on 2018/4/26.
 */

public class XListTabActivity extends BaseActivity {

    private static final int INIT_DATA = 1;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.vp_view)
    ViewPager mVpCoupon;
    @BindView(R.id.view)
    View mView;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.topbar_left_img)
    ImageButton mTopbarLeftImg;
    @BindView(R.id.topbar_left_text)
    TextView mTopbarLeftText;
    @BindView(R.id.topbar_right_img)
    ImageButton mTopbarRightImg;
    @BindView(R.id.topbar_right_text)
    TextView mTopbarRightText;
    private String functionType, nModuleId;
    private NewsPagerAdapter couponPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xlist_tab);
        ButterKnife.bind(this);
        functionType = getIntent().getStringExtra("functionType");
        nModuleId = getIntent().getIntExtra("nId", 0) + "";
        initTitle();
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    private void initTitle() {
        if ("主题教育".equals(functionType)) {
            mTitle.setText("专题教育");
        } else {
            mTitle.setText(functionType);
        }
        mTitle.setVisibility(View.VISIBLE);
        mTopbarRightImg.setVisibility(View.VISIBLE);
        mTopbarLeftImg.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.topbar_left_img, R.id.topbar_right_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_right_img:
                if (App.loginStatus) {
                    startActivity(new Intent(this, SearchActivity.class));
                } else {
                    startActivity(new Intent(this, LoginInActivity.class));
                }
                break;
            case R.id.topbar_left_img:
                finish();
                break;
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        return HttpDataService.getArticleTagList(nModuleId);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        String json = jsonObject.toString();
        Gson gson = new Gson();
        ArticleTagListInfo myPartyLogInfo = gson.fromJson(json, ArticleTagListInfo.class);
        initViewPager(myPartyLogInfo);
    }

    private void initViewPager(ArticleTagListInfo myPartyLogInfo) {
        List<String> newsClasses = new ArrayList<>();
        List<Fragment> list_fragment = new ArrayList<>();

        for (int i = 0; i < myPartyLogInfo.result.size(); i++) {
            NewsFragment mNewsFragment = new NewsFragment();
            mNewsFragment.setNclass(myPartyLogInfo.result.get(i).nId, functionType);
            list_fragment.add(mNewsFragment);
            newsClasses.add(myPartyLogInfo.result.get(i).vcTagName);
        }
        //隐藏TabLayout
        if (myPartyLogInfo.result.size() == 1) {
            mTabLayout.setVisibility(View.GONE);
            mView.setVisibility(View.GONE);
        }
        FragmentManager fm = getSupportFragmentManager();
        couponPagerAdapter = new NewsPagerAdapter(fm, list_fragment, newsClasses);
        mVpCoupon.setAdapter(couponPagerAdapter);
        mVpCoupon.setOffscreenPageLimit(6);
        mTabLayout.setupWithViewPager(mVpCoupon);
        //设置可以滑动
        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    class NewsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> list_fragment;
        private final List<String> list_Title;

        public NewsPagerAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String>
                list_Title) {
            super(fm);
            this.list_fragment = list_fragment;
            this.list_Title = list_Title;
        }

        @Override
        public Fragment getItem(int position) {
            return list_fragment.get(position);
        }

        @Override
        public int getCount() {
            return list_fragment.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list_Title.get(position);
        }
    }
}
