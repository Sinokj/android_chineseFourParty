package cn.sinokj.party.chineseFourParty.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.MyBookMoneyBean;

/**
 * Created by l on 2018/11/9.
 */
public class MyBookMoneyAdapyer extends BaseQuickAdapter<MyBookMoneyBean.ObjectsBean, BaseViewHolder> {

    public MyBookMoneyAdapyer(@Nullable List<MyBookMoneyBean.ObjectsBean> data) {
        super(R.layout.item_mybookmoney, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyBookMoneyBean.ObjectsBean item) {
        helper.setText(R.id.tv_time, item.dtReg)
                .setText(R.id.tv_money, "-" + item.nTokenFee)
                .setText(R.id.tv_orderno, "订单号:" + item.vcOrderNo)
                .setText(R.id.tv_title, item.vcType);
    }
}
