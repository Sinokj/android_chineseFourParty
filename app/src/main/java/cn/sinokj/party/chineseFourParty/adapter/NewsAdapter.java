package cn.sinokj.party.chineseFourParty.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.IndexImage;
import cn.sinokj.party.chineseFourParty.view.GlideRoundTransform;


/**
 * Created by l on 2017/10/31.
 */

public class NewsAdapter extends BaseQuickAdapter<IndexImage,BaseViewHolder>{

    public boolean isMineClick = false;

    public NewsAdapter(@Nullable List<IndexImage> data) {
        super(R.layout.xlist_important_news_item,data);
    }

    public NewsAdapter(@Nullable List<IndexImage> data,boolean isMineClick) {
        super(R.layout.xlist_important_news_item,data);
        this.isMineClick = isMineClick;
    }

    @Override
    protected void convert(BaseViewHolder holder, IndexImage item) {
        holder.setText(R.id.xlist_item_title,item.vcTitle)
                .setText(R.id.xlist_item_content,item.vcDescribe)
                .setText(R.id.xlist_item_click,isMineClick ? "已学习" : "浏览" + item.nclick)
                .setText(R.id.xlist_item_time, item.dtReg);
        //!"首页广告".equals(item.vcType) ||
        if ( item.nStatus > 0) {
            holder.setImageResource(R.id.xlist_item_read_stats,R.drawable.icon_label_2);
        } else {
            holder.setImageResource(R.id.xlist_item_read_stats,R.drawable.icon_label_1);
        }
        Glide.with(mContext)
                .load(item.vcPath).transform(new GlideRoundTransform(mContext, 5)).into((ImageView) holder.getView(R.id.xlist_item_image));

    }
}
