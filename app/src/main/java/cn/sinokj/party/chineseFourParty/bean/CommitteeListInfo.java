package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/4/20.
 */

public class CommitteeListInfo {

    /**
     * result : true
     * objects : [{"nModuleType":3,"nLevel":1,"vcPartyName":"院党委","vcPartyAppName":"院党委党建App","nId":1}]
     */

    public boolean result;
    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * nModuleType : 3
         * nLevel : 1
         * vcPartyName : 院党委
         * vcPartyAppName : 院党委党建App
         * nId : 1
         */

        public int nModuleType;
        public int nLevel;
        public String vcName;
        public String vcPartyAppName;
        public int nId;
    }
}
