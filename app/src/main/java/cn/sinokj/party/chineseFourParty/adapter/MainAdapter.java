package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ArticleListActivity;
import cn.sinokj.party.chineseFourParty.activity.CorporateCultureActivity;
import cn.sinokj.party.chineseFourParty.activity.DiscussionListActivity;
import cn.sinokj.party.chineseFourParty.activity.ExamListActivity;
import cn.sinokj.party.chineseFourParty.activity.IndexImageWebActivity;
import cn.sinokj.party.chineseFourParty.activity.LoginInActivity;
import cn.sinokj.party.chineseFourParty.activity.OpinionListActivity;
import cn.sinokj.party.chineseFourParty.activity.PartyListActivity;
import cn.sinokj.party.chineseFourParty.activity.XListActivity;
import cn.sinokj.party.chineseFourParty.activity.XListTabActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.HomeDataBean;


public class MainAdapter extends BaseAdapter {
    private int nModuleType;
    private Context context;
    private List<HomeDataBean.ObjectsBean.ModuleListBean> list;
    private GridView m;
    //	201610101537放开宣传与企业文化模块
//	private String[] underConstructions = { "宣传与企业文化", "党支部考核", "党费缴纳" };
    private String[] underConstructions = {"党支部考核", "任务活动"};
    private static Map<String, OnClickListener> onClickListenerMap = new HashMap<String, OnClickListener>();
    private OnClickListener onClickListener;

    public MainAdapter(Context context, List<HomeDataBean.ObjectsBean.ModuleListBean> list, GridView m, int nModuleType) {
        this.list = list;
        this.context = context;
        this.m = m;
        initOnClickListenerMap();
        this.nModuleType = nModuleType;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        final HomeDataBean.ObjectsBean.ModuleListBean moduleBean = list.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            if (nModuleType == 3) {
                convertView = LayoutInflater.from(context).inflate(R.layout.main_item_3, null);
            } else if (nModuleType == 4) {
                convertView = LayoutInflater.from(context).inflate(R.layout.main_item_4, null);
            }
            AbsListView.LayoutParams param = new AbsListView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, m.getHeight() / 3);
            convertView.setLayoutParams(param);
            viewHolder.flagText = (TextView) convertView.findViewById(R.id.main_item_text);
            viewHolder.iconImage = (ImageView) convertView.findViewById(R.id.main_item_image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.main_item_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //第一次调用getView时，parent的高度还是0,所以这里需要判断一下，并且重新设置，否则第一个子项显示不出来
        if (convertView.getHeight() == 0) {
            ViewGroup.LayoutParams layoutParams = convertView.getLayoutParams();
            layoutParams.height = parent.getHeight() / 3;
            convertView.setLayoutParams(layoutParams);
        }
        Glide.with(context).load(moduleBean.vcIconUrl).into(viewHolder.iconImage);
        if (moduleBean.unRead < 100) {
            viewHolder.flagText.setText(moduleBean.unRead + "");
        } else {
            viewHolder.flagText.setText("99+");
        }
        if (moduleBean.unRead != 0) {
            viewHolder.flagText.setVisibility(View.VISIBLE);
        } else {
            viewHolder.flagText.setVisibility(View.GONE);
        }
        viewHolder.title.setText(moduleBean.vcModule);
        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToNext(moduleBean.vcModule, moduleBean.nTemplate, moduleBean.nId, moduleBean.vcJumpLink);
            }
        });
        return convertView;
    }

    private void jumpToNext(String vcModule, int nTemplate, int nId, String vcJumpLink) {
        System.out.println("wsj---" + vcModule + "===" + nTemplate);
        Intent intent = new Intent();
        switch (nTemplate) {
            case 1: //1是新闻
                if (!App.loginStatus && vcModule.equals("公示公告")) {
                    Toast.makeText(context, "请登录", Toast.LENGTH_SHORT).show();
                } else {
                    intent.setClass(context, XListTabActivity.class);
                    intent.putExtra("functionType", vcModule);
                    intent.putExtra("nId", nId);
                    context.startActivity(intent);
                }

                break;
            case 2:    //2是答题
                intent.setClass(context, ExamListActivity.class);
                intent.putExtra("functionType", vcModule);
                intent.putExtra("type", 2);
                context.startActivity(intent);
                break;
            case 3:    //3是网页
                intent.setClass(context, IndexImageWebActivity.class);
                intent.putExtra("url", vcJumpLink);
                intent.putExtra("icon", "");
                intent.putExtra("newsTitle", "");
                intent.putExtra("topTitle", "党费缴纳");
                intent.putExtra("nShared", 2);
                intent.putExtra("describe", "");
                intent.putExtra("isRush", true);
                context.startActivity(intent);
                break;
            case 4: //4是意见征集
                if (App.loginStatus) {
                    intent.setClass(context, OpinionListActivity.class);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "请登录", Toast.LENGTH_SHORT).show();
                }
                break;
            case 5:    //5是三会一课
                intent.setClass(context, ArticleListActivity.class);
                context.startActivity(intent);
                break;
        }
    }


    private class ViewHolder {
        private TextView flagText;
        private ImageView iconImage;
        private TextView title;
    }

    private OnClickListener getCustomListener(final String vcName) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, XListActivity.class);
                intent.putExtra("functionType", vcName);
                context.startActivity(intent);
            }
        };
    }

    private OnClickListener getUnderConstructionListener(String string) {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "敬请期待...", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void initOnClickListenerMap() {
        // 遍历列表，添加默认事件
        for (HomeDataBean.ObjectsBean.ModuleListBean mainBean : list) {
            onClickListenerMap.put(mainBean.vcModule, getCustomListener(mainBean.vcModule));
        }

        // 正在建设中 事件覆盖
        for (String string : underConstructions) {
            onClickListenerMap.put(string, getUnderConstructionListener(string));
        }

        // 在线考试
        onClickListenerMap.put("在线考试", new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExamListActivity.class);
                intent.putExtra("functionType", "在线考试");
                context.startActivity(intent);
            }
        });

        // 问卷调查
        onClickListenerMap.put("问卷调查", new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (App.loginStatus) {
                    Intent intent = new Intent(context, ExamListActivity.class);
                    intent.putExtra("functionType", "问卷调查");
                    context.startActivity(intent);
                } else {
                    context.startActivity(new Intent(context, LoginInActivity.class));
                }
            }
        });

       /* // 三会一课
        onClickListenerMap.put("三会一课", new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ArticleActivity.class);
                context.startActivity(intent);
            }
        });
*/
        /*// "掌上答题"
        onClickListenerMap.put("掌上答题", new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ExamListActivity.class);
				intent.putExtra("functionType", "掌上答题");
				context.startActivity(intent);
			}
		});*/

        // "民主评议党员"
        onClickListenerMap.put("民主评议", new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DiscussionListActivity.class);
                context.startActivity(intent);
            }
        });

        // "意见征集"
        onClickListenerMap.put("意见征集", new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OpinionListActivity.class);
                context.startActivity(intent);
            }
        });
        // 党费缴纳
        onClickListenerMap.put("党费缴纳", new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.loginStatus) {
                    Intent intent = new Intent(context, PartyListActivity.class);
                    context.startActivity(intent);
                } else {
                    context.startActivity(new Intent(context, LoginInActivity.class));
                }

            }
        });

        // 宣传与企业文化
        onClickListenerMap.put("宣传与企业文化", new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.loginStatus) {
                    Intent intent = new Intent(context, CorporateCultureActivity.class);
                    context.startActivity(intent);
                } else {
                    context.startActivity(new Intent(context, LoginInActivity.class));
                }

            }
        });
    }


    public List<HomeDataBean.ObjectsBean.ModuleListBean> getDate() {
        return list;
    }
}
