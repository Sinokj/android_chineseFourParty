package cn.sinokj.party.chineseFourParty.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.fragment.WebFragment;
import de.greenrobot.event.EventBus;


public class IndexImageWebActivity extends BaseActivity {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ll_topbar_back)
    LinearLayout llTopbarBack;
    @BindView(R.id.topbar_close)
    TextView topbarClose;
    @BindView(R.id.topbar_right_img)
    ImageButton topbarRightImg;
    @BindView(R.id.topbar_right_text)
    TextView topbarRightText;
    private int nShared;
    private WebFragment webFragment;
    private String url, icon, topTitle, describe;

    private boolean isRush = false;
    private String newsTitle;
    private boolean showClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        setContentView(R.layout.index_image_web);
        ButterKnife.bind(this);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        url = getIntent().getStringExtra("url");
        icon = getIntent().getStringExtra("icon");
        topTitle = getIntent().getStringExtra("topTitle");
        newsTitle = getIntent().getStringExtra("newsTitle");
        describe = getIntent().getStringExtra("describe");
        nShared = getIntent().getIntExtra("nShared", 0);
        isRush = getIntent().getBooleanExtra("isRush", false);
        showClose = getIntent().getBooleanExtra("showClose", false);
        topbarClose.setVisibility(View.GONE);
        title.setText(topTitle);
        title.setVisibility(View.VISIBLE);
        if (nShared != 2) {
            topbarRightImg.setVisibility(View.VISIBLE);
            topbarRightImg.setBackgroundResource(R.drawable.ic_share);
        }
        webFragment = (WebFragment) getSupportFragmentManager().findFragmentById(R.id.index_image_webview);
        webFragment.loadUrl(url);
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK, getIntent());
        if (isRush) {
            //发送消息 刷新通知公告
            Message msg = Message.obtain();
            msg.what = 001;
            EventBus.getDefault().post(msg);
        }
        super.finish();
    }

    @OnClick({R.id.ll_topbar_back, R.id.topbar_right_img, R.id.topbar_close})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.ll_topbar_back:
                if (topTitle == null || topTitle.equals("") || !topTitle.equals("党费交纳")) {
                    if (webFragment.getWebView().canGoBack()) {
                        webFragment.getWebView().goBack();
                    } else {
                        finish();
                    }
                } else {
                    finish();
                }
                break;
            case R.id.topbar_right_img:
                showShare(icon, url, topTitle, describe);
                //imageShare(Environment.getExternalStorageDirectory() + "/youxi/123.jpg", 0);
                break;
            case R.id.topbar_close:
                finish();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (topTitle == null || topTitle.equals("") || !topTitle.equals("党费交纳")) {
                if (webFragment.getWebView().canGoBack()) {
                    webFragment.getWebView().goBack();
                } else {
                    finish();
                }
            } else {
                finish();
            }
            return true;
        }
        return false;
    }

    /**
     * 微信分享单张图片
     * sendtype(0:分享到微信好友，1：分享到微信朋友圈)
     */
    public void imageShare(String imgurl, int sendtype) {
        Log.e("imgurl---", imgurl);
        File file = new File(imgurl);
        if (!file.exists()) {
            //Toast.makeText(this, "图片不存在", Toast.LENGTH_LONG).show();
        }
        WXImageObject imgObj = new WXImageObject();
        imgObj.setImagePath(imgurl);
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = imgObj;
        Bitmap bmp = BitmapFactory.decodeFile(imgurl);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150, true);
        msg.setThumbImage(thumbBmp);
        bmp.recycle();
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;
        req.scene = sendtype == 0 ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;
        App.mWxApi.sendReq(req);
    }

    private void showShare(String icon, String url, String title, String describe) {
        Log.e("wsj--icon:", icon);
        Log.e("wsj--url:", url);
        Log.e("wsj--title:", title);
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(title);
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl(url);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(describe);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setImageUrl(icon);
        oks.setUrl(url);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        //oks.setComment(title);
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite("新闻");
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(url);
        // 启动分享GUI
        oks.show(this);
    }
}
