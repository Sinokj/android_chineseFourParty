package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2018/5/4.
 */

public class ArticleBean {

    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * dtReg : 2018-05-04 11:40:47
         * nId : 1
         * nStatus : 1
         * url : http://192.168.1.232:8080/cloudPartyApp/partyarticle/getPartyArticleContent.do?nId=2
         * vcTitle : 通信事业部党支部第一次例会——支委换届选举大会
         * vcType : 支部党员大会
         */

        public String dtReg;
        public int nId;
        public int nStatus;
        public String url;
        public String vcTitle;
        public String vcType;
    }
}
