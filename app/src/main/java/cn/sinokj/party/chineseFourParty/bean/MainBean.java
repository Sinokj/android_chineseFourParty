package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * 主界面Model
 * @author azzbcc
 *
 */
@SuppressWarnings("serial")
public class MainBean implements Serializable {
	@Expose
	private String vcName;
	@Expose
	private int unRead;
	@Expose
	private int nId;
	@Expose
	private int resource;

	public MainBean(String vcName, int resource) {
		this.vcName = vcName;
		this.resource = resource;
	}

	public String getVcName() {
		return vcName;
	}

	public void setVcName(String vcName) {
		this.vcName = vcName;
	}

	public int getUnRead() {
		return unRead;
	}

	public void setUnRead(int unRead) {
		this.unRead = unRead;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public int getResource() {
		return resource;
	}

	public void setResource(int resource) {
		this.resource = resource;
	}
}
