/**
 * @Title MineInfo.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午3:17:17
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName MineInfo
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午3:17:17
 **/
@SuppressWarnings("serial")
public class MineInfo implements Serializable {

	/**
	 * PalmExam : [{"answerdNumber":4,"dtReg":"2017-04-17 09:37:21","examedNumber":99,"isBegin":2,"isPass":false,"nId":21,"nPass":60,"nScore":10,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年4月 学系列讲话"},{"answerdNumber":4,"dtReg":"2017-03-28 17:24:03","examedNumber":94,"isBegin":2,"isPass":false,"nId":18,"nPass":60,"nScore":30,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年1月 学党章党规"},{"answerdNumber":4,"dtReg":"2017-03-28 17:52:24","examedNumber":94,"isBegin":2,"isPass":false,"nId":19,"nPass":60,"nScore":20,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年2月 学党章党规"},{"answerdNumber":6,"dtReg":"2017-03-28 18:03:12","examedNumber":92,"isBegin":2,"isPass":false,"nId":20,"nPass":60,"nScore":30,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年3月 学系列讲话"},{"answerdNumber":0,"dtReg":"2017-06-01 09:30:19","examedNumber":88,"isBegin":1,"isPass":false,"nId":25,"nPass":60,"nScore":0,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年6月 学党章党规"},{"answerdNumber":0,"dtReg":"2017-05-03 10:30:53","examedNumber":117,"isBegin":2,"isPass":false,"nId":24,"nPass":60,"nScore":0,"nTimer":30,"nTopicsTotal":10,"nTotal":100,"vcClassify":"掌上答题","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"wanyuanshiye","vcTitle":"2017年5月 学党章党规"}]
	 * PocketTutorPoint : 12.73
	 * UncorruptedExam : [{"answerdNumber":1,"dtReg":"2017-01-09 17:15:24","examedNumber":73,"isBegin":2,"isPass":false,"nId":15,"nPass":60,"nScore":5,"nTimer":30,"nTopicsTotal":20,"nTotal":100,"vcClassify":"廉政考试","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"admin","vcTitle":"《准则》与《条例》判断题"},{"answerdNumber":0,"dtReg":"2017-01-09 17:28:34","examedNumber":63,"isBegin":2,"isPass":false,"nId":16,"nPass":40,"nScore":0,"nTimer":30,"nTopicsTotal":7,"nTotal":70,"vcClassify":"廉政考试","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,9,19,20,21,26,42,43,44,45,46,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,34,74,75,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,天津新区运行保障事业部党支部,管理业务小组,技术业务小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"admin","vcTitle":"《准则》与《条例》多选题"},{"answerdNumber":1,"dtReg":"2016-10-11 14:51:40","examedNumber":81,"isBegin":2,"isPass":false,"nId":13,"nPass":60,"nScore":0,"nTimer":30,"nTopicsTotal":20,"nTotal":100,"vcClassify":"廉政考试","vcDept":"1","vcExamGroupId":"1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,","vcExamGroupName":"公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,","vcRegister":"admin","vcTitle":"廉政准则条例考题"}]
	 * about : http://218.244.60.138:80/eDaJia/views/about.jsp
	 * baseInfo : {"nId":694,"nPalmAnswerPoint":0,"nPocketTutorPoint":12.73,"vcHeadImgUrl":"http://192.168.1.7:8080/eDaJia/upload/image/head/129d601a-89d3-4dce-9aef-0bfcc34dd29a.jpg","vcLevel":"小学","vcName":"罗","vcTel":"18611182356"}
	 * discussions : [{"title":[{"nOptionScore":10,"nScore":10,"vcOption":"是否具有坚定的共产主义信念，能否坚持四项基本原则，坚持改革开放，把实现现阶段的共同理想同脚踏实地地做好本职工作结合起来，全心全意为人民服务。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否坚决贯彻执行党在社会主义初级阶段的基本路线和各项方针、政策，在政治上同党中央保持一致，为推动生产力的发展和社会主义精神文明建设作出贡献"},{"nOptionScore":10,"nScore":10,"vcOption":"是否站在改革的前列，维护改革的大局，正确处理国家、集体、个人利益之间的关系，做到个人利益服从党和人民的利益，局部利益服从整体利益"},{"nOptionScore":10,"nScore":10,"vcOption":"是否切实地执行党的决议，严守党纪、政纪、国法，坚决做到令行禁止"},{"nOptionScore":10,"nScore":10,"vcOption":"是否密切联系群众，关心群众疾苦，艰苦奋斗，廉洁奉公，在个人利益同党和人民的利益发生矛盾时，自觉地牺牲个人利益"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够自觉落实公司党委关于\u201c两学一做\u201d学习教育各项要求，从实际出发，做到学用结合，知行合一"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够按时、主动、足额交纳党费。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够发挥先锋模范作用，主动参与并解决工作中的\u201c急、难、险、重\u201d问题，带头参加公司及本单位的各项工作，面对组织分配的工作不讲条件、不找借口。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够关心集体，团结协作，与团队成员共同完成工作任务，组织评价高"},{"nOptionScore":10,"nScore":10,"vcOption":"是否具有大局意识，保质保量完成组织上安排的工作，对公司和本单位发展做出贡献，廉洁奉公、勇于奉献，在公司或本单位具有较高的威信。"}],"nId":67,"nScore":100,"vcBeGreadPerson":"13811768588","vcName":"刘宁","vcTel":"13811768588","vcTitle":"2016民主评议党员参考模板"}]
	 * exam : [{"isPass":false,"nId":21,"nPass":60,"nScore":10,"vcTitle":"2017年4月 学系列讲话"},{"isPass":false,"nId":15,"nPass":60,"nScore":5,"vcTitle":"《准则》与《条例》判断题"},{"isPass":false,"nId":16,"nPass":40,"nScore":0,"vcTitle":"《准则》与《条例》多选题"},{"isPass":false,"nId":13,"nPass":60,"nScore":0,"vcTitle":"廉政准则条例考题"},{"isPass":false,"nId":18,"nPass":60,"nScore":30,"vcTitle":"2017年1月 学党章党规"},{"isPass":false,"nId":19,"nPass":60,"nScore":20,"vcTitle":"2017年2月 学党章党规"},{"isPass":false,"nId":20,"nPass":60,"nScore":30,"vcTitle":"2017年3月 学系列讲话"},{"isPass":false,"nId":25,"nPass":60,"nScore":0,"vcTitle":"2017年6月 学党章党规"},{"isPass":false,"nId":24,"nPass":60,"nScore":0,"vcTitle":"2017年5月 学党章党规"}]
	 * level : {"nLevel":"1","vcLevel":"小学"}
	 * nLevelPoint : 12.73
*/
	private double PocketTutorPoint;
	private String about;
	private BaseInfoBean baseInfo;
	private LevelBean level;
	private double nLevelPoint;
	private List<ExamTopic> PalmExam;

	private List<ExamTopic> UncorruptedExam;
	private List<DiscussedTopics> discussions;
	private List<ExamBean> exam;

	public double getPocketTutorPoint() {
		return PocketTutorPoint;
	}

	public void setPocketTutorPoint(double PocketTutorPoint) {
		this.PocketTutorPoint = PocketTutorPoint;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public BaseInfoBean getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(BaseInfoBean baseInfo) {
		this.baseInfo = baseInfo;
	}

	public LevelBean getLevel() {
		return level;
	}

	public void setLevel(LevelBean level) {
		this.level = level;
	}

	public double getNLevelPoint() {
		return nLevelPoint;
	}

	public void setNLevelPoint(double nLevelPoint) {
		this.nLevelPoint = nLevelPoint;
	}

	public List<ExamTopic> getPalmExam() {
		return PalmExam;
	}

	public void setPalmExam(List<ExamTopic> PalmExam) {
		this.PalmExam = PalmExam;
	}

	public List<ExamTopic> getUncorruptedExam() {
		return UncorruptedExam;
	}

	public void setUncorruptedExam(List<ExamTopic> UncorruptedExam) {
		this.UncorruptedExam = UncorruptedExam;
	}

	public List<DiscussedTopics> getDiscussions() {
		return discussions;
	}

	public void setDiscussions(List<DiscussedTopics> discussions) {
		this.discussions = discussions;
	}

	public List<ExamBean> getExam() {
		return exam;
	}

	public void setExam(List<ExamBean> exam) {
		this.exam = exam;
	}

	public static class BaseInfoBean  implements Serializable{
		/**
		 * nId : 694
		 * nPalmAnswerPoint : 0
		 * nPocketTutorPoint : 12.73
		 * vcHeadImgUrl : http://192.168.1.7:8080/eDaJia/upload/image/head/129d601a-89d3-4dce-9aef-0bfcc34dd29a.jpg
		 * vcLevel : 小学
		 * vcName : 罗
		 * vcTel : 18611182356
		 */

		private int nId;
		private double nPalmAnswerPoint;
		private double nPocketTutorPoint;
		private String vcHeadImgUrl;
		private String vcLevel;
		private String vcName;
		private String vcTel;

        public String getVcDeptName() {
            return vcDeptName;
        }

        public void setVcDeptName(String vcDeptName) {
            this.vcDeptName = vcDeptName;
        }

        private String vcDeptName;

		public int getNId() {
			return nId;
		}

		public void setNId(int nId) {
			this.nId = nId;
		}

		public double getNPalmAnswerPoint() {
			return nPalmAnswerPoint;
		}

		public void setNPalmAnswerPoint(double nPalmAnswerPoint) {
			this.nPalmAnswerPoint = nPalmAnswerPoint;
		}

		public double getNPocketTutorPoint() {
			return nPocketTutorPoint;
		}

		public void setNPocketTutorPoint(double nPocketTutorPoint) {
			this.nPocketTutorPoint = nPocketTutorPoint;
		}

		public String getVcHeadImgUrl() {
			return vcHeadImgUrl;
		}

		public void setVcHeadImgUrl(String vcHeadImgUrl) {
			this.vcHeadImgUrl = vcHeadImgUrl;
		}

		public String getVcLevel() {
			return vcLevel;
		}

		public void setVcLevel(String vcLevel) {
			this.vcLevel = vcLevel;
		}

		public String getVcName() {
			return vcName;
		}

		public void setVcName(String vcName) {
			this.vcName = vcName;
		}

		public String getVcTel() {
			return vcTel;
		}

		public void setVcTel(String vcTel) {
			this.vcTel = vcTel;
		}
	}

	public static class LevelBean {
		/**
		 * nLevel : 1
		 * vcLevel : 小学
		 */

		private int nLevel;
		private String vcLevel;

		public int getNLevel() {
			return nLevel;
		}

		public void setNLevel(int nLevel) {
			this.nLevel = nLevel;
		}

		public String getVcLevel() {
			return vcLevel;
		}

		public void setVcLevel(String vcLevel) {
			this.vcLevel = vcLevel;
		}
	}

	public static class PalmExamBean implements Serializable{
		/**
		 * answerdNumber : 4
		 * dtReg : 2017-04-17 09:37:21
		 * examedNumber : 99
		 * isBegin : 2
		 * isPass : false
		 * nId : 21
		 * nPass : 60
		 * nScore : 10
		 * nTimer : 30
		 * nTopicsTotal : 10
		 * nTotal : 100
		 * vcClassify : 掌上答题
		 * vcDept : 1
		 * vcExamGroupId : 1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,
		 * vcExamGroupName : 公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,
		 * vcRegister : wanyuanshiye
		 * vcTitle : 2017年4月 学系列讲话
		 */

		private int answerdNumber;
		private String dtReg;
		private int examedNumber;
		private int isBegin;
		private boolean isPass;
		private int nId;
		private int nPass;
		private int nScore;
		private int nTimer;
		private int nTopicsTotal;
		private int nTotal;
		private String vcClassify;
		private String vcDept;
		private String vcExamGroupId;
		private String vcExamGroupName;
		private String vcRegister;
		private String vcTitle;

		public int getAnswerdNumber() {
			return answerdNumber;
		}

		public void setAnswerdNumber(int answerdNumber) {
			this.answerdNumber = answerdNumber;
		}

		public String getDtReg() {
			return dtReg;
		}

		public void setDtReg(String dtReg) {
			this.dtReg = dtReg;
		}

		public int getExamedNumber() {
			return examedNumber;
		}

		public void setExamedNumber(int examedNumber) {
			this.examedNumber = examedNumber;
		}

		public int getIsBegin() {
			return isBegin;
		}

		public void setIsBegin(int isBegin) {
			this.isBegin = isBegin;
		}

		public boolean isIsPass() {
			return isPass;
		}

		public void setIsPass(boolean isPass) {
			this.isPass = isPass;
		}

		public int getNId() {
			return nId;
		}

		public void setNId(int nId) {
			this.nId = nId;
		}

		public int getNPass() {
			return nPass;
		}

		public void setNPass(int nPass) {
			this.nPass = nPass;
		}

		public int getNScore() {
			return nScore;
		}

		public void setNScore(int nScore) {
			this.nScore = nScore;
		}

		public int getNTimer() {
			return nTimer;
		}

		public void setNTimer(int nTimer) {
			this.nTimer = nTimer;
		}

		public int getNTopicsTotal() {
			return nTopicsTotal;
		}

		public void setNTopicsTotal(int nTopicsTotal) {
			this.nTopicsTotal = nTopicsTotal;
		}

		public int getNTotal() {
			return nTotal;
		}

		public void setNTotal(int nTotal) {
			this.nTotal = nTotal;
		}

		public String getVcClassify() {
			return vcClassify;
		}

		public void setVcClassify(String vcClassify) {
			this.vcClassify = vcClassify;
		}

		public String getVcDept() {
			return vcDept;
		}

		public void setVcDept(String vcDept) {
			this.vcDept = vcDept;
		}

		public String getVcExamGroupId() {
			return vcExamGroupId;
		}

		public void setVcExamGroupId(String vcExamGroupId) {
			this.vcExamGroupId = vcExamGroupId;
		}

		public String getVcExamGroupName() {
			return vcExamGroupName;
		}

		public void setVcExamGroupName(String vcExamGroupName) {
			this.vcExamGroupName = vcExamGroupName;
		}

		public String getVcRegister() {
			return vcRegister;
		}

		public void setVcRegister(String vcRegister) {
			this.vcRegister = vcRegister;
		}

		public String getVcTitle() {
			return vcTitle;
		}

		public void setVcTitle(String vcTitle) {
			this.vcTitle = vcTitle;
		}
	}

	public static class UncorruptedExamBean  implements Serializable{
		/**
		 * answerdNumber : 1
		 * dtReg : 2017-01-09 17:15:24
		 * examedNumber : 73
		 * isBegin : 2
		 * isPass : false
		 * nId : 15
		 * nPass : 60
		 * nScore : 5
		 * nTimer : 30
		 * nTopicsTotal : 20
		 * nTotal : 100
		 * vcClassify : 廉政考试
		 * vcDept : 1
		 * vcExamGroupId : 1,80,81,2,3,4,7,16,17,5,6,22,23,24,25,8,82,9,19,20,21,26,42,43,44,45,46,84,27,50,51,52,53,54,55,56,57,58,28,59,60,61,62,63,64,29,65,66,30,67,68,31,69,70,10,32,33,71,73,35,18,37,38,39,40,41,79,36,47,48,49,
		 * vcExamGroupName : 公司党委,院领导,院领导临时小组,建筑公司党支部,建筑第一党小组,建筑第二党小组,建筑第三党小组,建筑第四党小组,建筑第五党小组,航天科研后勤保障中心党支部,办公室综合党小组,长征宾馆党小组,小车队党小组,汽车一队、租赁联合党小组,汽服公司、餐饮部联合党小组,物业公司党支部,天津运行保障党小组,物业第一党小组,物业第二党小组,物业第三党小组,物业第四党小组,通信事业部党支部,综合管理党小组,技术业务党小组,系统运营党小组,传输工程党小组,客户服务党小组,测试组,动力事业部党支部,综合一党小组,综合二党小组,综合三党小组,供水党小组,供暖党小组,供电党小组,燃气党小组,地热党小组,项目中心党小组,幼儿园党支部,第一党小组,第二党小组,第三党小组,第四党小组,第五党小组,第六党小组,商业地产运营部党支部,第一党小组,第二党小组,航天博物馆党支部,第一党小组,第二党小组,监理处党支部,第一党小组,第二党小组,深万源党支部,设计院党支部,绿化公司党支部,第一党小组,第二党小组,大连公司党支部,公司本部党支部,公司本级第一党小组,公司本级第二党小组,公司本级第三党小组,公司本级第四党小组,公司本级第五党小组,公司本级第六党小组,退休人员党总支,退休人员第一党支部,退休人员第二党支部,退休人员第三党支部,
		 * vcRegister : admin
		 * vcTitle : 《准则》与《条例》判断题
		 */

		private int answerdNumber;
		private String dtReg;
		private int examedNumber;
		private int isBegin;
		private boolean isPass;
		private int nId;
		private int nPass;
		private int nScore;
		private int nTimer;
		private int nTopicsTotal;
		private int nTotal;
		private String vcClassify;
		private String vcDept;
		private String vcExamGroupId;
		private String vcExamGroupName;
		private String vcRegister;
		private String vcTitle;

		public int getAnswerdNumber() {
			return answerdNumber;
		}

		public void setAnswerdNumber(int answerdNumber) {
			this.answerdNumber = answerdNumber;
		}

		public String getDtReg() {
			return dtReg;
		}

		public void setDtReg(String dtReg) {
			this.dtReg = dtReg;
		}

		public int getExamedNumber() {
			return examedNumber;
		}

		public void setExamedNumber(int examedNumber) {
			this.examedNumber = examedNumber;
		}

		public int getIsBegin() {
			return isBegin;
		}

		public void setIsBegin(int isBegin) {
			this.isBegin = isBegin;
		}

		public boolean isIsPass() {
			return isPass;
		}

		public void setIsPass(boolean isPass) {
			this.isPass = isPass;
		}

		public int getNId() {
			return nId;
		}

		public void setNId(int nId) {
			this.nId = nId;
		}

		public int getNPass() {
			return nPass;
		}

		public void setNPass(int nPass) {
			this.nPass = nPass;
		}

		public int getNScore() {
			return nScore;
		}

		public void setNScore(int nScore) {
			this.nScore = nScore;
		}

		public int getNTimer() {
			return nTimer;
		}

		public void setNTimer(int nTimer) {
			this.nTimer = nTimer;
		}

		public int getNTopicsTotal() {
			return nTopicsTotal;
		}

		public void setNTopicsTotal(int nTopicsTotal) {
			this.nTopicsTotal = nTopicsTotal;
		}

		public int getNTotal() {
			return nTotal;
		}

		public void setNTotal(int nTotal) {
			this.nTotal = nTotal;
		}

		public String getVcClassify() {
			return vcClassify;
		}

		public void setVcClassify(String vcClassify) {
			this.vcClassify = vcClassify;
		}

		public String getVcDept() {
			return vcDept;
		}

		public void setVcDept(String vcDept) {
			this.vcDept = vcDept;
		}

		public String getVcExamGroupId() {
			return vcExamGroupId;
		}

		public void setVcExamGroupId(String vcExamGroupId) {
			this.vcExamGroupId = vcExamGroupId;
		}

		public String getVcExamGroupName() {
			return vcExamGroupName;
		}

		public void setVcExamGroupName(String vcExamGroupName) {
			this.vcExamGroupName = vcExamGroupName;
		}

		public String getVcRegister() {
			return vcRegister;
		}

		public void setVcRegister(String vcRegister) {
			this.vcRegister = vcRegister;
		}

		public String getVcTitle() {
			return vcTitle;
		}

		public void setVcTitle(String vcTitle) {
			this.vcTitle = vcTitle;
		}
	}

	public static class DiscussionsBean implements Serializable{
		/**
		 * title : [{"nOptionScore":10,"nScore":10,"vcOption":"是否具有坚定的共产主义信念，能否坚持四项基本原则，坚持改革开放，把实现现阶段的共同理想同脚踏实地地做好本职工作结合起来，全心全意为人民服务。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否坚决贯彻执行党在社会主义初级阶段的基本路线和各项方针、政策，在政治上同党中央保持一致，为推动生产力的发展和社会主义精神文明建设作出贡献"},{"nOptionScore":10,"nScore":10,"vcOption":"是否站在改革的前列，维护改革的大局，正确处理国家、集体、个人利益之间的关系，做到个人利益服从党和人民的利益，局部利益服从整体利益"},{"nOptionScore":10,"nScore":10,"vcOption":"是否切实地执行党的决议，严守党纪、政纪、国法，坚决做到令行禁止"},{"nOptionScore":10,"nScore":10,"vcOption":"是否密切联系群众，关心群众疾苦，艰苦奋斗，廉洁奉公，在个人利益同党和人民的利益发生矛盾时，自觉地牺牲个人利益"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够自觉落实公司党委关于\u201c两学一做\u201d学习教育各项要求，从实际出发，做到学用结合，知行合一"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够按时、主动、足额交纳党费。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够发挥先锋模范作用，主动参与并解决工作中的\u201c急、难、险、重\u201d问题，带头参加公司及本单位的各项工作，面对组织分配的工作不讲条件、不找借口。"},{"nOptionScore":10,"nScore":10,"vcOption":"是否能够关心集体，团结协作，与团队成员共同完成工作任务，组织评价高"},{"nOptionScore":10,"nScore":10,"vcOption":"是否具有大局意识，保质保量完成组织上安排的工作，对公司和本单位发展做出贡献，廉洁奉公、勇于奉献，在公司或本单位具有较高的威信。"}]
		 * nId : 67
		 * nScore : 100
		 * vcBeGreadPerson : 13811768588
		 * vcName : 刘宁
		 * vcTel : 13811768588
		 * vcTitle : 2016民主评议党员参考模板
		 */

		private int nId;
		private int nScore;
		private String vcBeGreadPerson;
		private String vcName;
		private String vcTel;
		private String vcTitle;
		private List<ContentBean> content;

		public int getNId() {
			return nId;
		}

		public void setNId(int nId) {
			this.nId = nId;
		}

		public int getNScore() {
			return nScore;
		}

		public void setNScore(int nScore) {
			this.nScore = nScore;
		}

		public String getVcBeGreadPerson() {
			return vcBeGreadPerson;
		}

		public void setVcBeGreadPerson(String vcBeGreadPerson) {
			this.vcBeGreadPerson = vcBeGreadPerson;
		}

		public String getVcName() {
			return vcName;
		}

		public void setVcName(String vcName) {
			this.vcName = vcName;
		}

		public String getVcTel() {
			return vcTel;
		}

		public void setVcTel(String vcTel) {
			this.vcTel = vcTel;
		}

		public String getVcTitle() {
			return vcTitle;
		}

		public void setVcTitle(String vcTitle) {
			this.vcTitle = vcTitle;
		}

		public List<ContentBean> getContent() {
			return content;
		}

		public void setContent(List<ContentBean> content) {
			this.content = content;
		}

		public static class ContentBean implements Serializable{
			/**
			 * nOptionScore : 10
			 * nScore : 10
			 * vcOption : 是否具有坚定的共产主义信念，能否坚持四项基本原则，坚持改革开放，把实现现阶段的共同理想同脚踏实地地做好本职工作结合起来，全心全意为人民服务。
			 */

			private int nOptionScore;
			private int nScore;
			private String vcOption;

			public int getNOptionScore() {
				return nOptionScore;
			}

			public void setNOptionScore(int nOptionScore) {
				this.nOptionScore = nOptionScore;
			}

			public int getNScore() {
				return nScore;
			}

			public void setNScore(int nScore) {
				this.nScore = nScore;
			}

			public String getVcOption() {
				return vcOption;
			}

			public void setVcOption(String vcOption) {
				this.vcOption = vcOption;
			}
		}
	}

	public static class ExamBean {
		/**
		 * isPass : false
		 * nId : 21
		 * nPass : 60
		 * nScore : 10
		 * vcTitle : 2017年4月 学系列讲话
		 */

		private boolean isPass;
		private int nId;
		private int nPass;
		private int nScore;
		private String vcTitle;

		public boolean isIsPass() {
			return isPass;
		}

		public void setIsPass(boolean isPass) {
			this.isPass = isPass;
		}

		public int getNId() {
			return nId;
		}

		public void setNId(int nId) {
			this.nId = nId;
		}

		public int getNPass() {
			return nPass;
		}

		public void setNPass(int nPass) {
			this.nPass = nPass;
		}

		public int getNScore() {
			return nScore;
		}

		public void setNScore(int nScore) {
			this.nScore = nScore;
		}

		public String getVcTitle() {
			return vcTitle;
		}

		public void setVcTitle(String vcTitle) {
			this.vcTitle = vcTitle;
		}
	}
}
