package cn.sinokj.party.chineseFourParty.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONObject;

import java.util.Map;

import cn.sinokj.party.chineseFourParty.activity.PartyResultActivity;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import de.greenrobot.event.EventBus;


/**
 * Created by l on 2017/7/7.
 */

public class WXPayEntryActivity extends BaseActivity implements IWXAPIEventHandler {

    private IWXAPI wxapi;
    private int requestTimes = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wxapi = WXAPIFactory.createWXAPI(this, HttpConstants.WECHAT_APP_ID);
        wxapi.handleIntent(getIntent(),this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {
        if (baseResp.errCode == 0) {//支付成功
            new Thread(new LoadDataThread()).start();
        }else if (baseResp.errCode == -1) {//支付失败
            Toast.makeText(getApplicationContext(), "支付失败", Toast.LENGTH_SHORT).show();
            finish();
        }else {//取消
            Toast.makeText(getApplicationContext(), "支付取消", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        return HttpDataService.wxGetPayResult(App.nWeixinOrderId);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        super.httpHandlerResultData(msg, jsonObject);
        if (jsonObject.optInt("nRes") != 1) {
            if(requestTimes <3){
                new Thread(new LoadDataThread()).start();
            }else {
                Toast.makeText(this, jsonObject.optString("vcRes"), Toast.LENGTH_SHORT).show();
                finish();
            }
            return;
        }
        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
        Toast.makeText(WXPayEntryActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(WXPayEntryActivity.this, PartyResultActivity.class);
        intent.putExtra("nMoney", App.nMoney);
        startActivity(intent);
        Message msg1 = Message.obtain();
        msg1.what = HttpConstants.ACTIVITY_FINISH;
        EventBus.getDefault().post(msg1);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        requestTimes = 0;
    }
}
