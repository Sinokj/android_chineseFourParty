package cn.sinokj.party.chineseFourParty.utils.time;

import android.content.Context;
import android.widget.Button;

import cn.sinokj.party.chineseFourParty.R;


/**
 * 按钮倒计时工具类
 * @author azzbcc E-mail: azzbcc@sina.com
 * @version 创建时间：2015年12月22日 上午11:06:32
 */
public class TimeCountButtonUtil extends AdvancedCountdownTimer {
	@SuppressWarnings("unused")
	private Context mContext;
	private Button btn;// 按钮

	/**
	 * @param context 父Activity
	 * @param count 倒计时次数
	 * @param countDownInterval 倒计时时间间隔(微秒)
	 * @param btn 按钮
	 */
	public TimeCountButtonUtil(Context context, long count, long countDownInterval, Button btn) {
		super(count * countDownInterval, countDownInterval);
		this.mContext = context;
		this.btn = btn;
	}

	@Override
	public void onTick(long millisUntilFinished, int percent) {
		btn.setClickable(false);// 设置不能点击
		btn.setText(millisUntilFinished / 1000 + "秒后可重新发送");// 设置倒计时时间

		btn.setBackgroundResource(R.drawable.login_bg_h);
	}

	@Override
	public void onFinish() {
		btn.setText("重新获取验证码");
		btn.setClickable(true);// 重新获得点击
		btn.setBackgroundResource(R.drawable.login_bg_n);
	}
}