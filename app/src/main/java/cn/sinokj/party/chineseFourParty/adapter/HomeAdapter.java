package cn.sinokj.party.chineseFourParty.adapter;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.util.MultiTypeDelegate;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ArticleListActivity;
import cn.sinokj.party.chineseFourParty.activity.ExamListActivity;
import cn.sinokj.party.chineseFourParty.activity.IndexImageWebActivity;
import cn.sinokj.party.chineseFourParty.activity.OpinionListActivity;
import cn.sinokj.party.chineseFourParty.activity.XListTabActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.HomeDataBean;

/**
 * Created by l on 2018/5/31.
 */
public class HomeAdapter extends BaseQuickAdapter<HomeDataBean.ObjectsBean.ModuleListBean, BaseViewHolder> {

    private static final int TYPE_THREE = 3;
    private static final int TYPE_FOUR = 4;
    private int nModuleType;
    private int rvHomeLeftheight;


    public HomeAdapter(int rvHomeLeftheight, @Nullable List<HomeDataBean.ObjectsBean.ModuleListBean> data, final int nModuleType) {
        super(data);
        this.rvHomeLeftheight = rvHomeLeftheight;
        this.nModuleType = nModuleType;
        setMultiTypeDelegate(new MultiTypeDelegate<HomeDataBean.ObjectsBean.ModuleListBean>() {
            @Override
            protected int getItemType(HomeDataBean.ObjectsBean.ModuleListBean moduleListBean) {
                if (nModuleType == 3) {
                    return TYPE_THREE;
                } else if (nModuleType == 4) {
                    return TYPE_FOUR;
                }
                return 0;
            }
        });

        getMultiTypeDelegate().registerItemType(TYPE_THREE, R.layout.main_item_3)
                .registerItemType(TYPE_FOUR, R.layout.main_item_4);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeDataBean.ObjectsBean.ModuleListBean moduleBean) {
        helper.itemView.setLayoutParams(getlayoutParams(helper.getLayoutPosition()));

        Glide.with(mContext).load(moduleBean.vcIconUrl).into((ImageView) helper.getView(R.id.main_item_image));
        if (moduleBean.unRead < 100) {
            helper.setText(R.id.main_item_text, moduleBean.unRead + "");
        } else {
            helper.setText(R.id.main_item_text, "99+");
        }

        if (moduleBean.unRead != 0) {
            helper.setVisible(R.id.main_item_text, true);
        } else {
            helper.setVisible(R.id.main_item_text, false);
        }
        helper.setText(R.id.main_item_title, moduleBean.vcModule);

    }

    private RecyclerView.LayoutParams getlayoutParams(int position) {
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, rvHomeLeftheight / 3);
        if (position <= nModuleType) {
            layoutParams.bottomMargin = 1;
        } else if (position > mData.size() - nModuleType) {
            layoutParams.topMargin = 1;
        } else {
            layoutParams.bottomMargin = 1;
            layoutParams.topMargin = 1;
        }

        if (position % nModuleType != 0) {
            layoutParams.rightMargin = 1;
        } else if (position % nModuleType == nModuleType - 1) {
            layoutParams.leftMargin = 1;
        } else {
            layoutParams.leftMargin = 1;
            layoutParams.rightMargin = 1;
        }
        return layoutParams;
    }

   }
