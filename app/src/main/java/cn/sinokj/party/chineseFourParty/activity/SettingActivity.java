/**
 * @Title SettingActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月6日 下午2:58:47
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.Version;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.utils.Constans;
import cn.sinokj.party.chineseFourParty.utils.Utils;
import cn.sinokj.party.chineseFourParty.utils.update.UpdateManager;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;
import cn.sinokj.party.chineseFourParty.view.dialog.confirm.ConfirmDialog;
import cn.sinokj.party.chineseFourParty.view.dialog.confirm.ConfirmInterface;
import de.greenrobot.event.EventBus;

/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName SettingActivity
 * @Description 设置界面
 * @date 创建时间 2016年7月6日 下午2:58:47
 **/
public class SettingActivity extends BaseActivity {
    private static final int LOGOUT = 1;
    private static final int UPDATE = 2;
    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public ImageButton topLeftImage;
    @BindView(R.id.setting_switch_voice)
    public Switch settingSwitchVoice;
    @BindView(R.id.setting_switch_shock)
    public Switch settingSwitchShock;
    @BindView(R.id.setting_version_text)
    public TextView versionNewText;

    private Version version;
    private String about, localVersion;
    SharedPreferences SwitchVoiceSP, SwitchShockSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        ButterKnife.bind(this);
        SwitchVoiceSP = getSharedPreferences("SwitchVoice", Activity.MODE_PRIVATE);
        SwitchShockSP = getSharedPreferences("SwitchShockSP", Activity.MODE_PRIVATE);
        // 获取Editor对象
        final SharedPreferences.Editor SwitchVoiceEditor = SwitchVoiceSP.edit();
        final SharedPreferences.Editor SwitchShockSPEditor = SwitchShockSP.edit();
        initCheckStatu();
        about = getIntent().getStringExtra("about");
        titleText.setText("系统设置");
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);

        localVersion = Utils.getAppVersionName(this);
        versionNewText.setText("v" + localVersion);

        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(UPDATE)).start();
        //是否选中的监听事件
        settingSwitchVoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SwitchVoiceEditor.putBoolean("voice", isChecked);
                SwitchVoiceEditor.commit();
            }
        });

        settingSwitchShock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SwitchShockSPEditor.putBoolean("shock", isChecked);
                SwitchShockSPEditor.commit();
            }
        });
    }

    private void initCheckStatu() {
        if (SwitchVoiceSP.getBoolean("voice", false)) {
            settingSwitchVoice.setChecked(true);
        } else {
            settingSwitchVoice.setChecked(false);
        }
        //-----
        if (SwitchShockSP.getBoolean("shock", false)) {
            settingSwitchShock.setChecked(true);
        } else {
            settingSwitchShock.setChecked(false);
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        switch (what) {
            case LOGOUT:
                return HttpDataService.logout();
            case UPDATE:
                return HttpDataService.update();
        }
        return super.getDataFunction(what, arg1, arg2, obj);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        DialogShow.closeDialog();
        switch (msg.what) {
            case LOGOUT:
                if (jsonObject.optBoolean(HttpConstants.RESULT)) {
                    afterlogout();
                }
                break;
            case UPDATE:
                version = new Gson().fromJson(jsonObject.toString(), Version.class);
                break;
        }
    }

    private void afterlogout() {
        App.loginStatus = false;
        App.MINE_REFRESH = false;
        App.nCommitteeId = "2";
        App.loginInfo = null;
       /* Message msg = Message.obtain();
        msg.what = Constans.SHOW_HOME;
        EventBus.getDefault().post(msg);
        Message msg2 = Message.obtain();
        msg2.what = Constans.HOME_REFRESH;
        EventBus.getDefault().post(msg2);*/
        //要闻刷新
       /* Message msg4 = Message.obtain();
        msg4.what = Constans.ANNOUNCEMENT_REFRESH;
        EventBus.getDefault().post(msg4);*/
        startActivity(new Intent(this, LoginInActivity.class));
        finish();
    }

    @OnClick({R.id.topbar_left_img, R.id.setting_change_password, R.id.setting_logout, R.id.setting_about})
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
            case R.id.setting_change_password:
                intent.setClass(this, ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.setting_logout:
                new Thread(new LoadDataThread(LOGOUT)).start();
                break;
            case R.id.setting_update:
                if (version != null) {
                    if (version.equals(localVersion)) {
                        ConfirmDialog.confirmAction(this, "您已是最新版本", "确定", null, null);
                    } else {
                        ConfirmDialog.confirmAction(this, "最新版本为v" + localVersion + "，是否升级", "升级", "取消",
                                new ConfirmInterface() {
                                    @Override
                                    public void onOkButton() {
                                        UpdateManager updateManager = new UpdateManager(SettingActivity.this,
                                                version.vcUrl);
                                        updateManager.showDownloadDialog();
                                    }

                                    @Override
                                    public void onCancelButton() {
                                    }

                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                    }
                }
                break;
            case R.id.setting_about:
                intent.setClass(this, IndexImageWebActivity.class);
                intent.putExtra("url", about);
                intent.putExtra("title", "关于我们");
                startActivity(intent);
        }
    }
}
