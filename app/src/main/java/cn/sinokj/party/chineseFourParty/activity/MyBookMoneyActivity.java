package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.MyBookMoneyAdapyer;
import cn.sinokj.party.chineseFourParty.bean.MyBookMoneyBean;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


/**
 * Created by l on 2018/11/8.
 */
public class MyBookMoneyActivity extends BaseActivity {
    private static final int INIT_LIST_DATA = 101;
    private static final int INIT_BOOKMONEY = 100;
    @BindView(R.id.topbar_left_img)
    ImageView topbarLeftImg;
    @BindView(R.id.tutor_ll)
    RelativeLayout tutorLl;
    @BindView(R.id.tutor_dialog)
    TextView tutorDialog;
    @BindView(R.id.tv_mybookmoney)
    TextView tvMybookmoney;
    @BindView(R.id.rl_mybookmoney)
    RecyclerView rlMybookmoney;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mybookmoney);
        ButterKnife.bind(this);

        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(INIT_BOOKMONEY)).start();
        new Thread(new LoadDataThread(INIT_LIST_DATA)).start();
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        switch (what) {
            case INIT_BOOKMONEY:
                return HttpDataService.getMyTokenMoney();
            case INIT_LIST_DATA:
                return HttpDataService.getMyTokenPayLog();
        }
        return super.getDataFunction(what, arg1, arg2, obj);

    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        super.httpHandlerResultData(msg, jsonObject);
        DialogShow.closeDialog();
        switch (msg.what) {
            case INIT_BOOKMONEY:
                if (jsonObject.optBoolean("result")) {
                    String bookmoney = jsonObject.optString("objects");
                    tvMybookmoney.setText(bookmoney);
                }
                break;
            case INIT_LIST_DATA:
                String json = jsonObject.toString();
                Gson gson = new Gson();
                MyBookMoneyBean myBookMoneyBean = gson.fromJson(json, MyBookMoneyBean.class);
                if (myBookMoneyBean.result) {
                    initView(myBookMoneyBean);
                }
                break;
        }
    }

    private void initView(MyBookMoneyBean myBookMoneyBean) {
        MyBookMoneyAdapyer myPartyLogAdapter = new MyBookMoneyAdapyer(myBookMoneyBean.objects);
        rlMybookmoney.setLayoutManager(new LinearLayoutManager(this));
        rlMybookmoney.setAdapter(myPartyLogAdapter);
    }

    @OnClick(R.id.topbar_left_img)
    public void onViewClicked() {
        finish();
    }
}
