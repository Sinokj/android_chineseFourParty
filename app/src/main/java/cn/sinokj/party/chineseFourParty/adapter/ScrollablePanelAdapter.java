package cn.sinokj.party.chineseFourParty.adapter;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.sinokj.party.chineseFourParty.R;
import cn.zhouchaoyuan.excelpanel.BaseExcelPanelAdapter;


/**
 * Created by l on 2017/6/30.
 */

public class ScrollablePanelAdapter extends BaseExcelPanelAdapter<String, String, String> {

    private final Context mContext;

    public ScrollablePanelAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(mContext).inflate(R.layout.item_scrollablepancel, null);
        return new ThisHolder(cellView);
    }

    @Override
    public void onBindCellViewHolder(RecyclerView.ViewHolder holder, int verticalPosition, int horizontalPosition) {
        ((ThisHolder)holder).tv_text.setText(majorData.get(verticalPosition).get(horizontalPosition));
        if(verticalPosition%2 ==0){
            ((ThisHolder)holder).tv_text.setBackgroundColor(Color.rgb(233,240,245));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateTopViewHolder(ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(mContext).inflate(R.layout.item_top_scrollablepancel, null);
        return new ThisHolder(cellView);
    }

    @Override
    public void onBindTopViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ThisHolder)holder).tv_text.setText(topData.get(position));
    }

    @Override
    public RecyclerView.ViewHolder onCreateLeftViewHolder(ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(mContext).inflate(R.layout.item_scrollablepancel, null);
        return new ThisHolder(cellView);
    }

    @Override
    public void onBindLeftViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ThisHolder)holder).tv_text.setText(leftData.get(position));
        if(position%2 ==0){
            ((ThisHolder)holder).tv_text.setBackgroundColor(Color.rgb(233,240,245));
        }
    }

    @Override
    public View onCreateTopLeftView() {
        View lefttopView = LayoutInflater.from(mContext).inflate(R.layout.item_topleft_view, null);
        return lefttopView;
    }

    class ThisHolder extends RecyclerView.ViewHolder {

        public TextView tv_text;

        public ThisHolder(View itemView) {
            super(itemView);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }
}
