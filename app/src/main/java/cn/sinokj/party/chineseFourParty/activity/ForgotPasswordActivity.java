/** 
 * @Title ForgotPasswordActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月11日 下午2:02:00
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.utils.Utils;
import cn.sinokj.party.chineseFourParty.utils.device.DeviceUtils;
import cn.sinokj.party.chineseFourParty.utils.time.TimeCountButtonUtil;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


/**
 * @ClassName ForgotPasswordActivity
 * @Description 忘记密码界面
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月11日 下午2:02:00
 **/
public class ForgotPasswordActivity extends BaseActivity {
	private static final int GET_SMS = 0;
	private static final int SUBMIT = 1;
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public ImageButton topLeftImage;
	@BindView(R.id.forgot_password_tel_num)
	public EditText telNumEdit;
	@BindView(R.id.forgot_password_username)
	public EditText userNameEdit;
	@BindView(R.id.forgot_password_password)
	public EditText passwordEdit;
	@BindView(R.id.forgot_password_password_confirm)
	public EditText passwordConfirmEdit;
	@BindView(R.id.forgot_password_identify)
	public EditText identifyEdit;
	
	private String telNum, deviceId, identify, username, password, passwordConfirm;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		ButterKnife.bind(this);
//		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
//		deviceId = telephonyManager.getDeviceId();
		deviceId = DeviceUtils.getDeviceIMEI(this);
		titleText.setText("忘记密码");
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
	}

	@OnClick({R.id.forgot_password_get_identify, R.id.topbar_left_img, R.id.forgot_password_submit})
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.forgot_password_get_identify:
			if (initializeArgs(GET_SMS)) {
				new Thread(new LoadDataThread(GET_SMS)).start();
			}
			break;
		case R.id.topbar_left_img:
			finish();
			break;
		case R.id.forgot_password_submit:
			if (initializeArgs(SUBMIT)) {
				new Thread(new LoadDataThread(SUBMIT)).start();
			}
			break;
		}
	}

	private boolean initializeArgs(int what) {
		switch (what) {
		case SUBMIT:
			/*username = userNameEdit.getText().toString().trim();
			if (TextUtils.isEmpty(username)) {
				Toast.makeText(this, "请填写登陆账号", Toast.LENGTH_SHORT).show();
				return false;
			}*/
			password = passwordEdit.getText().toString().trim();
			if (TextUtils.isEmpty(password)) {
				Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
				return false;
			}
			passwordConfirm = passwordConfirmEdit.getText().toString().trim();
			if (TextUtils.isEmpty(password)) {
				Toast.makeText(this, "请确认密码", Toast.LENGTH_SHORT).show();
				return false;
			}
			if (!TextUtils.equals(password, passwordConfirm)) {
				Toast.makeText(this, "两次输入密码不一致", Toast.LENGTH_SHORT).show();
				return false;
			}
			identify = identifyEdit.getText().toString().trim();
			if (TextUtils.isEmpty(identify)) {
				Toast.makeText(this, "请输入验证码", Toast.LENGTH_SHORT).show();
				return false;
			}
		case GET_SMS:
			telNum = telNumEdit.getText().toString().trim();
			if (!Utils.isMobileNo(telNum)) {
				Toast.makeText(this, "手机号码格式不正确", Toast.LENGTH_SHORT).show();
				return false;
			}
			break;
		}
		return true;
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		switch (what) {
		case GET_SMS:
			return HttpDataService.sendSMSNew(telNum, deviceId);
		case SUBMIT:
			return HttpDataService.forgotPswNew(telNum, password, identify);
		}
		return super.getDataFunction(what, arg1, arg2, obj);
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		DialogShow.closeDialog();
		switch (msg.what) {
		case GET_SMS:
			if (1 != jsonObject.optInt(HttpConstants.ReturnResult.NRES)) {
				Toast.makeText(this, jsonObject.optString(HttpConstants.ReturnResult.VCRES), Toast.LENGTH_SHORT).show();
				return;
			}
			new TimeCountButtonUtil(this, 60, 1000, (Button) findViewById(R.id.forgot_password_get_identify)).start();
			break;
		case SUBMIT:
			Toast.makeText(this, jsonObject.optString(HttpConstants.ReturnResult.VCRES), Toast.LENGTH_SHORT).show();
			if (1 == jsonObject.optInt(HttpConstants.ReturnResult.NRES)) {
				finish();
			}
			break;
		}
	}
}
