/**
 * @Title AnswerSheetActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月8日 下午4:31:08
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.AnswerSheetAdapter;
import cn.sinokj.party.chineseFourParty.bean.ExamQuestion;
import cn.sinokj.party.chineseFourParty.bean.ExamQuestionAnswered;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.scroll.ScrollGridView;


/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName AnswerSheetActivity
 * @Description 答题卡
 * @date 创建时间 2016年7月8日 下午4:31:08
 **/

public class AnswerSheetActivity extends BaseActivity {
    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public ImageButton topLeftImage;
    @BindView(R.id.topbar_right_text)
    public TextView topRightText;
    @BindView(R.id.answer_sheet_layout)
    public LinearLayout layout;
    @BindView(R.id.answer_sheet_all)
    public TextView allText;
    @BindView(R.id.answer_sheet_answered)
    public TextView answeredText;
    @BindView(R.id.answer_sheet_bg)
    LinearLayout answerSheetBg;
    @BindView(R.id.ensure)
    Button ensure;
    @BindView(R.id.mScrollView)
    ScrollView mScrollView;

    private int nTopicsId;
    private boolean examFinish = false;
    private int allQuestionCount, answeredCount = 0;
    private Map<String, List<ExamQuestion>> examQuestionMap;
    private Map<Integer, ExamQuestionAnswered> examQuestionAnsweredMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_answer_sheet);
        ButterKnife.bind(this);
        initializeData();
        titleText.setText("答题卡");
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);
        // 已完成的不可以再次交卷
        if (!examFinish) {
            ensure.setVisibility(View.VISIBLE);
        } else {
            //answerSheetBg.setBackgroundResource(R.color.colorPrimary);
        }
        for (Map.Entry<String, List<ExamQuestion>> entry : examQuestionMap.entrySet()) {
            initializeView(entry.getKey());
        }
        mScrollView.smoothScrollTo(0, 0);
        mScrollView.setFocusable(true);
        allText.setText(String.valueOf(allQuestionCount));
        answeredText.setText(String.valueOf(answeredCount));
    }

    @SuppressWarnings("unchecked")
    private void initializeData() {
        examFinish = getIntent().getBooleanExtra("examFinish", false);
        examQuestionMap = new LinkedHashMap<String, List<ExamQuestion>>();
        List<ExamQuestion> examQuestionList = (List<ExamQuestion>) getIntent().getSerializableExtra("examQuestionList");
        allQuestionCount = examQuestionList.size();
        for (int i = 0; i < allQuestionCount; i++) {
            ExamQuestion examQuestion = examQuestionList.get(i);
            String type = examQuestion.getVcType();
            List<ExamQuestion> examQuestions = examQuestionMap.get(type);
            if (examQuestions != null) {
                examQuestions.add(examQuestion);
            } else {
                examQuestions = new ArrayList<ExamQuestion>();
                examQuestions.add(examQuestion);
                examQuestionMap.put(type, examQuestions);
            }
        }

        examQuestionAnsweredMap = (Map<Integer, ExamQuestionAnswered>) getIntent().getSerializableExtra("examQuestionAnsweredMap");
        for (ExamQuestionAnswered examQuestionAnswered : examQuestionAnsweredMap.values()) {
            if (examQuestionAnswered.isAnswered()) {
                answeredCount += 1;
            }
        }

        nTopicsId = examQuestionList.get(0).getnTopicsId();
    }

    @SuppressLint("InflateParams")
    private void initializeView(final String type) {
        View view = getLayoutInflater().inflate(R.layout.answer_sheet_grid, null);
        TextView textView = (TextView) view.findViewById(R.id.answer_sheet_grid_text);
        ScrollGridView gridView = (ScrollGridView) view.findViewById(R.id.answer_sheet_grid);

        textView.setText(type + "题");
        gridView.setAdapter(new AnswerSheetAdapter(this, examQuestionMap.get(type), examQuestionAnsweredMap, examFinish));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<ExamQuestion> examQuestionList = examQuestionMap.get(type);
                ExamQuestion examQuestion = examQuestionList.get(position);
                Intent intent = getIntent();
                intent.putExtra("nTopicId", examQuestion.getnId());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        layout.addView(view);
    }

    @OnClick({R.id.topbar_left_img, R.id.ensure})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
            case R.id.ensure:
                new Thread(new LoadDataThread()).start();
                break;
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        return HttpDataService.submitTopics(String.valueOf(nTopicsId));
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        if (1 == jsonObject.optInt(HttpConstants.ReturnResult.NRES)) {
            Intent intent = new Intent(this, ExamResultActivity.class);
            intent.putExtra("nScore", jsonObject.optInt("nScore"));
            intent.putExtra("isPass", jsonObject.optString("isPass"));
            startActivity(intent);
            AnswerActivity.answerActivity.finish();
            finish();
        }
    }
}
