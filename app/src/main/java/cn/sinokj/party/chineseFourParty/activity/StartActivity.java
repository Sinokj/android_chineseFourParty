package cn.sinokj.party.chineseFourParty.activity;

import android.content.Intent;
import android.os.Bundle;

import cn.jpush.android.api.InstrumentedActivity;


public class StartActivity extends InstrumentedActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = new Intent(this, FiringActivity.class);
		startActivity(intent);
		finish();
	}
}
