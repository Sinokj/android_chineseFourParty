package cn.sinokj.party.chineseFourParty.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ExamListActivity;
import cn.sinokj.party.chineseFourParty.bean.TopicListBean;

/**
 * Created by l on 2017/11/21.
 */

public class TopicListAdapter extends BaseAdapter {

    private ExamListActivity mContext;
    private List<TopicListBean.ObjectsBean> mData;

    public TopicListAdapter(ExamListActivity examListActivity, List<TopicListBean.ObjectsBean> objects) {
        this.mContext = examListActivity;
        this.mData = objects;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_topiclist, parent, false);
            holder = new ViewHolder(convertView);
            holder.examListItemTitle = (TextView) convertView.findViewById(R.id.exam_list_item_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.examListItemTitle.setText(mData.get(position).vcTitle);
        return convertView;
    }

    static class ViewHolder {


        @BindView(R.id.exam_list_item_title)
        TextView examListItemTitle;

        ViewHolder(View view) {
            int layoutId = R.layout.item_topiclist;
            ButterKnife.bind(this, view);
        }
    }
}
