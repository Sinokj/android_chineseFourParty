package cn.sinokj.party.chineseFourParty.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by l on 2017/11/10.
 */

public class OpinionListInfo implements Serializable{

    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * vcRegister : admin
         * vcContent : 是否新建党总支机构意见征集
         * dtRegist : 2017-11-14 00:00:00
         * dtBegin : 2018-05-01 12:00:18
         * vcPersonName : 航天院党委
         * vcPersonId : 2
         * dtEnd : 2018-05-30 00:00:00
         * nId : 8
         * vcTopic : 关于新建党组织的决定
         * vcstatus : 进行中
         */

        public String vcRegister;
        public String vcContent;
        public String dtRegist;
        public String dtBegin;
        public String vcPersonName;
        public String vcPersonId;
        public String dtEnd;
        public int nId;
        public String vcTopic;
        public String vcstatus;
    }
}
