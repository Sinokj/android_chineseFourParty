package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andview.refreshview.XRefreshView;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.MyPartyLogAdapter;
import cn.sinokj.party.chineseFourParty.bean.MyPartyLogInfo;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;

import static cn.sinokj.party.chineseFourParty.activity.ArticleListActivity.INIT_DATA;


/**
 * Created by l on 2017/10/27.
 */

public class MyMembershipMoneyActivity extends BaseActivity {
    @BindView(R.id.topbar_left_img)
    public ImageButton topbarLeftImg;
    @BindView(R.id.title)
    public TextView title;
    @BindView(R.id.rl_membership_money)
    public RecyclerView rlMembershipMoney;
    @BindView(R.id.x_refresh)
    public XRefreshView xrefreshView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mymembership_money);
        ButterKnife.bind(this);
        initTitle();
        initRefresh();
        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    private void initTitle() {
        title.setVisibility(View.VISIBLE);
        title.setText("我的党费");
        topbarLeftImg.setVisibility(View.VISIBLE);
    }

    private void initRefresh() {
        xrefreshView.setPullLoadEnable(false); //下拉加载
        xrefreshView.setPullRefreshEnable(true);
        // 设置静默加载模式
        xrefreshView.setSilenceLoadMore(true);
        //设置刷新完成以后，headerview固定的时间
        xrefreshView.setPinnedTime(1000);
        xrefreshView.setMoveForHorizontal(true);
        xrefreshView.setAutoLoadMore(false); //滑动到底部自动加载更多
        xrefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh(boolean isPullDown) {
                new Thread(new LoadDataThread(INIT_DATA)).start();

            }

        });
        //设置静默加载时提前加载的item个数
        xrefreshView.setPreLoadCount(5);
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {

        switch (what) {
            case INIT_DATA:
                return HttpDataService.getMyPartyLog();
        }
        return super.getDataFunction(what, arg1, arg2, obj);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        DialogShow.closeDialog();
        switch (msg.what){
            case INIT_DATA:
                xrefreshView.stopRefresh(true);
                String json = jsonObject.toString();
                Gson gson = new Gson();
                MyPartyLogInfo myPartyLogInfo = gson.fromJson(json, MyPartyLogInfo.class);
                if(myPartyLogInfo.result){
                    initView(myPartyLogInfo);
                }
                break;
        }
        super.httpHandlerResultData(msg, jsonObject);

    }

    private void initView(MyPartyLogInfo myPartyLogInfo) {
        MyPartyLogAdapter myPartyLogAdapter = new MyPartyLogAdapter(myPartyLogInfo.objects);
        rlMembershipMoney.setLayoutManager(new LinearLayoutManager(this));
        rlMembershipMoney.setAdapter(myPartyLogAdapter);
    }

    @OnClick({R.id.topbar_left_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
        }
    }
}
