package cn.sinokj.party.chineseFourParty.fragment.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.gyf.barlibrary.ImmersionBar;

import org.json.JSONObject;

import java.util.Map;

import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.utils.logs.Logger;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;

import static cn.sinokj.party.chineseFourParty.service.HttpDataService.logout;


public abstract class BaseFragment extends Fragment {
	protected static Logger logger = Logger.getLogger();
	private Activity mContext;
	private ImmersionBar mImmersionBar;

	/**
	 * 调用web接口的一些变量设置
	 *
	 * @author azzbcc E-mail: azzbcc@sina.com
	 * @version 创建时间：2015年10月28日 上午9:10:38
	 */
	private Map<String, JSONObject> result;
	protected Handler httpHandler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			logger.i("", result);
			if (null == result) {
				logger.e(new NullPointerException("result"));
				DialogShow.closeDialog();
				return false;
			}
			try {
				JSONObject json = result.values().iterator().next();
				String status = json.optString("status");
				if ("sessionTimeOut".equals(status)) {
					logout();
					DialogShow.closeDialog();
					return false;
				}
				JSONObject jsonObject = result.get(HttpConstants.RESULT);
				if (null == jsonObject) {
					Toast.makeText(mContext, "网络连接异常", Toast.LENGTH_SHORT).show();
					DialogShow.closeDialog();
					return false;
				}

				httpHandlerResultData(msg, jsonObject);
			} catch (Exception e) {
				logger.e(e);
			}
			return false;
		}
	});

	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return null;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mContext = getActivity();
	}

	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		DialogShow.closeDialog();
	}

	protected final class LoadDataThread implements Runnable {

		private Object obj;
		private int what, arg1, arg2;
		public LoadDataThread() {
		}
		public LoadDataThread(int what) {
			this.what = what;
		}
		public LoadDataThread(int what, Object obj) {
			this.what = what;
			this.obj = obj;
		}
		public LoadDataThread(int what, int arg1, int arg2, Object obj) {
			this.what = what;
			this.arg1 = arg1;
			this.arg2 = arg2;
			this.obj = obj;
		}
		@Override
		public void run() {
			result = getDataFunction(what, arg1, arg2, obj);
			Message msg = httpHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}

	public void initImmersionBar(View view) {
		mImmersionBar = ImmersionBar.with(this).statusBarView(view);
		mImmersionBar.keyboardEnable(true).navigationBarWithKitkatEnable(false).init();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mImmersionBar != null)
			mImmersionBar.destroy();
	}
}