package cn.sinokj.party.chineseFourParty.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import cn.sinokj.party.chineseFourParty.utils.gson.GsonHandler;


/**
 * Created by l on 2017/10/30.
 */

public class PreferencesUtil {
    private Context mAppContext;
    private SharedPreferences mSharedPreferences;

    public PreferencesUtil(Context context) {
        mAppContext = context.getApplicationContext();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mAppContext);
    }

    public PreferencesUtil(Context context, int mode) {
        mAppContext = context.getApplicationContext();
        mSharedPreferences = context.getSharedPreferences(context.getClass().getName(), mode);
    }

    public PreferencesUtil(Context context, String name, int mode) {
        mAppContext = context.getApplicationContext();
        mSharedPreferences = context.getSharedPreferences(name, mode);
    }

    public String getString(String key) {
        return mSharedPreferences.getString(key, "");
    }

    @SuppressLint("CommitPrefEdits")
    public void putString(String key, String val) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, val);
        editor.apply();
    }


    /**
     * 序列化对象
     *
     * @param object
     * @return
     * @throws IOException
     */
    private String serialize(Object object) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(object);
        String serStr = byteArrayOutputStream.toString("ISO-8859-1");
        serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
        objectOutputStream.close();
        byteArrayOutputStream.close();
        return serStr;
    }

    /**
     * 反序列化对象
     *
     * @param string
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private Object deSerialization(String string) throws IOException, ClassNotFoundException {
        if (string == null) return null;
        String redStr = java.net.URLDecoder.decode(string, "UTF-8");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(redStr.getBytes
                ("ISO-8859-1"));
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        Object object = objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        return object;
    }

    public <T> T getObject(String key, Class<T> cls) {
        String result = mSharedPreferences.getString(key, null);
        if (null == result) return null;
        return GsonHandler.getNoExportGson().fromJson(result, cls);
    }

    public <T> void putObject(String key, T object) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, GsonHandler.getNoExportGson().toJson(object));
        editor.apply();
    }

    public <T> List<T> getList(String key, Class<T> cls) {
        String result = mSharedPreferences.getString(key, null);
        if (null == result) return null;
        ArrayList<T> mList = new ArrayList<>();
        JsonArray array = new JsonParser().parse(result).getAsJsonArray();
        for (final JsonElement elem : array) {
            mList.add(GsonHandler.getNoExportGson().fromJson(elem, cls));
        }
        return mList;
    }

    public <T> void putList(String key, List<T> list) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, GsonHandler.getNoExportGson().toJson(list));
        editor.apply();
    }

    public void putBoolean(String isLogin, boolean b) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(isLogin, b).apply();
    }

    public boolean getBoolean(String key, boolean defValue) {
        boolean aBoolean = mSharedPreferences.getBoolean(key, defValue);
        return aBoolean;
    }

    public void putInt(String key, int defValue) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, defValue).apply();
    }

    public int getInt(String key, int defValue) {
        int aBoolean = mSharedPreferences.getInt(key, defValue);
        return aBoolean;
    }

    public void delect(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("key");
        editor.commit();
    }
}
