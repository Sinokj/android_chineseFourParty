/** 
 * @Title VoteTopic.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午3:29:38
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName VoteTopic
 * @Description 投票Model
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午3:29:38
 **/
@SuppressWarnings("serial")
public class VoteTopic implements Serializable {
	@Expose
	private int nMaxVote;
	@Expose
	private List<VoteChoose> choices;
	@Expose
	private String vcType;
	@Expose
	private VoteAnswer isVoted;
	@Expose
	private Date dtEnd;
	@Expose
	private int nId;
	@Expose
	private String vcTopic;

	public int getnMaxVote() {
		return nMaxVote;
	}

	public void setnMaxVote(int nMaxVote) {
		this.nMaxVote = nMaxVote;
	}

	public List<VoteChoose> getChoices() {
		return choices;
	}

	public void setChoices(List<VoteChoose> choices) {
		this.choices = choices;
	}

	public String getVcType() {
		return vcType;
	}

	public void setVcType(String vcType) {
		this.vcType = vcType;
	}

	public VoteAnswer getIsVoted() {
		return isVoted;
	}

	public void setIsVoted(VoteAnswer isVoted) {
		this.isVoted = isVoted;
	}

	public Date getDtEnd() {
		return dtEnd;
	}

	public void setDtEnd(Date dtEnd) {
		this.dtEnd = dtEnd;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public String getVcTopic() {
		return vcTopic;
	}

	public void setVcTopic(String vcTopic) {
		this.vcTopic = vcTopic;
	}

	@Override
	public String toString() {
		return "VoteTopic [nMaxVote = " + nMaxVote + ", choices = " + choices
				+ ", vcType = " + vcType + ", isVoted = " + isVoted
				+ ", dtEnd = " + dtEnd + ", nId = " + nId + ", vcTopic = "
				+ vcTopic + "]";
	}

}
