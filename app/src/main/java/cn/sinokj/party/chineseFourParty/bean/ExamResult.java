/** 
 * @Title ExamResult.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月27日 上午9:48:16
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName ExamResult
 * @Description 考试结果
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月27日 上午9:48:16
 **/
@Deprecated
@SuppressWarnings("serial")
public class ExamResult implements Serializable {
	@Expose
	private String vcTitle;
	@Expose
	private int nPass;
	@Expose
	private boolean isPass;
	@Expose
	private int nScore;

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public int getnPass() {
		return nPass;
	}

	public void setnPass(int nPass) {
		this.nPass = nPass;
	}

	public boolean isPass() {
		return isPass;
	}

	public void setPass(boolean isPass) {
		this.isPass = isPass;
	}

	public int getnScore() {
		return nScore;
	}

	public void setnScore(int nScore) {
		this.nScore = nScore;
	}
}
