package cn.sinokj.party.chineseFourParty.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.util.MultiTypeDelegate;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.CommitteeListInfo;

/**
 * 区域选择的适配器
 */

public class AreaSelectionAdapter extends BaseQuickAdapter<CommitteeListInfo.ObjectsBean, BaseViewHolder> {

    private static final int TOTAL_PARTY = 1;
    private static final int OTHER_PARTY = 2;

    public AreaSelectionAdapter(@Nullable List<CommitteeListInfo.ObjectsBean> data) {
        super(R.layout.item_otherarea_selection, data);
        setMultiTypeDelegate(new MultiTypeDelegate<CommitteeListInfo.ObjectsBean>() {
            @Override
            protected int getItemType(CommitteeListInfo.ObjectsBean objectsBean) {
                return objectsBean.nId == 2 ? TOTAL_PARTY : OTHER_PARTY;
            }
        });

        getMultiTypeDelegate().registerItemType(TOTAL_PARTY, R.layout.item_totalarea_selection)
                .registerItemType(OTHER_PARTY, R.layout.item_otherarea_selection);
    }

    @Override
    protected void convert(BaseViewHolder helper, CommitteeListInfo.ObjectsBean item) {
        helper.setText(R.id.area_app_name, item.vcName);
    }
}
