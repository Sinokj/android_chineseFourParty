/** 
 * @Title OpinionCollectionActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月5日 下午7:55:31
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.OpinionListAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.OpinionListInfo;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;

/**
 * @ClassName OpinionCollectionActivity
 * @Description 意见征集界面
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月5日 下午7:55:31
 **/
public class OpinionListActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.opinion_list_listview)
	public ListView listView;
	
	private OpinionListAdapter opinionListAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.opinion_list);
		ButterKnife.bind(this);
		titleText.setText("意见征集");
		topLeft.setVisibility(View.VISIBLE);
		titleText.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		DialogShow.showRoundProcessDialog(this);
		new Thread(new LoadDataThread()).start();
	}
	
	@OnClick(R.id.topbar_left_img)
	public void OnClick(View view) {
		finish();
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.getOpinionTopics(App.nCommitteeId);
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		DialogShow.closeDialog();
        OpinionListInfo opinionListInfo = new Gson().fromJson(jsonObject.toString(), OpinionListInfo.class);
        opinionListAdapter = new OpinionListAdapter(this, opinionListInfo.objects);
        /*JSONArray jsonArray = jsonObject.optJSONArray(Constants.OBJECTS);
		List<OpinionTopic> opinionTopicList = GsonHandler.getGson().fromJson(jsonArray.toString(),
				new TypeToken<List<OpinionTopic>>() {}.getType());
		opinionListAdapter = new OpinionListAdapter(this, opinionTopicList);*/
		listView.setAdapter(opinionListAdapter);
		if (opinionListInfo.objects.size() == 0) {
			Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
		}

	}
}
