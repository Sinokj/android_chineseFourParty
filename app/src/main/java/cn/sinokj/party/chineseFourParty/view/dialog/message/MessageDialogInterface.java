/** 
 * @Title MessageAlertDialogInterface.java
 * @Package cn.sinokj.mobile.business.activity.base
 * @Description 显示多条message
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年3月3日 上午9:43:54
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.view.dialog.message;

/**
 * @ClassName MessageDialogInterface
 * @Description 显示多条message
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年3月3日 上午9:43:54
 **/
public interface MessageDialogInterface<T> {
	
	/**
	 * @Title getDataItem
	 * @Description 数据显示方式
	 * @author azzbcc Email: azzbcc@sina.com
	 * @date 创建时间 2016年3月3日 上午9:51:18
	 * @return
	 **/
	public String getDataItem(T data);

	/**
	 * @Title onItemClick
	 * @Description 点击列表某一项触发
	 * @author azzbcc Email: azzbcc@sina.com
	 * @param which 
	 * @date 创建时间 2016年3月3日 上午9:48:54
	 **/
	public void onItemClick(int which);
}
