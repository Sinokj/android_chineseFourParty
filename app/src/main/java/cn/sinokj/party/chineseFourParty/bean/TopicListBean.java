package cn.sinokj.party.chineseFourParty.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by l on 2017/11/21.
 */

public class TopicListBean implements Serializable {

    public List<ObjectsBean> objects;

    public static class ObjectsBean {
        /**
         * dtBegin : 2017-11-16 00:00:00
         * dtEnd : 2017-11-30 00:00:00
         * dtReg : 2017-11-15 19:50:50
         * nId : 1
         * nJoined : 3
         * nRegistGroupId : 0
         * vcGroupId : 2,5,12,
         * vcGroupName : 6
         * vcRegister : 3
         * vcTitle : 1233333
         */

        public String dtBegin;
        public String dtEnd;
        public String dtReg;
        public int nId;
        public int nJoined;
        public int nRegistGroupId;
        public String vcGroupId;
        public String vcGroupName;
        public String vcRegister;
        public String vcTitle;
    }
}
