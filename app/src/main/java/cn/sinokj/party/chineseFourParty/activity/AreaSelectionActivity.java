package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andview.refreshview.XRefreshView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.AreaSelectionAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.CommitteeListInfo;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.utils.Constans;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;
import de.greenrobot.event.EventBus;

/**
 * 党委区域选择
 * Created by Administrator on 2018/4/19.
 */

public class AreaSelectionActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.topbar_left_img)
    ImageButton mTopbarLeftImg;
    @BindView(R.id.topbar_left_text)
    TextView mTopbarLeftText;
    @BindView(R.id.rl_area_select)
    RecyclerView mRlAreaSelect;
    @BindView(R.id.x_refresh)
    XRefreshView xrefreshView;
    @BindView(R.id.empty_text)
    TextView emptyText;
    //网络请求的变量
    public static final int INIT_DATA = 1;
    //适配器
    private AreaSelectionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_selection);
        ButterKnife.bind(this);
        initTitle();
        initRefresh();
        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    /**
     * 初始化Title
     */
    private void initTitle() {
        mTitle.setVisibility(View.VISIBLE);
        mTitle.setText("航天一院党建云");
        setTextSize(mTitle);
        mTopbarLeftImg.setVisibility(View.VISIBLE);
    }

    private void initRefresh() {
        xrefreshView.setPullLoadEnable(false); //下拉加载
        xrefreshView.setPullRefreshEnable(true);
        // 设置静默加载模式
        xrefreshView.setSilenceLoadMore(true);
        //设置刷新完成以后，headerview固定的时间
        xrefreshView.setPinnedTime(1000);
        xrefreshView.setMoveForHorizontal(true);
        xrefreshView.setAutoLoadMore(false); //滑动到底部自动加载更多
        xrefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh(boolean isPullDown) {
                new Thread(new LoadDataThread(INIT_DATA)).start();
            }

        });
        //设置静默加载时提前加载的item个数
        xrefreshView.setPreLoadCount(5);
    }

    /**
     * Base类里封装的-网络请求
     */
    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        switch (what) {
            case INIT_DATA:
                return HttpDataService.getCommitteeList();
        }
        return super.getDataFunction(what, arg1, arg2, obj);
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        DialogShow.closeDialog();
        switch (msg.what) {
            case INIT_DATA:
                xrefreshView.stopRefresh(true);
                String json = jsonObject.toString();
                Gson gson = new Gson();
                CommitteeListInfo mCommitteeListInfo = gson.fromJson(json, CommitteeListInfo.class);
                if (mCommitteeListInfo.result) {
                    initView(mCommitteeListInfo);
                }
                break;
        }
        super.httpHandlerResultData(msg, jsonObject);
    }

    private void initView(CommitteeListInfo committeeListInfo) {
        mRlAreaSelect.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AreaSelectionAdapter(committeeListInfo.objects);
        if (mAdapter.getData().isEmpty()) {
            emptyText.setVisibility(View.VISIBLE);
        } else {
            emptyText.setVisibility(View.GONE);
            mRlAreaSelect.setAdapter(mAdapter);
        }
        //item的点击事件
       mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                App.nCommitteeId = mAdapter.getData().get(position).nId + "";
                //首页刷新
                Message msg = Message.obtain();
                msg.what = Constans.HOME_REFRESH;
                EventBus.getDefault().post(msg);
                //要闻刷新
                Message msg4 = Message.obtain();
                msg4.what = Constans.ANNOUNCEMENT_REFRESH;
                EventBus.getDefault().post(msg4);
                finish();
            }
        });
    }


    @OnClick({R.id.topbar_left_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
        }
    }
}
