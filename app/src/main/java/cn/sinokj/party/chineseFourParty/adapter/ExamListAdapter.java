/**
 * @Title ExamListAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月13日 上午11:37:24
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ExamActivity;
import cn.sinokj.party.chineseFourParty.bean.ExamTopic;


/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName ExamListAdapter
 * @Description 考试列表容器
 * @date 创建时间 2016年7月13日 上午11:37:24
 **/
public class ExamListAdapter extends BaseAdapter {

    private Context context;
    private List<ExamTopic> list;

    /**
     *
     **/
    public ExamListAdapter(Context context, List<ExamTopic> list) {
        this.context = context;
        this.list = list;
    }

    /**
     * @return
     * @see android.widget.Adapter#getCount()
     **/
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItem(int)
     **/
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItemId(int)
     **/
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     **/
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (list != null && list.size() > 0) {
            final ExamTopic examTopic = list.get(position);
            convertView = LayoutInflater.from(context).inflate(R.layout.new_exam_list_item, null);
            TextView titleText = (TextView) convertView.findViewById(R.id.exam_list_item_title);
            TextView countText = (TextView) convertView.findViewById(R.id.item_exam_count);
            TextView dtText = (TextView) convertView.findViewById(R.id.item_exam_time);
            //TextView totalText = (TextView) convertView.findViewById(R.id.exam_list_item_total);
            //TextView answeredText = (TextView) convertView.findViewById(R.id.exam_list_item_answered);
            TextView statusText = (TextView) convertView.findViewById(R.id.exam_list_item_status);
            titleText.setText(examTopic.getVcTitle());
            //totalText.setText(String.valueOf(examTopic.getnTopicsTotal()));
            //answeredText.setText(String.valueOf(examTopic.getAnswerdNumber()));
            dtText.setText(examTopic.getDtReg());
            countText.setText(String.valueOf(examTopic.getExamedNumber()) + "人作答");
            if (examTopic.getIsBegin() == 2) {
                statusText.setBackgroundResource(R.drawable.answer_unselect_status);
                statusText.setTextColor(context.getResources().getColor(R.color.mediumaquamarine));
                statusText.setText("已完成");
            } else {
                statusText.setBackgroundResource(R.drawable.answer_select_status);
                statusText.setTextColor(context.getResources().getColor(R.color.app_text));
                statusText.setText("未参与");
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dtEnd = examTopic.getDtEnd();

                    Intent intent = new Intent(context, ExamActivity.class);
                    intent.putExtra("examTopic", examTopic);
                    intent.putExtra("dtEnd", dtEnd);
                    context.startActivity(intent);
                }
            });
        }
        return convertView;
    }

}
