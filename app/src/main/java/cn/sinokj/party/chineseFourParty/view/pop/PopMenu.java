package cn.sinokj.party.chineseFourParty.view.pop;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.sinokj.party.chineseFourParty.R;


/**
 * 自定义弹出菜单
 * 
 * @author guopeng
 * @created 2015年11月27日
 */
public class PopMenu {
	private Context context;
	private PopListView listView;
	private PopupWindow popupWindow;
	private PopAdapter popAdapter = new PopAdapter();
	private ArrayList<String> itemList = new ArrayList<String>();

	public PopMenu(Context context) {
		this.context = context;
		initializeViews();
		notifyDataSetChanged();
	}

	public void notifyDataSetChanged() {
		popAdapter.notifyDataSetChanged();
	}

	@SuppressLint("InflateParams")
	@SuppressWarnings("deprecation")
	private void initializeViews() {
		View view = LayoutInflater.from(context).inflate(R.layout.pop_menu, null);
		listView = (PopListView) view.findViewById(R.id.popup_view_listView);

		listView.setAdapter(popAdapter);
		listView.setFocusableInTouchMode(true);
		listView.setFocusable(true);
		
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景（很神奇的）
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
	}

	public void setOnDismissListener(OnDismissListener onDismissListener) {
		popupWindow.setOnDismissListener(onDismissListener);
	}

	// 设置菜单项点击监听器
	public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
		listView.setOnItemClickListener(listener);
	}

	// 批量添加菜单项
	public void addItems(List<String> strings) {
		for (String s : strings) {
			itemList.add(s);
		}
	}

	// 单个添加菜单项
	public void addItem(String string) {
		itemList.add(string);
	}

	// 下拉式 弹出 pop菜单 parent 右下角
	public void showAsDropDown(View parent) {
		popupWindow.showAsDropDown(parent);
		// 使其聚集
		popupWindow.setFocusable(true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		// 刷新状态
		popupWindow.update();
	}

	// 隐藏菜单
	public void dismiss() {
		popupWindow.dismiss();
	}

	// 适配器
	private final class PopAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint({ "ViewHolder", "InflateParams" })
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = LayoutInflater.from(context).inflate(R.layout.pop_menu_item, null);
			TextView textView = (TextView) convertView.findViewById(R.id.pop_item_header);
			textView.setText(itemList.get(position));
			return convertView;
		}
	}
}