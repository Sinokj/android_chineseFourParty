package cn.sinokj.party.chineseFourParty.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.mob.MobSDK;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsDownloader;
import com.tencent.smtt.sdk.TbsListener;

import java.io.File;

import cn.jpush.android.api.JPushInterface;
import cn.sinokj.party.chineseFourParty.bean.LoginInfo;


public class App extends Application {

    public static Context context;
    public static LoginInfo loginInfo;
    public static IWXAPI msgApi;
    public static String nWeixinOrderId;
    public static String nMoney;
    public static boolean MINE_REFRESH;
    public static boolean loginStatus;
    public static IWXAPI mWxApi;
    public static String nCommitteeId = "2";
    //极光的ID
    public static String registrationId = "";

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        registToWX();
        MobSDK.init(this);
        initImageLoader(getApplicationContext());
        initX5Environment();
        TbsDownloader.needDownload(getApplicationContext(), false);
        InitJPush();
        //注册腾讯bugly-建议在测试阶段建议设置成true，发布时设置为false
        CrashReport.initCrashReport(getApplicationContext(), "244eb764d4", true);
        //微信支付注册APPID
        //msgApi = WXAPIFactory.createWXAPI(context, null);
        // 将该app注册到微信
        //msgApi.registerApp("wx8a7b96f1e6f41d60");
    }

    /**
     * 初始化极光的sdk
     */
    private void InitJPush() {
        JPushInterface.setDebugMode(true);//正式版的时候设置false，关闭调试
        JPushInterface.init(this);
        registrationId = JPushInterface.getRegistrationID(this);
       // JPushInterface.setAlias(this,1,"adf");
        //统计调用
        //JAnalyticsInterface.setDebugMode(true);
        //JAnalyticsInterface.init(this);
        //建议添加tag标签，发送消息的之后就可以指定tag标签来发送了
        //Set<String> set = new HashSet<>();
        //set.add("CouldParty");//名字任意，可多添加几个,能区别就好了
        //\\JPushInterface.setTags(this, set, null);//设置标签
        //设置通知栏图标
        //BasicPushNotificationBuilder builder = new BasicPushNotificationBuilder(this);
        //builder.statusBarDrawable = R.d;
        //JPushInterface.setPushNotificationBuilder(1, builder);
    }

    /**
     * 初始化ImageLoader
     */
    public static void initImageLoader(Context context) {
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "partyBuilding/Cache");// 获取到缓存的目录地址
        Log.d("cacheDir", cacheDir.getPath());
        // 创建配置ImageLoader(所有的选项都是可选的,只使用那些你真的想定制)，这个可以设定在APPLACATION里面，设置为全局的配置参数
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                // .memoryCacheExtraOptions(480, 800) // max width, max
                // height，即保存的每个缓存文件的最大长宽
                // .discCacheExtraOptions(480, 800, CompressFormat.JPEG, 75,
                // null) // Can slow ImageLoader, use it carefully (Better don't
                // use it)设置缓存的详细信息，最好不要设置这个
                .threadPoolSize(3)// 线程池内加载的数量
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                // .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 *
                // 1024)) // You can pass your own memory cache
                // implementation你可以通过自己的内存缓存实现
                // .memoryCacheSize(2 * 1024 * 1024)
                // .discCacheSize(50 * 1024 * 1024)
                //.discCacheFileNameGenerator(new Md5FileNameGenerator())// 将保存的时候的URI名称用MD5
                // 加密
                // .discCacheFileNameGenerator(new
                // HashCodeFileNameGenerator())//将保存的时候的URI名称用HASHCODE加密
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                // .discCacheFileCount(100) //缓存的File数量
                .discCache(new UnlimitedDiscCache(cacheDir))// 自定义缓存路径
                // .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                // .imageDownloader(new BaseImageDownloader(context, 5 * 1000,
                // 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)超时时间
                // .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);// 全局初始化此配置
    }

    private void initX5Environment() {

        // 搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。
        // TbsDownloader.needDownload(getApplicationContext(), false);

        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                // TODO Auto-generated method stub
                Log.e("app", " onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
                // TODO Auto-generated method stub

            }
        };

        QbSdk.setTbsListener(new TbsListener() {
            @Override
            public void onDownloadFinish(int i) {
                Log.d("app", "onDownloadFinish");
            }

            @Override
            public void onInstallFinish(int i) {
                Log.d("app", "onInstallFinish");
            }

            @Override
            public void onDownloadProgress(int i) {
                Log.d("app", "onDownloadProgress:" + i);
            }
        });
        QbSdk.initX5Environment(getApplicationContext(), null);
    }

    private void registToWX() {
        //AppConst.WEIXIN.APP_ID是指你应用在微信开放平台上的AppID，记得替换。
        mWxApi = WXAPIFactory.createWXAPI(this, "wx69478824935366a1", false);
        // 将该app注册到微信
        mWxApi.registerApp("wx69478824935366a1");
    }
}
