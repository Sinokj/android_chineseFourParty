/** 
 * @Title Version.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月27日 上午11:53:41
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Version implements Serializable {

	/**
	 * nId : 2
	 * vcVersion : 1.0.1
	 * vcUrl : http://218.244.60.19:80/cloudPartyApp/APP/CloundPartyAPP_1.0.1.apk
	 * bUse : true
	 * dtTime : 2018-06-11 12:36:32
	 * vcVersionName : null
	 * vcVersionCode : 1
	 * tContent : null
	 */

	public int nId;
	public String vcVersion;
	public String vcUrl;
	public boolean bUse;
	public String dtTime;
	public String vcVersionName;
	public int vcVersionCode;
	public String tContent;
}
