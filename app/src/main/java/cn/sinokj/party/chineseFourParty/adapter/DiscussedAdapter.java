/** 
 * @Title DiscussedAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月27日 上午9:25:23
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.DiscussedAnswer;


/**
 * @ClassName DiscussedAdapter
 * @Description 评议信息
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月27日 上午9:25:23
 **/
public class DiscussedAdapter extends BaseAdapter {
	private Context context;
	private List<DiscussedAnswer> list;

	/**
	 **/
	public DiscussedAdapter(Context context, List<DiscussedAnswer> list) {
		this.context = context;
		this.list = list;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View,
	 *      ViewGroup)
	 **/
	@SuppressLint({ "InflateParams", "ViewHolder" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DiscussedAnswer discussedAnswer = list.get(position);

		convertView = LayoutInflater.from(context).inflate(R.layout.discussion_item, null);

		TextView nIdText = (TextView) convertView.findViewById(R.id.discussion_item_nid);
		TextView titleText = (TextView) convertView.findViewById(R.id.discussion_item_title);
		EditText scoreEdit = (EditText) convertView.findViewById(R.id.discussion_item_score);

		nIdText.setText(String.valueOf(position + 1));
		titleText.setText(discussedAnswer.getVcOption() + "   (" + discussedAnswer.getnOptionScore() + "分)");

		scoreEdit.setKeyListener(null);
		scoreEdit.setText(String.valueOf(discussedAnswer.getnScore()));
		return convertView;
	}

}
