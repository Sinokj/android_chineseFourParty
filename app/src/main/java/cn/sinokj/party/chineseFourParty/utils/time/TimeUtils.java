package cn.sinokj.party.chineseFourParty.utils.time;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by l on 2017/5/11.
 */

public class TimeUtils {
    private final static long minute = 60 * 1000;// 1分钟
    private final static long hour = 60 * minute;// 1小时
    private final static long day = 24 * hour;// 1天
    private final static long month = 31 * day;// 月
    private final static long year = 12 * month;// 年

    /**
     * 返回文字描述的日期
     *
     * @param date
     * @return
     */
    public static String getTimeFormatText(Date date) {
        if (date == null) {
            return null;
        }
        long diff = new Date().getTime() - date.getTime();
        long r = 0;
        /*if (diff > year) {
            r = (diff / year);
            return r + "年前";
        }
        if (diff > month) {
            r = (diff / month);
            return r + "个月前";
        }*/
        if (diff > month) {
            return "";
        }
        if (diff > day) {
            r = (diff / day);
            return r + "天前";
        }
        if (diff > hour) {
            r = (diff / hour);
            return r + "小时前";
        }
        if (diff > minute) {
            r = (diff / minute);
            return r + "分钟前";
        }
        return "刚刚";
    }

    public static String getTimeFormatText(String forMatDate) {
        if (forMatDate == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(forMatDate);
            String timeFormatText = getTimeFormatText(date);
            return TextUtils.equals("", timeFormatText) ? forMatDate : timeFormatText;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getTimeLeftFromNow(String forMatDate) {
        if (forMatDate == null) {
            return false;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(forMatDate);
            long diff = Math.abs(new Date().getTime() - date.getTime());
            double timeLeft = (double) diff / (double)day;
            if(timeLeft < 1){
                return true;
            }
            return false;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 判断 时间是否在当前时间之前
     * @param forMatDate
     * @return
     */
    public static boolean getTimeBeforeNow(String forMatDate) {
        if (forMatDate == null) {
            return false;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(forMatDate);
            long diff = new Date().getTime() - date.getTime();

            if(diff > 0){
                return true;
            }
            return false;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
