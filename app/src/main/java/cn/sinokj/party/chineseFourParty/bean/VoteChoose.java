/** 
 * @Title VoteChoose.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午3:32:00
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName VoteChoose
 * @Description 投票选项
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午3:32:00
 **/
@SuppressWarnings("serial")
public class VoteChoose implements Serializable {
	@Expose
	private int nCount;
	@Expose
	private String vcOption;
	@Expose
	private int nOptionId;

	public int getnCount() {
		return nCount;
	}

	public void setnCount(int nCount) {
		this.nCount = nCount;
	}

	public String getVcOption() {
		return vcOption;
	}

	public void setVcOption(String vcOption) {
		this.vcOption = vcOption;
	}

	public int getnOptionId() {
		return nOptionId;
	}

	public void setnOptionId(int nOptionId) {
		this.nOptionId = nOptionId;
	}
}
