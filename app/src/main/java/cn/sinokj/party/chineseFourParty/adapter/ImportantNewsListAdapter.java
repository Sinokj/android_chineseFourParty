/**
 * @Title XListAdapter.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月7日 上午9:19:00
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.IndexImage;
import cn.sinokj.party.chineseFourParty.utils.image.loader.ImageLoaderUtils;
import cn.sinokj.party.chineseFourParty.view.GlideRoundTransform;


/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName XListAdapter
 * @Description TODO(这里用一句话描述这个类的作用)
 * @date 创建时间 2016年7月7日 上午9:19:00
 **/
public class ImportantNewsListAdapter extends BaseAdapter {

    private Activity activity;
    private List<IndexImage> list;
    private boolean isMineClick = false;

    /**
     **/
    public ImportantNewsListAdapter(Activity activity, List<IndexImage> list) {
        this.activity = activity;
        this.list = list;
    }

    public ImportantNewsListAdapter(Activity activity, List<IndexImage> list, boolean isMineClick) {
        this.activity = activity;
        this.list = list;
        this.isMineClick = isMineClick;
    }

    /**
     * @return
     * @see android.widget.Adapter#getCount()
     **/
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItem(int)
     **/
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItemId(int)
     **/
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     **/
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final IndexImage indexImage = list.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.xlist_important_news_item, null);
            viewHolder.timeText = (TextView) convertView.findViewById(R.id.xlist_item_time);
            viewHolder.titleText = (TextView) convertView.findViewById(R.id.xlist_item_title);
            viewHolder.clickText = (TextView) convertView.findViewById(R.id.xlist_item_click);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.xlist_item_image);
            viewHolder.contentText = (TextView) convertView.findViewById(R.id.xlist_item_content);
            viewHolder.readStatusText = (ImageView) convertView.findViewById(R.id.xlist_item_read_stats);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.titleText.setText(indexImage.vcTitle);
        viewHolder.contentText.setText(indexImage.vcDescribe);
        if (isMineClick) {
            //  viewHolder.clickText.setText("已学习");
            viewHolder.clickText.setVisibility(View.GONE);
            viewHolder.readStatusText.setVisibility(View.GONE);
        } else {
            if (App.nCommitteeId.equals("2")) {
                viewHolder.readStatusText.setVisibility(View.GONE);
            } else {
                viewHolder.readStatusText.setVisibility(View.VISIBLE);
            }
            if (indexImage.nStatus == 0) {
                viewHolder.readStatusText.setBackgroundResource(R.drawable.icon_label_1);
            } else {
                viewHolder.readStatusText.setBackgroundResource(R.drawable.icon_label_2);
            }

            viewHolder.clickText.setText("阅读 " + indexImage.nclick);
        }
        ImageLoaderUtils.displayImage(indexImage.vcPath, viewHolder.imageView);
        Glide.with(activity)
                .load(indexImage.vcPath)
                .transform(new GlideRoundTransform(activity, 5))
                .into(viewHolder.imageView);

        viewHolder.timeText.setText(indexImage.dtReg);
//		convertView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(activity, IndexImageWebActivity.class);
//				intent.putExtra("url", indexImage.getUrl());
//				intent.putExtra("icon", indexImage.getVcPath());
//				intent.putExtra("title", indexImage.getVcTitle());
//				intent.putExtra("nShared", indexImage.getnShared());
//				intent.putExtra("describe", indexImage.getVcDescribe());
//				activity.startActivityForResult(intent, XListActivity.TO_CHANGE_READ_STATUS);
//			}
//		});
        return convertView;
    }

    class ViewHolder {
        TextView timeText;
        TextView titleText;
        TextView clickText;
        ImageView imageView;
        TextView contentText;
        ImageView readStatusText;
    }
}
