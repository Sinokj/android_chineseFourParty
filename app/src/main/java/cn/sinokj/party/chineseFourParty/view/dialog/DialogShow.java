package cn.sinokj.party.chineseFourParty.view.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import cn.sinokj.party.chineseFourParty.R;


public class DialogShow {

	private static Dialog mDialog;

	private static View dialog;

	@SuppressLint("InflateParams")
	public static void showRoundProcessDialog(Context mContext) {
		closeDialog();

		OnKeyListener keyListener = new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
					mDialog.dismiss();
				}
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					mDialog.dismiss();
				}
				return false;
			}
		};
		mDialog = new Dialog(mContext, R.style.dialog);
		dialog = LayoutInflater.from(mContext).inflate(R.layout.loading_process_dialog_icon, null);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.setOnKeyListener(keyListener);
		mDialog.setCancelable(false);
		mDialog.setContentView(dialog);
		mDialog.show();
	}

	public static void closeDialog() {
		if (null != mDialog && mDialog.isShowing()) {
			mDialog.dismiss();
		}
	}
}
