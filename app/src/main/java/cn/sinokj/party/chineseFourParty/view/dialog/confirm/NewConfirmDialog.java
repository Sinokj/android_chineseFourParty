package cn.sinokj.party.chineseFourParty.view.dialog.confirm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.utils.display.DisplayUtil;


/**
 * Created by azzbcc on 16-5-16.
 */
@SuppressWarnings("ResourceType")
public class NewConfirmDialog {
	@SuppressWarnings("deprecation")
	public static void confirmAction(Context context, String message, String okString, String cancelString,
			final ConfirmInterface confirmIf) {
		final AlertDialog confirmDialog = new AlertDialog.Builder(context).create();
		confirmDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
					confirmDialog.dismiss();
				}
				return false;
			}
		});
		confirmDialog.setCancelable(true);
		confirmDialog.show();
		confirmDialog.setContentView(R.layout.answer_dialog);
		Display display = DisplayUtil.getContextDisplay(context);
		LayoutParams layoutParams = confirmDialog.getWindow().getAttributes();
		layoutParams.width = (int) (display.getWidth() * 0.8);
		confirmDialog.getWindow().setAttributes(layoutParams);

		TextView confirm = (TextView) confirmDialog.findViewById(R.id.confirm_message);
		TextView cancel = (TextView) confirmDialog.findViewById(R.id.cancel_bady);
		TextView ok = (TextView) confirmDialog.findViewById(R.id.ok_bady);
		if (!TextUtils.isEmpty(message)) {
			confirm.setText(message);
		}
		if (!TextUtils.isEmpty(okString)) {
			ok.setText(okString);
		} else {
			ok.setVisibility(View.GONE);
		}
		if (!TextUtils.isEmpty(cancelString)) {
			cancel.setText(cancelString);
		} else {
			cancel.setVisibility(View.GONE);
		}
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmDialog.dismiss();
				if (confirmIf != null) {
					confirmIf.onCancelButton();
				}
			}
		});
		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmDialog.dismiss();
				if (confirmIf != null) {
					confirmIf.onOkButton();
				}
			}
		});
	}
}
