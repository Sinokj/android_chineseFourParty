package cn.sinokj.party.chineseFourParty.utils.device;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;

public class DeviceUtils {

	@SuppressLint("HardwareIds")
	public static String getDeviceId(Context context) {
		String deviceId = null;

		try {
			TelephonyManager sp = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			deviceId = sp.getDeviceId();
		} catch (SecurityException var6) {
			Log.e("DeviceUtils", "SecurityException!!!");
		}

		SharedPreferences sp1 = context.getSharedPreferences("Statistics", 0);
		if (TextUtils.isEmpty(deviceId) || deviceId.equals("000000000000000") || deviceId.equals("000000000000")) {
			deviceId = sp1.getString("UDID", "");
			if (TextUtils.isEmpty(deviceId)) {
				SecureRandom e = new SecureRandom();
				deviceId = (new BigInteger(64, e)).toString(16);
				SharedPreferences.Editor editor = sp1.edit();
				editor.putString("UDID", deviceId);
				editor.apply();
			}
		}

		if (deviceId != null) {
			try {
				byte[] e1 = deviceId.getBytes();
				deviceId = new String(e1, "UTF-8");
			} catch (UnsupportedEncodingException var5) {
				var5.printStackTrace();
			}
		}

		return "RCV2:" + deviceId;
	}

	@SuppressLint("HardwareIds")
	public static String getWifiMacAddress(Context context) {
		SharedPreferences sp = context.getSharedPreferences("Statistics", 0);
		String macAddr = sp.getString("Mac", "");
		if (TextUtils.isEmpty(macAddr)) {
			try {
				WifiManager editor = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
				macAddr = editor.getConnectionInfo().getMacAddress();
			} catch (SecurityException var4) {
				Log.e("DeviceUtils", "SecurityException!!!");
			}

			Log.i("DeviceUtils", "MAC is: " + macAddr);
			if (!TextUtils.isEmpty(macAddr)) {
				SharedPreferences.Editor editor1 = sp.edit();
				editor1.putString("Mac", macAddr);
				editor1.apply();
			}
		}

		return macAddr;
	}

	@SuppressLint("HardwareIds")
	public static String getDeviceIMEI(Context context) {
		SharedPreferences sp = context.getSharedPreferences("Statistics", 0);
		String imei = sp.getString("IMEI", "");
		if (TextUtils.isEmpty(imei)) {
			try {
				TelephonyManager editor = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				imei = editor.getDeviceId();
			} catch (SecurityException var4) {
				Log.e("DeviceUtils", "SecurityException!!!");
			}

			Log.i("DeviceUtils", "IMEI is: " + imei);
			if (!TextUtils.isEmpty(imei) && !imei.equals("000000000000000") && !imei.equals("000000000000")) {
				SharedPreferences.Editor editor1 = sp.edit();
				editor1.putString("IMEI", imei);
				editor1.apply();
			}
		}

		return imei;
	}

	@SuppressLint("HardwareIds")
	public static String getDeviceIMSI(Context context) {
		SharedPreferences sp = context.getSharedPreferences("Statistics", 0);
		String imsi = sp.getString("IMSI", "");
		if (TextUtils.isEmpty(imsi)) {
			try {
				TelephonyManager editor = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				imsi = editor.getSubscriberId();
			} catch (SecurityException var4) {
				Log.e("DeviceUtils", "SecurityException!!!");
			}

			Log.i("DeviceUtils", "IMSI is: " + imsi);
			if (!TextUtils.isEmpty(imsi)) {
				SharedPreferences.Editor editor1 = sp.edit();
				editor1.putString("IMSI", imsi);
				editor1.apply();
			}
		}

		return imsi;
	}

	public static String getPhoneInformation(Context context) {
		String network = "";
		String MCCMNC = "";

		try {
			ConnectivityManager manufacturer = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (manufacturer != null && manufacturer.getActiveNetworkInfo() != null) {
				network = manufacturer.getActiveNetworkInfo().getTypeName();
			}

			TelephonyManager model = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			if (model != null) {
				MCCMNC = model.getNetworkOperator();
			}
		} catch (SecurityException var6) {
			Log.e("DeviceUtils", "SecurityException!!!");
		}

		String manufacturer1 = Build.MANUFACTURER;
		String model1 = Build.MODEL;
		if (manufacturer1 == null) {
			manufacturer1 = "";
		}

		if (model1 == null) {
			model1 = "";
		}

		String devInfo = manufacturer1 + "|";
		devInfo = devInfo + model1;
		devInfo = devInfo + "|";
		devInfo = devInfo + String.valueOf(Build.VERSION.SDK_INT);
		devInfo = devInfo + "|";
		devInfo = devInfo + network;
		devInfo = devInfo + "|";
		devInfo = devInfo + MCCMNC;
		devInfo.replace("-", "_");
		Log.i("DeviceUtils", "getPhoneInformation.the phone information is: " + devInfo);
		return devInfo;
	}
}
