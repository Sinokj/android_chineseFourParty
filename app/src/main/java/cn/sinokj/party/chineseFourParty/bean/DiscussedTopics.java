/** 
 * @Title DiscussedTopics.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月27日 上午9:13:39
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName DiscussedTopics
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月27日 上午9:13:39
 **/
@SuppressWarnings("serial")
public class DiscussedTopics implements Serializable {
	@Expose
	private List<DiscussedAnswer> content;
	@Expose
	private Date dtBegin;
	@Expose
	private String vcName;
	@Expose
	private String vcBeGreadPerson;
	@Expose
	private String vcTel;
	@Expose
	private String vcTitle;
	@Expose
	private int nId;
	@Expose
	private int nScore;
	@Expose
	private int nTopicsId;
	@Expose
	private Date dtEnd;
	public List<DiscussedAnswer> getContent() {
		return content;
	}
	public void setContent(List<DiscussedAnswer> content) {
		this.content = content;
	}
	public Date getDtBegin() {
		return dtBegin;
	}
	public void setDtBegin(Date dtBegin) {
		this.dtBegin = dtBegin;
	}
	public String getVcName() {
		return vcName;
	}
	public void setVcName(String vcName) {
		this.vcName = vcName;
	}
	public String getVcBeGreadPerson() {
		return vcBeGreadPerson;
	}
	public void setVcBeGreadPerson(String vcBeGreadPerson) {
		this.vcBeGreadPerson = vcBeGreadPerson;
	}
	public String getVcTel() {
		return vcTel;
	}
	public void setVcTel(String vcTel) {
		this.vcTel = vcTel;
	}
	public String getVcTitle() {
		return vcTitle;
	}
	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}
	public int getnId() {
		return nId;
	}
	public void setnId(int nId) {
		this.nId = nId;
	}
	public int getnScore() {
		return nScore;
	}
	public void setnScore(int nScore) {
		this.nScore = nScore;
	}
	public int getnTopicsId() {
		return nTopicsId;
	}
	public void setnTopicsId(int nTopicsId) {
		this.nTopicsId = nTopicsId;
	}
	public Date getDtEnd() {
		return dtEnd;
	}
	public void setDtEnd(Date dtEnd) {
		this.dtEnd = dtEnd;
	}
}
