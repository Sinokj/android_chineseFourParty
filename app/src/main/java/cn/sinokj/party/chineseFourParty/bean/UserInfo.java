/** 
 * @Title UserInfo.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月26日 上午10:40:39
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName UserInfo
 * @Description 用户姓名头像
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 上午10:40:39
 **/
@SuppressWarnings("serial")
public class UserInfo implements Serializable {
	@Expose
	private String vcName;
	@Expose
	private String vcHeadImgUrl;
	@Expose
	private String vcTel;
	@Expose
	private int nId;

	public String getVcName() {
		return vcName;
	}

	public void setVcName(String vcName) {
		this.vcName = vcName;
	}

	public String getVcHeadImgUrl() {
		return vcHeadImgUrl;
	}

	public void setVcHeadImgUrl(String vcHeadImgUrl) {
		this.vcHeadImgUrl = vcHeadImgUrl;
	}

	public String getVcTel() {
		return vcTel;
	}

	public void setVcTel(String vcTel) {
		this.vcTel = vcTel;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	@Override
	public String toString() {
		return "UserInfo [vcName = " + vcName + ", vcHeadImgUrl = "
				+ vcHeadImgUrl + ", vcTel = " + vcTel + ", nId = " + nId + "]";
	}
}
