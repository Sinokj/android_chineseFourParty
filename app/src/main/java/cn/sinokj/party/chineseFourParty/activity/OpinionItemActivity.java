package cn.sinokj.party.chineseFourParty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;

/**
 * Created by l on 2018/5/3.
 */

public class OpinionItemActivity extends BaseActivity {
    @BindView(R.id.tv_topic)
    TextView tvTopic;
    @BindView(R.id.tv_describe)
    TextView tvDescribe;
    @BindView(R.id.status_bar_height)
    View statusBarHeight;
    @BindView(R.id.title)
    TextView tvTitle;
    @BindView(R.id.topbar_left_img)
    ImageButton topbarLeftImg;
    @BindView(R.id.topbar_left_text)
    TextView topbarLeftText;
    @BindView(R.id.topbar_right_img)
    ImageButton topbarRightImg;
    @BindView(R.id.topbar_right_text)
    TextView topbarRightText;
    private int nId;
    private String title;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opinionitem);
        ButterKnife.bind(this);
        nId = getIntent().getIntExtra("nId", 0);
        title = getIntent().getStringExtra("title");
        content = getIntent().getStringExtra("content");
        initView();
    }

    private void initView() {
        tvTitle.setVisibility(View.VISIBLE);
        topbarLeftImg.setVisibility(View.VISIBLE);
        topbarRightImg.setVisibility(View.VISIBLE);
        tvTitle.setText("意见征集");
        tvTopic.setText(title);
        tvDescribe.setText(content);
        topbarRightImg.setBackgroundResource(R.drawable.suggest_creat);
    }

    @OnClick({R.id.topbar_left_img, R.id.topbar_right_img})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
            case R.id.topbar_right_img:
                Intent intent = new Intent(this, OpinionEditActivity.class);
                intent.putExtra("nId", nId + "");
                startActivity(intent);
                break;
        }
    }
}
