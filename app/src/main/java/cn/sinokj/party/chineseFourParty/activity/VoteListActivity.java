/** 
 * @Title VoteListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月21日 下午3:26:08
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.VoteListAdapter;
import cn.sinokj.party.chineseFourParty.bean.VoteTopic;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;

/**
 * @ClassName VoteListActivity
 * @Description 投票列表
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月21日 下午3:26:08
 **/
public class VoteListActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public ImageButton topLeftImage;
	@BindView(R.id.vote_list_listview)
	public ListView voteListView;

	private boolean voted;
	private VoteListAdapter voteListAdapter;
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vote_list);
		ButterKnife.bind(this);
		voted = getIntent().getBooleanExtra("voted", false);
		titleText.setText("投票列表");
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onResume() {
		DialogShow.showRoundProcessDialog(this);
		new Thread(new LoadDataThread()).start();
		super.onResume();
	}

	@OnClick({R.id.topbar_left_img})
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.topbar_left_img:
			finish();
			break;
		}
	}
	
	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.getVoteTopics();
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		JSONArray jsonArray = jsonObject.optJSONArray(HttpConstants.OBJECTS);
		List<VoteTopic> voteTopics = new Gson().fromJson(jsonArray.toString(),
				new TypeToken<List<VoteTopic>>() {}.getType());
		if (voted) {
			List<VoteTopic> voteTopicsVoted = new ArrayList<VoteTopic>();
			for (VoteTopic voteTopic : voteTopics) {
				if (voteTopic.getIsVoted().hasChecked()) {
					voteTopicsVoted.add(voteTopic);
				}
			}
			voteTopics = voteTopicsVoted;
		}
		voteListAdapter = new VoteListAdapter(this, voteTopics);
		voteListView.setAdapter(voteListAdapter);
		if (voteTopics.size() == 0) {
			Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
		}
		DialogShow.closeDialog();
	}
}
