package cn.sinokj.party.chineseFourParty.utils.image.loader;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import cn.sinokj.party.chineseFourParty.R;


public class ImageLoaderUtils {

	/**
	 * imageLoader 的相关操作 调用this.displayImage 调用this.initializeImageLoader后
	 * 
	 * @author azzbcc E-mail: azzbcc@sina.com
	 * @version 创建时间：2015年12月21日 下午1:48:20
	 */
	private static final ImageLoader imageLoader = ImageLoader.getInstance();
	private static final DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.ic_stub)
			.showImageForEmptyUri(R.drawable.ic_default)
			.showImageOnFail(R.drawable.ic_default).cacheInMemory(true)
			.cacheOnDisc(true).imageScaleType(ImageScaleType.NONE)
			.bitmapConfig(Bitmap.Config.RGB_565)// 设置为RGB565比起默认的ARGB_8888要节省大量的内存
			.delayBeforeLoading(100)// 载入图片前稍做延时可以提高整体滑动的流畅度
			.build();

	public static void displayImage(String uri, ImageView imageView,
			ImageLoadingListener listener,
			ImageLoadingProgressListener progressListener) {
		imageLoader.displayImage(uri, imageView, options, listener,
				progressListener);
	}

	public static void displayImage(String uri, ImageView imageView,
			ImageLoadingListener listener) {
		imageLoader.displayImage(uri, imageView, options, listener);
	}

	public static void displayImage(String uri, ImageView imageView) {
		imageLoader.displayImage(uri, imageView, options);
	}
}
