package cn.sinokj.party.chineseFourParty.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.MyPartyLogInfo;


/**
 * Created by l on 2017/10/30.
 */

public class MyPartyLogAdapter extends BaseQuickAdapter<MyPartyLogInfo.ObjectsBean, BaseViewHolder> {

    public MyPartyLogAdapter(@Nullable List data) {
        super(R.layout.item_newmypartylog, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, MyPartyLogInfo.ObjectsBean item) {
        helper.setText(R.id.tv_time, item.dtReg)
                .setText(R.id.tv_money, item.mRealFee + "元")
                .setText(R.id.tv_title, sub(item.dtReg.toString()));
    }

    private String sub(String dtReg) {
        String[] strs = dtReg.split("-");
        return strs[0] + "年" + strs[1] + "月份党费";
    }
}
