/** 
 * @Title DiscussedListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月26日 下午3:12:20
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.DiscussedListAdapter;
import cn.sinokj.party.chineseFourParty.bean.DiscussedTopics;


/**
 * @ClassName DiscussedListActivity
 * @Description 被评议列表
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午3:12:20
 **/
public class DiscussedListActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView title;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.discussion_list_list_view)
	public ListView listView;

	private List<DiscussedTopics> discussedList;
	private DiscussedListAdapter discussedListAdapter;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discussion_list);
		ButterKnife.bind(this);
		discussedList = (List<DiscussedTopics>) getIntent().getSerializableExtra("discussedList");
		
		title.setText("我的评议");
		title.setVisibility(View.VISIBLE);
		topLeft.setVisibility(View.VISIBLE);
		discussedListAdapter = new DiscussedListAdapter(this, discussedList);
		listView.setAdapter(discussedListAdapter);
		
		if (discussedList == null || discussedList.size() == 0) {
			Toast.makeText(this, "当前没有人对您作出评议", Toast.LENGTH_SHORT).show();
		}
	}

	@OnClick(R.id.topbar_left_img)
	public void OnClick(View view) {
		finish();
	}
}
