/** 
 * @Title ChangePasswordActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月11日 下午3:20:22
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;


/**
 * @ClassName ChangePasswordActivity
 * @Description 修改密码界面
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月11日 下午3:20:22
 **/
public class ChangePasswordActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public ImageButton topLeftImage;
	@BindView(R.id.change_password_password)
	public EditText passwordEdit;
	@BindView(R.id.change_password_new_password)
	public EditText newPasswordEdit;
	@BindView(R.id.change_password_new_password_confirm)
	public EditText newPasswordConfirmEdit;
	
	private String password, newPassword, newPasswordConfirm;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password);
		ButterKnife.bind(this);
		titleText.setText("密码安全");
		titleText.setVisibility(View.VISIBLE);
		topLeftImage.setVisibility(View.VISIBLE);
	}

	@OnClick({R.id.topbar_left_img, R.id.change_password_change})
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.topbar_left_img:
			finish();
			break;
		case R.id.change_password_change:
			if (initializeArgs()) {
				new Thread(new LoadDataThread()).start();
			}
			break;
		}
	}

	private boolean initializeArgs() {
		password = passwordEdit.getText().toString().trim();
		newPassword = newPasswordEdit.getText().toString().trim();
		newPasswordConfirm = newPasswordConfirmEdit.getText().toString().trim();
		if (TextUtils.isEmpty(password)) {
			Toast.makeText(this, "请输入原密码", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (TextUtils.isEmpty(newPassword)) {
			Toast.makeText(this, "请输入新密码", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (TextUtils.isEmpty(newPasswordConfirm)) {
			Toast.makeText(this, "请确认新密码", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!TextUtils.equals(newPassword, newPasswordConfirm)) {
			Toast.makeText(this, "两次输入的密码不一致", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.modiPsw(password, newPassword);
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		Toast.makeText(this, jsonObject.optString(HttpConstants.ReturnResult.VCRES), Toast.LENGTH_SHORT).show();
		if (1 == jsonObject.optInt(HttpConstants.ReturnResult.NRES)) {
			finish();
		}
	}
}
