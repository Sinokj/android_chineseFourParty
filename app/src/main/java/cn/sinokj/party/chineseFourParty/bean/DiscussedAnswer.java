/** 
 * @Title Discussed.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月26日 下午3:14:31
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName Discussed
 * @Description 被评议
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午3:14:31
 **/
@SuppressWarnings("serial")
public class DiscussedAnswer implements Serializable {
	@Expose
	private String vcTitle;
	@Expose
	private int nOptionScore;
	@Expose
	private String vcOption;
	@Expose
	private int nScore;
	@Expose
	private float fAverageScore;

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public int getnOptionScore() {
		return nOptionScore;
	}

	public void setnOptionScore(int nOptionScore) {
		this.nOptionScore = nOptionScore;
	}

	public String getVcOption() {
		return vcOption;
	}

	public void setVcOption(String vcOption) {
		this.vcOption = vcOption;
	}

	public int getnScore() {
		return nScore;
	}

	public void setnScore(int nScore) {
		this.nScore = nScore;
	}

	public float getfAverageScore() {
		return fAverageScore;
	}

	public void setfAverageScore(float fAverageScore) {
		this.fAverageScore = fAverageScore;
	}
}
