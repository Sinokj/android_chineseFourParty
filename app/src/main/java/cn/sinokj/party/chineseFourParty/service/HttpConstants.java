package cn.sinokj.party.chineseFourParty.service;

import java.util.ArrayList;
import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.MainBean;
import cn.sinokj.party.chineseFourParty.utils.Utils;


/**
 * Created by azzbcc on 16-5-14.
 */
public class HttpConstants {
    private final static String IP;
    public static final int ACTIVITY_FINISH = 10087;
    public static int EXTERNAL_STORAGE = 10086;
    public static String WECHAT_APP_ID = "wxb9b26d87f4b114ec";

    static {
        if (Utils.DEBUG_MODE) {
            //IP = "39.105.214.117:8080";
            IP = "sw.ewanyuan.cn";

        } else {
            //IP = "192.168.1.232:8018";
            //IP = "39.105.214.117:8080";
            IP = "sw.ewanyuan.cn";
        }
    }

    private static final String COLON = ":";
    private static final String SEPARATOR = "/";
    private static final String PROTOCOL = "https://";
    private static final String BACKSERNAME = SEPARATOR + "cloudPartyAppStandard";
    private static final String SERVER = PROTOCOL + IP + BACKSERNAME + SEPARATOR;
    // 返回格式的固定类型
    public static final String RESULT = "result";
    public static final String OBJECTS = "objects";

    public static class ReturnResult {
        public static String NRES = "nRes";
        public static String VCRES = "vcRes";
    }

    /*
     * 接口链接管理
     */
    // 获取首页滚动图片
    public static final String GET_IMGS = SERVER + "getImgs.do";
    // 获取活动类别
    public static final String GET_NEWS = SERVER + "article/getArticleList.do";
    // APP 登陆
    public static final String LOGIN = SERVER + "loginSecond.do";
    //public static final String LOGIN = SERVER + "login.do";
    // APP 退出登陆
    public static final String LOGOUT = SERVER + "logout.do";
    // 发送验证码
    public static final String SEND_SMS_NEW = SERVER + "sendSMSNew.do";
    // 修改密码
    public static final String MODI_PSW = SERVER + "modiPsw.do";
    // 忘记密码
    public static final String FORGOT_PSW_NEW = SERVER + "forgotPswNew.do";
    // 获取试卷列表信息
    public static final String GET_TOPICS = SERVER + "getTopics.do";
    // 获取单个试卷信息
    public static final String GET_TOPIC_DETAIL = SERVER + "getTopicDetail.do";
    // 开始答题
    public static final String BEGIN_ANSWER = SERVER + "beginAnswer.do";
    // 提交单个答案
    public static final String UPDATE_ANSWER = SERVER + "updateAnswer.do";
    // 继续考试获取答案
    public static final String GET_ANSWERD = SERVER + "getAnswerd.do";
    // 提交整个试卷
    public static final String SUBMIT_TOPICS = SERVER + "submitTopics.do";
    // 获取民主评议信息
    public static final String GET_DISCUSSIONS_TOPICS = SERVER + "getDiscussionsTopics.do";
    // 提交民主评议
    public static final String SUBMIT_GREAD = SERVER + "submitGread.do";
    // 获取投票主题
    public static final String GET_VOTE_TOPICS = SERVER + "getVoteTopics.do";
    // 提交投票结果
    public static final String SUBMIT_VOTE = SERVER + "submitVote.do";
    // 获取意见征集主题
    public static final String GET_TOPICS_LIST = SERVER + "getTopicsList.do";
    // 意见征集详细内容
    public static final String GET_TOPICS_CONTENT = SERVER + "getTopics_Content.do?nId=";
    //	// 获取意见征集主题
    public static final String GET_OPINION_TOPICS = SERVER + "getOpinionTopics.do";
    //	// 提交意见征集
    public static final String SUBMIT_OPINION = SERVER + "submitOpinion.do";
    // 获取启动图名称，判断是否变化
    public static final String START_PHOTO = SERVER + "startphoto.do";
    // 获取用户信息
    public static final String GET_ALL_MY_INFO = SERVER + "getAllMyInfo.do";
    // 头像上传
    public static final String UPDATE_HEAD_IMG = SERVER + "updateHeadImg.do";
    // 版本更新
    public static final String UPDATE = SERVER + "update.do";
    // 绑定推送信息
    public static final String BIND_PUSH_INFO = SERVER + "push/savePushId.do";
    // 首页标记
    public static final String GET_UN_READ_LIST = SERVER + "getUnReadList.do";
    // 获取三会一课列表
    public static final String GET_PARTY_ARTICLE_LIST = SERVER + "partyarticle/getPartyArticleList.do";
    // 三会一课详细内容
    public static final String GET_PARTY_ARTICLE_CONTENT = SERVER + "partyarticle/getPartyArticleContent.do?nId=";
    // 关于我们
    //public static final String GET_ARTICLE = SERVER + "getArticle.do";
    //public static final String GET_ARTICLE = SERVER + "toCommonPage.do?nId=93";
    public static final String GET_ARTICLE = SERVER + "about.do?nCommitteeId=";

    // 获取某个文章当前阅读状态
    public static final String GET_SINGLE_READ_STATUS = SERVER + "article/getSingleReadStatus.do";
    // 获取三会一课阅读状态
    public static final String GET_ARTICLE_READ_STATUS = SERVER + "partyarticle/getPartyArticleStatus.do";
    // 获取我的学习
    public static final String GET_POCKET_TUTOR_LOG = SERVER + "article/getArtileLog.do";
    // 获取三会一课类型
    public static final String GET_PARTY_TYPE_LIST = SERVER + "partyarticle/getPartyTypeList.do";
    // 根据类型获取三会一课列表
    public static final String GET_PARTY_ARTICLE_LIST_BY_TYPE = SERVER + "partyarticle/getPartyArticleListByType.do";
    //支付宝党费缴纳
    public static final String PARTY_ALI_PAYCHECK = SERVER + "partyAliPayCheck.do";
    //微信党费缴纳
    public static final String PARTY_WX_PAY = SERVER + "wxPay.do";
    //支付宝党费缴纳
    public static final String WX_GETPAY_RESULT = SERVER + "wxGetPayResult.do";
    //企业与宣传文化
    public static final String GET_ARTICLE_PUBLISH_STATICS = SERVER + "ArticlePublish/getArticlePublishStatics.do";
    //我的党费
    public static final String GET_MYPARTYLOG = SERVER + "getMyPartyLog.do";
    //查询
    public static final String SEARCH_NEWS = SERVER + "article/searchNews.do";
    //查询
    public static final String GETTOPICS_CONTENT = SERVER + "getTopics_Content.do";

    //查询是否拥有点击权限
    public static final String GET_PERMISSION = SERVER + "module/getVerfityModule.do";
    //首页党委集合
    public static final String GETCOMMIT_LIST = SERVER + "module/getCommitteeList.do";

    //获取我的 代币余额接口
    public static final String GET_MYTOKEN_MONEY = SERVER + "getMyTokenMoney.do";

    //获取我的 代币消费记录接口
    public static final String GET_MYTOKEN_PAYLOG = SERVER + "getMyTokenPayLog.do";

    //获取首页数据
    public static final String GET_HOMEDATA = SERVER + "module/getHomeData.do";

    //获取文章Tag
    public static final String GET_ARTICLE_TAG = SERVER + "article/getArticleTagList.do";

    public static class ExamTopicType {
        public static final int BEGIN = 0;
        public static final int CONTINUE = 1;
        public static final int FINISH = 2;
        public static final String[] buttons = {"开始答题", "继续答题", "查看结果"};
        public static final String[] types = {"", "未完成", "已完成"};

        public static String getExamTopic(int what) {
            return types[what];
        }

        public static String getExamButtonText(int what) {
            return buttons[what];
        }
    }

    @SuppressWarnings("serial")
    public static List<MainBean> mainList = new ArrayList<MainBean>() {{
        add(new MainBean("中央精神", R.drawable.icon_spirit));
        add(new MainBean("专题教育", R.drawable.icon_notice));
        add(new MainBean("党章党规", R.drawable.icon_rule));
        add(new MainBean("任务活动", R.drawable.icon_police));
        add(new MainBean("先进典型", R.drawable.icon_power));
        add(new MainBean("民主评议", R.drawable.icon_comment));
        add(new MainBean("掌上课堂", R.drawable.icon_course));
        add(new MainBean("在线考试", R.drawable.icon_answer));
        add(new MainBean("问卷调查", R.drawable.icon_examine));
    }};
}
