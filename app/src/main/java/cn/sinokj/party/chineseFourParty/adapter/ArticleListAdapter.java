package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.ArticleBean;


public class ArticleListAdapter extends BaseAdapter {

	private Context context;
	private List<ArticleBean.ObjectsBean> list;

	public ArticleListAdapter(Context context, List<ArticleBean.ObjectsBean> list) {
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		final ArticleBean.ObjectsBean article = list.get(position);
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.article_list_item, null);
			viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
			viewHolder.titleText = (TextView) convertView.findViewById(R.id.item_topic);
			viewHolder.timeAndNread = (TextView) convertView.findViewById(R.id.item_time_and_nread);
			viewHolder.imageStatus = (ImageView) convertView.findViewById(R.id.image_status);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.timeAndNread.setText(article.dtReg);
		viewHolder.titleText.setText(article.vcTitle);
		//Glide.with(context).load(article.url)
		if (article.nStatus == 0) {
			viewHolder.imageStatus.setBackgroundResource(R.drawable.icon_label_1);
		} else {
			viewHolder.imageStatus.setBackgroundResource(R.drawable.icon_label_2);
		}
//
//		convertView.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(context, IndexImageWebActivity.class);
//				intent.putExtra("url", HttpConstants.GET_PARTY_ARTICLE_CONTENT + article.getnId());
//				intent.putExtra("title", article.getVcTitle());
//				context.startActivity(intent);
//			}
//		});
		return convertView;
	}

	private class ViewHolder {
		private ImageView ivIcon;
		private TextView titleText;
		private TextView timeAndNread;
		private ImageView imageStatus;
	}
}
