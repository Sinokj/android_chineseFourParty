/** 
 * @Title DiscussedActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月26日 下午4:02:50
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.DiscussedAdapter;
import cn.sinokj.party.chineseFourParty.bean.DiscussedTopics;


/**
 * @ClassName DiscussedActivity
 * @Description 被评议结果
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午4:02:50
 **/
public class DiscussedActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.discussion_list_view)
	public ListView listView;
	@BindView(R.id.discussion_submit)
	public Button submitButton;
	
	private DiscussedTopics discussedTopics;
	private DiscussedAdapter discussedAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discussion);
		ButterKnife.bind(this);
		discussedTopics = (DiscussedTopics) getIntent().getSerializableExtra("discussedTopics");
		topLeft.setVisibility(View.VISIBLE);
		titleText.setVisibility(View.VISIBLE);
		
		titleText.setText("我的评议得分");
		discussedAdapter = new DiscussedAdapter(this, discussedTopics.getContent());
		listView.setAdapter(discussedAdapter);
		
		submitButton.setVisibility(View.GONE);
	}

	@OnClick(R.id.topbar_left_img)
	public void onClick(View view) {
		finish();
	}
}
