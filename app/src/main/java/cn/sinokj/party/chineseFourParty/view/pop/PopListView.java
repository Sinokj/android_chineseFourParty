package cn.sinokj.party.chineseFourParty.view.pop;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

public class PopListView extends ListView {

	public PopListView(Context context) {
		super(context);
	}

	public PopListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PopListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    int maxWidth = meathureWidthByChilds() + getPaddingLeft() + getPaddingRight();
	    super.onMeasure(MeasureSpec.makeMeasureSpec(maxWidth, MeasureSpec.EXACTLY), heightMeasureSpec);     
	}

	public int meathureWidthByChilds() {
	    int maxWidth = 0;
	    View view = null;
	    for (int i = 0; i < getAdapter().getCount(); i++) {
	        view = getAdapter().getView(i, view, this);
	        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
	        if (view.getMeasuredWidth() > maxWidth){
	            maxWidth = view.getMeasuredWidth();
	        }
	    }
	    return maxWidth;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (ev.getAction() == MotionEvent.ACTION_MOVE) {
			return true;
		}
		return super.dispatchTouchEvent(ev);
	}
}