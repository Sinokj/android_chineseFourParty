package cn.sinokj.party.chineseFourParty.utils;

import android.content.Context;
/**
 * Created by HASEE.
 */

public class UIUitls {

    //dp转换成px
    public static int dip2px(Context context,int dip){
        float density = context.getResources().getDisplayMetrics().density;
        return (int)(density*dip+0.5f);
    }

    //px转换成dp
    public static int px2dip(Context context,int px){
        float density = context.getResources().getDisplayMetrics().density;
        return (int)(px/density+0.5f);
    }
}
