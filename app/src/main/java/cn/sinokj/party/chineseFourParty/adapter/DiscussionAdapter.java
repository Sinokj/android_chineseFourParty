/** 
 * @Title DiscussionAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 下午4:40:21
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.bean.DiscussionChoose;


/**
 * @ClassName DiscussionAdapter
 * @Description 评议容器
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 下午4:40:21
 **/
public class DiscussionAdapter extends BaseAdapter {

	private Context context;
	private Map<String, String> answerMap;
	private List<DiscussionChoose> questionList, answerList;

	/**
	 * @param context
	 * @param questionList
	 * @param answerList
	 * @param answerMap
	 **/
	public DiscussionAdapter(Context context,
			List<DiscussionChoose> questionList,
			List<DiscussionChoose> answerList, Map<String, String> answerMap) {
		this.context = context;
		this.answerMap = answerMap;
		this.answerList = answerList;
		this.questionList = questionList;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return questionList.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return questionList.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View,
	 *      ViewGroup)
	 **/
	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DiscussionChoose question = questionList.get(position);

		convertView = LayoutInflater.from(context).inflate(R.layout.discussion_item, null);

		TextView nIdText = (TextView) convertView.findViewById(R.id.discussion_item_nid);
		TextView titleText = (TextView) convertView.findViewById(R.id.discussion_item_title);
		EditText scoreEdit = (EditText) convertView.findViewById(R.id.discussion_item_score);

		titleText.setText(question.getVcOption() + "   (" + question.getnScore() + "分)");
		nIdText.setText(String.valueOf(position + 1));
		
		if (answerList != null && answerList.size() > 0) {
			DiscussionChoose answer = answerList.get(position);
			scoreEdit.setKeyListener(null);
			scoreEdit.setText(String.valueOf(answer.getnScore()));
		} else {
			scoreEdit.addTextChangedListener(getNewTextWatcher(scoreEdit, question));
		}

		return convertView;
	}
	
	private TextWatcher getNewTextWatcher(final EditText editText, final DiscussionChoose question) {
		return new TextWatcher() {
			private boolean flag = true;
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				if (flag && !TextUtils.isEmpty(s)) {
					int markVal = 0;
					try {
						markVal = Integer.parseInt(s.toString());
					} catch (NumberFormatException e) {
						markVal = 0;
					}
					if (markVal > question.getnScore()) {
						markVal = question.getnScore();
					}
					flag = false;
					String val = String.valueOf(markVal);
					editText.setText(val);
					editText.setSelection(val.length());
					answerMap.put("answer_" + question.getnId(), val);
					flag = true;
				}
			}
		};
	};
}
