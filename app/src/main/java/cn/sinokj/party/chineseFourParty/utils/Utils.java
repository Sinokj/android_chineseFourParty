package cn.sinokj.party.chineseFourParty.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by azzbcc on 16-5-14.
 */
public class Utils {
	public static final boolean DEBUG_MODE = false;
    public static int LOG_LEVEL;
	static {
		if (DEBUG_MODE) {
			LOG_LEVEL = Log.DEBUG;
		} else {
			LOG_LEVEL = Log.ERROR;
		}
	}
	private static final String ADVERTISEMENT = "广告";
	public static final String DEVICE_TYPE = "Android";
	public static final String PLATFORM = "e党建";
	public static final String MAIN_AD = "首页" + ADVERTISEMENT;
	public static final String INITIAL_PASSWORD = "123456";

    /**
     * 验证手机格式
     * @author azzbcc E-mail: azzbcc@sina.com
     * @version 创建时间：2015年12月22日 下午2:54:53
     * @param mobiles
     * @return
     */
    public static boolean isMobileNo(String mobiles) {
		/*
		 * "[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
		 * 移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188、182
		 * 联通：130、131、132、152、155、156、185、186
		 * 电信：133、153、180、189、177、（1349卫通）
		 * 虚拟：170
		*/
        /*String telRegex = "^((13[0-9])|(15[^4,\\D])|(14[57])|(17[07])|(18[0-9]))\\d{8}$";
        if (TextUtils.isEmpty(mobiles)) return false;
        else return mobiles.matches(telRegex);*/
        return true;
    }
    
    /**
     * @Title getAppVersionName
     * @Description 获取APP版本号
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月27日 上午11:45:22
     * @param context
     * @return
     **/
    public static String getAppVersionName(Context context) {
		String versionName = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			versionName = packageInfo.versionName;
			if (TextUtils.isEmpty(versionName)) {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return versionName;
	}

	/**
	 * @param context
	 * @return
	 * @Title getAppVersionName
	 * @Description 获取APP版本号
	 * @author azzbcc EmailNotification: azzbcc@sina.com
	 * @date 创建时间 2016年7月27日 上午11:45:22
	 **/
	public static int getAppVersionCode(Context context) {
		int versionCode = 0;
		try {
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			versionCode = packageInfo.versionCode;
			if (versionCode == 0) {
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return versionCode;
	}
}
