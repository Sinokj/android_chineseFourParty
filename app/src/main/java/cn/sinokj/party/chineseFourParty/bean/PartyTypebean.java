package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2018/5/4.
 */

public class PartyTypebean {

    public List<ResultBean> result;

    public static class ResultBean {
        /**
         * nId : 1
         * nTagName : 支部党员大会
         * norder : 1
         */

        public int nId;
        public String nTagName;
        public int norder;
    }
}
