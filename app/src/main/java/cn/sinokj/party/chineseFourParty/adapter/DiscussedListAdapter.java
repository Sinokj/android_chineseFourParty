/** 
 * @Title DiscussedListAdapter.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月26日 下午3:29:32
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.DiscussedActivity;
import cn.sinokj.party.chineseFourParty.bean.DiscussedTopics;


/**
 * @ClassName DiscussedListAdapter
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月26日 下午3:29:32
 **/
public class DiscussedListAdapter extends BaseAdapter {

	private Context context;
	private List<DiscussedTopics> list;
	/**
	 **/
	public DiscussedListAdapter(Context context, List<DiscussedTopics> list) {
		this.context = context;
		this.list = list;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View, ViewGroup)
	 **/
	@SuppressLint({ "InflateParams", "ViewHolder" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		final DiscussedTopics discussedTopics = list.get(position);
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.mine_discussion_list_item, null);
			viewHolder.nameText = (TextView) convertView.findViewById(R.id.discussion_list_item_name);
			viewHolder.scoreText = (TextView) convertView.findViewById(R.id.discussion_list_item_score);
			viewHolder.userIdText = (TextView) convertView.findViewById(R.id.discussion_list_item_user_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.nameText.setText(discussedTopics.getVcName());
		viewHolder.userIdText.setText(discussedTopics.getVcTitle());
		viewHolder.scoreText.setText(String.valueOf(discussedTopics.getnScore()));
		
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, DiscussedActivity.class);
				intent.putExtra("discussedTopics", discussedTopics);
				context.startActivity(intent);
			}
		});
		return convertView;
	}
	
	private class ViewHolder {
		private TextView nameText;
		private TextView userIdText;
		private TextView scoreText;
	}
}
