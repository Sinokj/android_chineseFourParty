package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.ScrollablePanelAdapter;
import cn.sinokj.party.chineseFourParty.bean.CorporateCultureInfo;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.zhouchaoyuan.excelpanel.ExcelPanel;


public class CorporateCultureActivity extends BaseActivity {

    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public View topLeftImage;
    @BindView(R.id.content_container)
    public ExcelPanel excelPanel;
    private ScrollablePanelAdapter scrollablePanelAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporateculture);
        ButterKnife.bind(this);
        initTitle();
        initData();
    }

    private void initData() {
        new Thread(new LoadDataThread()).start();
    }

    private void initTitle() {
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);
        titleText.setText("宣传与企业文化");
    }


    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        return HttpDataService.getArticlePublishStatics();
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        super.httpHandlerResultData(msg, jsonObject);
        String json = jsonObject.toString();
        CorporateCultureInfo corporateCultureInfo = new Gson().     fromJson(json, CorporateCultureInfo.class);
        handleData(corporateCultureInfo);
    }

    private void handleData(CorporateCultureInfo corporateCultureInfo) {
        List<List<String>> cellColList = new ArrayList<>();
        for (CorporateCultureInfo.ObjectsBean objBean : corporateCultureInfo.getObjects()) {
            List<String> cellList = new ArrayList<>();
            cellList.add(objBean.getWanyuanshiye() + "");
            cellList.add(objBean.getGongsiguanwei() + "");
            cellList.add(objBean.getHangtianchangzheng() + "");
            cellList.add(objBean.getChangzhengtai() + "");
            cellList.add(objBean.getYuanweixin() + "");
            cellList.add(objBean.getYuanguanwang() + "");
            cellList.add(objBean.getHangtianbao() + "");
            cellList.add(objBean.getHangtianshoujibao() + "");
            cellList.add(objBean.getJituanguanwei() + "");
            cellList.add(objBean.getJituanguanwang() + "");

            cellColList.add(cellList);
        }

        List<String> rowTitles = new ArrayList<>();
        rowTitles.add("万源视野");
        rowTitles.add("公司官微");
        rowTitles.add("航天长征");
        rowTitles.add("长征台");
        rowTitles.add("院微公众号");
        rowTitles.add("院官网");
        rowTitles.add("中国航天报");
        rowTitles.add("中国航天手机报");
        rowTitles.add("集团官微");
        rowTitles.add("集团官网");

        List<String> colTitles = new ArrayList<>();
        colTitles.add("建筑");
        colTitles.add("保障中心");
        colTitles.add("物业");
        colTitles.add("通信");
        colTitles.add("动力");
        colTitles.add("幼儿园");
        colTitles.add("地产运营部");
        colTitles.add("博物馆");
        colTitles.add("监理");
        colTitles.add("深万源");
        colTitles.add("设计");
        colTitles.add("绿化");
        colTitles.add("大连公司");

        scrollablePanelAdapter = new ScrollablePanelAdapter(this);
        excelPanel.setAdapter(scrollablePanelAdapter);
        scrollablePanelAdapter.setAllData(colTitles, rowTitles, cellColList);
    }

    @OnClick({R.id.topbar_left_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;

        }
    }
}
