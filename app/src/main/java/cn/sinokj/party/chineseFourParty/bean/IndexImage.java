/** 
 * @Title IndexImage.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月5日 下午2:30:03
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import java.io.Serializable;

/**
 * @ClassName IndexImage
 * @Description 首页图片显示
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年5月24日 下午2:57:49
 **/
@SuppressWarnings("serial")
public class IndexImage implements Serializable {

	/**
	 * bCustom : true
	 * dtReg : 2017-10-25 08:02:44
	 * nId : 837
	 * nLogin : 0
	 * nOrder : 0
	 * nShow : 1
	 * nclick : 14
	 * url : http://218.244.60.138:80/eDaJia/toCommonPage.do?nId=837
	 * vcDescribe : 习近平主持大会并发表重要讲话
	 * vcPath : http://218.244.60.138:80/eDaJiaManager/ueditor/jsp/upload/image/20171025/1508889721860001553.png
	 * vcPlatform : e党建
	 * vcRegister : wanyuanshiye
	 * vcTitle : 中国共产党第十九次全国代表大会在京闭幕
	 * vcType : 首页广告
	 * vcUrl :
	 */

	public boolean bCustom;
	public String dtReg;
	public int nId;
	public int nLogin;
	public int nOrder;
	public int nShow;
	public int nclick;
	public String url;
	public String vcDescribe;
	public String vcPath;
	public String vcPlatform;
	public String vcRegister;
	public String vcTitle;
	public String vcType;
	public String vcUrl;
	public int nShared;
	public int nStatus;
}
