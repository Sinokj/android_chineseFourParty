package cn.sinokj.party.chineseFourParty.service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.utils.http.HttpUtils;
import cn.sinokj.party.chineseFourParty.utils.logs.Logger;


/**
 * Created by azzbcc on 16-5-14.
 */
public class HttpDataService {

    private static Logger logger = Logger.getLogger();
    private static Map<String, JSONObject> homeData;
    private static Map<String, JSONObject> myTokenMoney;
    private static Map<String, JSONObject> myTokenPayLog;

    /**
     * 对结果封装
     *
     * @param result   将要封装到的map
     * @param response 需要封装的字符串
     * @throws Exception json格式错误异常
     * @author azzbcc E-mail: azzbcc@sina.com
     * @version 创建时间：2015年10月27日 下午5:53:33
     */
    private static void handleResult(Map<String, JSONObject> result, String response) throws Exception {
        if (null != response) {
        JSONObject jsonObject = new JSONObject();
        if (response.startsWith("[")) {
            JSONArray resultArray = new JSONArray(response);
            jsonObject.put(HttpConstants.OBJECTS, resultArray);
        } else {
            jsonObject = new JSONObject(response);
        }
        result.put(HttpConstants.RESULT, jsonObject);
    }
}

    /**
     * @param vcPlatform
     * @param vcType
     * @return
     * @Title getImgs
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月6日 下午8:42:15
     **/
    public static Map<String, JSONObject> getImgs(String vcPlatform, String vcType) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcPlatform", vcPlatform);
            params.put("vcType", vcType);
            String s = HttpUtils.post(HttpConstants.GET_IMGS, params);
            handleResult(result, s);
        } catch (Exception e) {
            logger.e(e);
        }
        return result;
    }

    /**
     * @param
     * @param vcType
     * @param nPageNo
     * @param nPageSize
     * @return
     * @Title getNews
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月7日 下午2:18:58
     **/
    public static Map<String, JSONObject> getNews(String vcType, String nPageNo, String nPageSize, String nCommitteeId, String nTagId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcType", vcType);
            params.put("nPageNo", nPageNo);
            params.put("nPageSize", nPageSize);
            params.put("nCommitteeId", nCommitteeId);
            params.put("nTagId", nTagId);
            String s = HttpUtils.post(HttpConstants.GET_NEWS, params);
            handleResult(result, s);
        } catch (Exception e) {
            logger.e(e);
        }
        return result;
    }

    /**
     * @param
     * @param password
     * @return
     * @Title login
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月11日 上午11:50:09
     **/
    public static Map<String, JSONObject> login(String userId, String password) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("userId", userId);
            params.put("password", password);
            String s = HttpUtils.post(HttpConstants.LOGIN, params);
            handleResult(result, s);
        } catch (Exception e) {
            logger.e(e);
        }
        return result;
    }

    /**
     * @param
     * @param
     * @return
     * @Title sendSMS
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月11日 下午2:20:04
     **/
    public static Map<String, JSONObject> sendSMSNew(String vcLinkTel, String vcIMEI) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcLinkTel", vcLinkTel);
            params.put("vcIMEI", vcIMEI);
            params.put("vcType", "1");  // 1是忘记密码，2是改密码
            String s = HttpUtils.post(HttpConstants.SEND_SMS_NEW, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param oldPassword
     * @param newPassword
     * @return
     * @Title modiPsw
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月11日 下午3:51:41
     **/
    public static Map<String, JSONObject> modiPsw(String oldPassword, String newPassword) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("oldPassword", oldPassword);
            params.put("newPassword", newPassword);
            String s = HttpUtils.post(HttpConstants.MODI_PSW, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param vcTel
     * @param newPassword
     * @return
     * @Title resetPsw
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月11日 下午4:49:13
     **/
    public static Map<String, JSONObject> forgotPswNew(String vcTel, String newPassword, String smsCode) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcTel", vcTel);
            params.put("newPassword", newPassword);
            params.put("smsCode", smsCode);
            String s = HttpUtils.post(HttpConstants.FORGOT_PSW_NEW, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title getTopics
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月13日 上午10:31:50
     **/
    public static Map<String, JSONObject> getTopics(String vcClassify,String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcClassify", vcClassify);
            params.put("nCommitteeId", nCommitteeId);
            String s = HttpUtils.post(HttpConstants.GET_TOPICS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param nTopicsId
     * @return
     * @Title getTopicDetail
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月13日 下午4:53:59
     **/
    public static Map<String, JSONObject> getTopicDetail(String nTopicsId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            String s = HttpUtils.post(HttpConstants.GET_TOPIC_DETAIL, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param
     * @return
     * @Title beginAnswer
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月14日 下午4:05:18
     **/
    public static Map<String, JSONObject> beginAnswer(String nTopicsId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            String s = HttpUtils.post(HttpConstants.BEGIN_ANSWER, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param
     * @return
     * @Title getAnswerd
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月14日 下午4:32:05
     **/
    public static Map<String, JSONObject> getAnswerd(String nTopicsId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            String s = HttpUtils.post(HttpConstants.GET_ANSWERD, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param nTopicsId
     * @param nTitleId
     * @param nTitleScore
     * @param vcAnswer
     * @return
     * @Title updateAnswer
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月14日 下午4:21:13
     **/
    public static Map<String, JSONObject> updateAnswer(String nTopicsId,
                                                       String nTitleId, String nTitleScore, String vcAnswer) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            params.put("nTitleId", nTitleId);
            params.put("nTitleScore", nTitleScore);
            params.put("vcAnswer", vcAnswer);
            String s = HttpUtils.post(HttpConstants.UPDATE_ANSWER, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param nTopicsId
     * @return
     * @Title submitTopics
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月15日 下午2:51:30
     **/
    public static Map<String, JSONObject> submitTopics(String nTopicsId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            String s = HttpUtils.post(HttpConstants.SUBMIT_TOPICS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title getDiscussionsTopics
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月19日 上午9:48:17
     **/
    public static Map<String, JSONObject> getDiscussionsTopics() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_DISCUSSIONS_TOPICS);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> submitGread(String nTopicsId, String vcTel, Map<String, String> map) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            params.put("vcTel", vcTel);
            params.putAll(map);
            String s = HttpUtils.post(HttpConstants.SUBMIT_GREAD, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title getVoteTopics
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月21日 下午3:28:54
     **/
    public static Map<String, JSONObject> getVoteTopics() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_VOTE_TOPICS);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param nTopicsId
     * @param vcVoteOptions
     * @return
     * @Title submitVote
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月21日 下午5:54:35
     **/
    public static Map<String, JSONObject> submitVote(String nTopicsId, String vcVoteOptions) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            params.put("vcVoteOptions", vcVoteOptions);
            String s = HttpUtils.post(HttpConstants.SUBMIT_VOTE, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title getTopicsList
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月22日 上午10:10:28
     **/
    public static Map<String, JSONObject> getTopicsList() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_TOPICS_LIST);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title getTopicsList
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月22日 上午10:10:28
     **/
    public static Map<String, JSONObject> getOpinionTopics(String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nCommitteeId", nCommitteeId);
            String s = HttpUtils.post(HttpConstants.GET_OPINION_TOPICS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

//	/**
//	 * @Title submitOpinion
//	 * @Description TODO(这里用一句话描述这个函數的作用)
//	 * @author azzbcc Email: azzbcc@sina.com
//	 * @date 创建时间 2016年7月22日 上午10:54:30
//	 * @param nTopicsId
//	 * @param vcContent
//	 * @return
//	 **/
//	public static Map<String, JSONObject> submitOpinion(String nTopicsId, String vcContent) {
//		Map<String, JSONObject> result = new HashMap<String, JSONObject>();
//		try {
//			Map<String, String> params = new LinkedHashMap<String, String>();
//			params.put("nTopicsId", nTopicsId);
//			params.put("vcContent", vcContent);
//			String s = HttpUtils.post(HttpConstants.SUBMIT_OPINION, params);
//			handleResult(result, s);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
//	}

    /**
     * @return
     * @Title getAllMyInfo
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月26日 上午10:33:43
     **/
    public static Map<String, JSONObject> getAllMyInfo() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_ALL_MY_INFO);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param picPath
     * @return
     * @Title updateHeadImg
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月27日 上午10:35:47
     **/
    public static Map<String, JSONObject> updateHeadImg(String picPath) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            List<String> fileList = new ArrayList<String>();
            fileList.add(picPath);
            Map<String, String> params = new LinkedHashMap<String, String>();
            String s = HttpUtils.post(HttpConstants.UPDATE_HEAD_IMG, fileList, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title update
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月27日 上午11:33:04
     **/
    public static Map<String, JSONObject> update() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.UPDATE);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param vcPushId
     * @param vcKey
     * @param vcAPPName
     * @param vcPlatform
     * @return
     * @Title bindPushInfo
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月27日 下午3:55:22
     **/
    public static Map<String, JSONObject> bindPushInfo(String vcPushId,
                                                       String vcKey, String vcAPPName, String vcPlatform) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcPushId", vcPushId);
            //params.put("vcKey", vcKey);
            params.put("vcPlatform", vcPlatform);
            // params.put("vcAPPName", vcAPPName);
            String s = HttpUtils.post(HttpConstants.BIND_PUSH_INFO, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     * @Title logout
     * @Description TODO(这里用一句话描述这个函數的作用)
     * @author azzbcc Email: azzbcc@sina.com
     * @date 创建时间 2016年7月29日 上午11:37:11
     **/
    public static Map<String, JSONObject> logout() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.LOGOUT);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getUnReadList() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_UN_READ_LIST);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getPartyArticleList(String nCommitteeId, String vcType) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nCommitteeId", nCommitteeId);
            params.put("vcType", vcType);
            String s = HttpUtils.post(HttpConstants.GET_PARTY_ARTICLE_LIST, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getArticle() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_ARTICLE);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getSingleReadStatus(String nArticleId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nArticleId", nArticleId);
            String s = HttpUtils.post(HttpConstants.GET_SINGLE_READ_STATUS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getArticleReadStatus(String nArticleId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nArticleId", nArticleId);
            String s = HttpUtils.post(HttpConstants.GET_ARTICLE_READ_STATUS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getPocketTutorLog(String nPageNo, String nPageSize, String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nPageNo", nPageNo);
            params.put("nPageSize", nPageSize);
            params.put("nCommitteeId", nCommitteeId);
            String s = HttpUtils.post(HttpConstants.GET_POCKET_TUTOR_LOG, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getPartyTypeList() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_PARTY_TYPE_LIST);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getPartyArticleListByType(String vcType) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("vcType", vcType);
            String s = HttpUtils.post(HttpConstants.GET_PARTY_ARTICLE_LIST_BY_TYPE, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 支付宝
     *
     * @param money
     * @param vcSource
     * @return
     */
    public static Map<String, JSONObject> getAliPartyInfo(String money, String vcSource) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("money", money);
            params.put("vcSource", vcSource);
            String s = HttpUtils.post(HttpConstants.PARTY_ALI_PAYCHECK, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 微信
     */
    public static Map<String, JSONObject> getWeChatPartyInfo(String money, String vcSource) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("money", money);
            params.put("vcSource", vcSource);
            String s = HttpUtils.post(HttpConstants.PARTY_WX_PAY, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 微信
     */
    public static Map<String, JSONObject> wxGetPayResult(String nOrderId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nOrderId", nOrderId);
            String s = HttpUtils.post(HttpConstants.WX_GETPAY_RESULT, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 宣传与企业文化接口
     *
     * @return
     */
    public static Map<String, JSONObject> getArticlePublishStatics() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_ARTICLE_PUBLISH_STATICS);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 我的党费接口
     *
     * @return
     */
    public static Map<String, JSONObject> getMyPartyLog() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_MYPARTYLOG);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 首页搜索接口
     *
     * @return
     */
    public static Map<String, JSONObject> searchNews(int nPageNo, int nPageSize, String vcTitle, String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            //params.put("vcPlatform", vcPlatform);
            params.put("vcTitle", vcTitle);
            params.put("nPageNo", nPageNo + "");
            params.put("nPageSize", nPageSize + "");
            params.put("nCommitteeId", App.nCommitteeId);
            String s = HttpUtils.post(HttpConstants.SEARCH_NEWS, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 问卷调查
     *
     * @return
     */
    public static Map<String, JSONObject> getTopicsContent(int nId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nId", nId + "");
            String s = HttpUtils.post(HttpConstants.GETTOPICS_CONTENT, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 首页数据
     *
     * @return
     */
    public static Map<String, JSONObject> getHomeData(String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nCommitteeId", nCommitteeId);
            String s = HttpUtils.post(HttpConstants.GET_HOMEDATA, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 首页党委集合
     *
     * @return
     */
    public static Map<String, JSONObject> getCommitteeList() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GETCOMMIT_LIST);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取文章Tag
     */
    public static Map<String, JSONObject> getArticleTagList(String nModuleId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nModuleId", nModuleId);
            String s = HttpUtils.post(HttpConstants.GET_ARTICLE_TAG, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 意见征集
     *
     * @param nTopicsId
     * @param vcSuggestion
     * @return
     */
    public static Map<String, JSONObject> submitOpinion(String nTopicsId, String vcSuggestion) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nTopicsId", nTopicsId);
            params.put("vcSuggestion", vcSuggestion);
            String s = HttpUtils.post(HttpConstants.SUBMIT_OPINION, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> startphoto() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.START_PHOTO);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getMyTokenMoney() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_MYTOKEN_MONEY);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, JSONObject> getMyTokenPayLog() {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            String s = HttpUtils.post(HttpConstants.GET_MYTOKEN_PAYLOG);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 问卷调查
     *
     * @return
     */
    public static Map<String, JSONObject> getVerfityModule(int nId, String nCommitteeId) {
        Map<String, JSONObject> result = new HashMap<String, JSONObject>();
        try {
            Map<String, String> params = new LinkedHashMap<String, String>();
            params.put("nId", nId + "");
            params.put("nCommitteeId", nCommitteeId + "");
            String s = HttpUtils.post(HttpConstants.GET_PERMISSION, params);
            handleResult(result, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}