package cn.sinokj.party.chineseFourParty.bean;

import java.util.List;

/**
 * Created by l on 2017/6/30.
 */

public class CorporateCultureInfo {

    /**
     * nRes : 1
     * objects : [{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":1,"nOther":0,"vcMemo":"","vcPartyGroup":"建筑","wanyuanshiye":1,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":2,"nOther":0,"vcPartyGroup":"保障中心","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":3,"nOther":0,"vcPartyGroup":"物业","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":4,"nOther":0,"vcPartyGroup":"通信","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":5,"nOther":0,"vcPartyGroup":"动力","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":6,"nOther":0,"vcPartyGroup":"幼儿园","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":7,"nOther":0,"vcPartyGroup":"地产运营部","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":4,"gongsiguanwei":11,"hangtianbao":2,"hangtianchangzheng":7,"hangtianshoujibao":3,"jituanguanwang":1,"jituanguanwei":1,"nId":8,"nOther":10,"vcPartyGroup":"博物馆","wanyuanshiye":13,"yuanguanwang":5,"yuanweixin":3},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":9,"nOther":0,"vcPartyGroup":"监理","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":10,"nOther":0,"vcPartyGroup":"深万源","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":11,"nOther":0,"vcPartyGroup":"设计","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":12,"nOther":0,"vcPartyGroup":"绿化","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0},{"changzhengtai":0,"gongsiguanwei":0,"hangtianbao":0,"hangtianchangzheng":0,"hangtianshoujibao":0,"jituanguanwang":0,"jituanguanwei":0,"nId":13,"nOther":0,"vcPartyGroup":"大连公司","wanyuanshiye":0,"yuanguanwang":0,"yuanweixin":0}]
     */

    private int nRes;
    private List<ObjectsBean> objects;

    public int getNRes() {
        return nRes;
    }

    public void setNRes(int nRes) {
        this.nRes = nRes;
    }

    public List<ObjectsBean> getObjects() {
        return objects;
    }

    public void setObjects(List<ObjectsBean> objects) {
        this.objects = objects;
    }

    public static class ObjectsBean {
        /**
         * changzhengtai : 0
         * gongsiguanwei : 0
         * hangtianbao : 0
         * hangtianchangzheng : 0
         * hangtianshoujibao : 0
         * jituanguanwang : 0
         * jituanguanwei : 0
         * nId : 1
         * nOther : 0
         * vcMemo :
         * vcPartyGroup : 建筑
         * wanyuanshiye : 1
         * yuanguanwang : 0
         * yuanweixin : 0
         */

        private int changzhengtai;
        private int gongsiguanwei;
        private int hangtianbao;
        private int hangtianchangzheng;
        private int hangtianshoujibao;
        private int jituanguanwang;
        private int jituanguanwei;
        private int nId;
        private int nOther;
        private String vcMemo;
        private String vcPartyGroup;
        private int wanyuanshiye;
        private int yuanguanwang;
        private int yuanweixin;

        public int getChangzhengtai() {
            return changzhengtai;
        }

        public void setChangzhengtai(int changzhengtai) {
            this.changzhengtai = changzhengtai;
        }

        public int getGongsiguanwei() {
            return gongsiguanwei;
        }

        public void setGongsiguanwei(int gongsiguanwei) {
            this.gongsiguanwei = gongsiguanwei;
        }

        public int getHangtianbao() {
            return hangtianbao;
        }

        public void setHangtianbao(int hangtianbao) {
            this.hangtianbao = hangtianbao;
        }

        public int getHangtianchangzheng() {
            return hangtianchangzheng;
        }

        public void setHangtianchangzheng(int hangtianchangzheng) {
            this.hangtianchangzheng = hangtianchangzheng;
        }

        public int getHangtianshoujibao() {
            return hangtianshoujibao;
        }

        public void setHangtianshoujibao(int hangtianshoujibao) {
            this.hangtianshoujibao = hangtianshoujibao;
        }

        public int getJituanguanwang() {
            return jituanguanwang;
        }

        public void setJituanguanwang(int jituanguanwang) {
            this.jituanguanwang = jituanguanwang;
        }

        public int getJituanguanwei() {
            return jituanguanwei;
        }

        public void setJituanguanwei(int jituanguanwei) {
            this.jituanguanwei = jituanguanwei;
        }

        public int getNId() {
            return nId;
        }

        public void setNId(int nId) {
            this.nId = nId;
        }

        public int getNOther() {
            return nOther;
        }

        public void setNOther(int nOther) {
            this.nOther = nOther;
        }

        public String getVcMemo() {
            return vcMemo;
        }

        public void setVcMemo(String vcMemo) {
            this.vcMemo = vcMemo;
        }

        public String getVcPartyGroup() {
            return vcPartyGroup;
        }

        public void setVcPartyGroup(String vcPartyGroup) {
            this.vcPartyGroup = vcPartyGroup;
        }

        public int getWanyuanshiye() {
            return wanyuanshiye;
        }

        public void setWanyuanshiye(int wanyuanshiye) {
            this.wanyuanshiye = wanyuanshiye;
        }

        public int getYuanguanwang() {
            return yuanguanwang;
        }

        public void setYuanguanwang(int yuanguanwang) {
            this.yuanguanwang = yuanguanwang;
        }

        public int getYuanweixin() {
            return yuanweixin;
        }

        public void setYuanweixin(int yuanweixin) {
            this.yuanweixin = yuanweixin;
        }
    }
}
