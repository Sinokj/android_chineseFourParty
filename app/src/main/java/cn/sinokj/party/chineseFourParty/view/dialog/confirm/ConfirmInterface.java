package cn.sinokj.party.chineseFourParty.view.dialog.confirm;

import android.content.DialogInterface;

/**
 * Created by azzbcc on 16-5-16.
 */
public interface ConfirmInterface {

    /**
     * 当按下确定键时执行动作
     * @author azzbcc E-mail: azzbcc@sina.com
     * @version 创建时间：2016年1月21日 下午5:22:00
     */
    public void onOkButton();

    /**
     * 当按下取消键时执行动作
     * @author azzbcc E-mail: azzbcc@sina.com
     * @version 创建时间：2016年1月21日 下午5:22:00
     */
    public void onCancelButton();

    /**
     * 消失监听
     * @param dialog
     */
    void onDismiss(DialogInterface dialog)  ;
}
