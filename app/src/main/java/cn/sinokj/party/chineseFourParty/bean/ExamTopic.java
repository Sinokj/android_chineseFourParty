package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ExamTopic implements Serializable {
	@Expose
	private String vcRegister;
	@Expose
	private String vcExamGroupName;
	@Expose
	private int answerdNumber;
	@Expose
	private String dtReg;
	@Expose
	private int nPass;
	@Expose
	private String vcExamGroupId;
	@Expose
	private int nTopicsTotal;
	@Expose
	private int nTimer;
	@Expose
	private int examedNumber;
	@Expose
	private String vcTitle;
	@Expose
	private int nStatus;
	@Expose
	private int nTotal;
	@Expose
	private int nId;
	@Expose
	private int isBegin;
	@Expose
	private boolean isPass;

	public String getVcClassify() {
		return vcClassify;
	}

	@Expose
	private int nScore;

	public void setVcClassify(String vcClassify) {
		this.vcClassify = vcClassify;
	}

	@Expose
	private String vcClassify;

	public void setDtEnd(String dtEnd) {
		this.dtEnd = dtEnd;
	}

	public String getDtEnd() {
		return dtEnd;
	}

	@Expose
	private String dtEnd;

	public int getnScore() {
		return nScore;
	}

	public void setnScore(int nScore) {
		this.nScore = nScore;
	}

	public boolean isPass() {
		return isPass;
	}

	public void setPass(boolean isPass) {
		this.isPass = isPass;
	}

	public String getVcRegister() {
		return vcRegister;
	}

	public void setVcRegister(String vcRegister) {
		this.vcRegister = vcRegister;
	}

	public String getVcExamGroupName() {
		return vcExamGroupName;
	}

	public void setVcExamGroupName(String vcExamGroupName) {
		this.vcExamGroupName = vcExamGroupName;
	}

	public int getAnswerdNumber() {
		return answerdNumber;
	}

	public void setAnswerdNumber(int answerdNumber) {
		this.answerdNumber = answerdNumber;
	}

	public String getDtReg() {
		return dtReg;
	}

	public void setDtReg(String dtReg) {
		this.dtReg = dtReg;
	}

	public int getnPass() {
		return nPass;
	}

	public void setnPass(int nPass) {
		this.nPass = nPass;
	}

	public String getVcExamGroupId() {
		return vcExamGroupId;
	}

	public void setVcExamGroupId(String vcExamGroupId) {
		this.vcExamGroupId = vcExamGroupId;
	}

	public int getnTopicsTotal() {
		return nTopicsTotal;
	}

	public void setnTopicsTotal(int nTopicsTotal) {
		this.nTopicsTotal = nTopicsTotal;
	}

	public int getnTimer() {
		return nTimer;
	}

	public void setnTimer(int nTimer) {
		this.nTimer = nTimer;
	}

	public int getExamedNumber() {
		return examedNumber;
	}

	public void setExamedNumber(int examedNumber) {
		this.examedNumber = examedNumber;
	}

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public int getnStatus() {
		return nStatus;
	}

	public void setnStatus(int nStatus) {
		this.nStatus = nStatus;
	}

	public int getnTotal() {
		return nTotal;
	}

	public void setnTotal(int nTotal) {
		this.nTotal = nTotal;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public int getIsBegin() {
		return isBegin;
	}

	public void setIsBegin(int isBegin) {
		this.isBegin = isBegin;
	}
}