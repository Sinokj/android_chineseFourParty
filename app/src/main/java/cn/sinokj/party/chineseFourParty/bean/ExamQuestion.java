/** 
 * @Title ExamQuestion.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月13日 下午5:29:00
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName ExamQuestion
 * @Description 试题model
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月13日 下午5:29:00
 **/
@SuppressWarnings("serial")
public class ExamQuestion implements Serializable {
	@Expose
	private String vcAnswer;
	@Expose
	private String vcDescribe;
	@Expose
	private String vcTitle;
	@Expose
	private List<ExamChoose> choices;
	@Expose
	private String vcType;
	@Expose
	private int nTopicsId;
	@Expose
	private int nId;
	@Expose
	private int nCodeId;
	@Expose
	private int nScore;

	public String getVcAnswer() {
		return vcAnswer;
	}

	public void setVcAnswer(String vcAnswer) {
		this.vcAnswer = vcAnswer;
	}

	public String getVcDescribe() {
		return vcDescribe;
	}

	public void setVcDescribe(String vcDescribe) {
		this.vcDescribe = vcDescribe;
	}

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public List<ExamChoose> getChoices() {
		return choices;
	}

	public void setChoices(List<ExamChoose> choices) {
		this.choices = choices;
	}

	public String getVcType() {
		return vcType;
	}

	public void setVcType(String vcType) {
		this.vcType = vcType;
	}

	public int getnTopicsId() {
		return nTopicsId;
	}

	public void setnTopicsId(int nTopicsId) {
		this.nTopicsId = nTopicsId;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public int getnCodeId() {
		return nCodeId;
	}

	public void setnCodeId(int nCodeId) {
		this.nCodeId = nCodeId;
	}

	public int getnScore() {
		return nScore;
	}

	public void setnScore(int nScore) {
		this.nScore = nScore;
	}
}
