/** 
 * @Title DiscussionListAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 下午1:47:53
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.DiscussionActivity;
import cn.sinokj.party.chineseFourParty.bean.DiscussionTopic;
import cn.sinokj.party.chineseFourParty.utils.format.FormatUtils;


/**
 * @ClassName DiscussionListAdapter
 * @Description 民主评议列表容器
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 下午1:47:53
 **/
public class DiscussionListAdapter extends BaseAdapter {

	private Context context;
	private List<DiscussionTopic> list;
	/**
	 * @param context
	 * @param list
	 **/
	public DiscussionListAdapter(Context context, List<DiscussionTopic> list) {
		this.context = context;
		this.list = list;
	}

	/**
	 * @return
	 * @see android.widget.Adapter#getCount()
	 **/
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItem(int)
	 **/
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	/**
	 * @param position
	 * @return
	 * @see android.widget.Adapter#getItemId(int)
	 **/
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 * @see android.widget.Adapter#getView(int, View, ViewGroup)
	 **/
	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final DiscussionTopic discussionTopic = list.get(position);
		
		convertView = LayoutInflater.from(context).inflate(R.layout.discussion_list_item, null);
		
		TextView endText = (TextView) convertView.findViewById(R.id.discussion_list_item_end);
		TextView titleText = (TextView) convertView.findViewById(R.id.discussion_list_item_title);
		TextView startText = (TextView) convertView.findViewById(R.id.discussion_list_item_start);
		TextView statusText = (TextView) convertView.findViewById(R.id.discussion_list_item_status);
		
		if (discussionTopic.getnStatus() == 1) {
			statusText.setVisibility(View.VISIBLE);
		}
		
		titleText.setText(discussionTopic.getVcTitle());
		endText.setText(FormatUtils.formatDate(discussionTopic.getDtEnd(), "MM-dd HH:mm"));
		startText.setText(FormatUtils.formatDate(discussionTopic.getDtBegin(), "MM-dd HH:mm"));

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, DiscussionActivity.class);
				intent.putExtra("discussionTopic", discussionTopic);
				context.startActivity(intent);
			}
		});
		
		return convertView;
	}
}
