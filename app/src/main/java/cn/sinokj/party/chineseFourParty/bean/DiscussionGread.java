/** 
 * @Title DiscussionGread.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 上午11:48:13
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName DiscussionGread
 * @Description 被评议人
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 上午11:48:13
 **/
@SuppressWarnings("serial")
public class DiscussionGread implements Serializable {
	@Expose
	private String beGreadPerson;
	@Expose
	private String vcTel;
	@Expose
	private int nStatus;
	@Expose
	List<DiscussionChoose> optionScore;

	public String getBeGreadPerson() {
		return beGreadPerson;
	}

	public void setBeGreadPerson(String beGreadPerson) {
		this.beGreadPerson = beGreadPerson;
	}

	public String getVcTel() {
		return vcTel;
	}

	public void setVcTel(String vcTel) {
		this.vcTel = vcTel;
	}

	public int getnStatus() {
		return nStatus;
	}

	public void setnStatus(int nStatus) {
		this.nStatus = nStatus;
	}

	public List<DiscussionChoose> getOptionScore() {
		return optionScore;
	}

	public void setOptionScore(List<DiscussionChoose> optionScore) {
		this.optionScore = optionScore;
	}

	@Override
	public String toString() {
		return "DiscussionGread [beGreadPerson = " + beGreadPerson
				+ ", vcTel = " + vcTel + ", nStatus = " + nStatus
				+ ", optionScore = " + optionScore + "]";
	}
}
