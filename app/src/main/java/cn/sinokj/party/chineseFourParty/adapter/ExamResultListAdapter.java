/**
 * @Title ExamResultListAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月27日 上午9:55:34
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.ExamActivity;
import cn.sinokj.party.chineseFourParty.bean.ExamTopic;


/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName ExamResultListAdapter
 * @Description TODO(这里用一句话描述这个类的作用)
 * @date 创建时间 2016年7月27日 上午9:55:34
 **/
public class ExamResultListAdapter extends BaseAdapter {

    private Context context;
    private List<ExamTopic> list;


    public ExamResultListAdapter(Context context, List<ExamTopic> list) {
        this.context = context;
        this.list = list;
    }

    /**
     * @return
     * @see android.widget.Adapter#getCount()
     **/
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItem(int)
     **/
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItemId(int)
     **/
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     **/
    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ExamTopic examTopic = list.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.new_exam_result_list_item, null);

        TextView titleText = (TextView) convertView.findViewById(R.id.exam_result_list_item_title);
        TextView scoreText = (TextView) convertView.findViewById(R.id.exam_result_list_item_score);
        ImageView resultText = (ImageView) convertView.findViewById(R.id.exam_result_list_item_result);

        titleText.setText(examTopic.getVcTitle());
        scoreText.setText(String.valueOf(examTopic.getnScore()));
        if (examTopic.isPass()) {
            resultText.setBackgroundResource(R.drawable.label_success);
        } else {
            resultText.setBackgroundResource(R.drawable.label_lose);
        }
        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExamActivity.class);
                intent.putExtra("examTopic", examTopic);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

}
