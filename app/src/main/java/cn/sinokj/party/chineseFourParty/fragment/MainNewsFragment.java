package cn.sinokj.party.chineseFourParty.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.LoginInActivity;
import cn.sinokj.party.chineseFourParty.activity.SearchActivity;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.ArticleTagListInfo;
import cn.sinokj.party.chineseFourParty.fragment.base.BaseFragment;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.utils.Constans;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * Created by l on 2017/11/12.
 */

public class MainNewsFragment extends BaseFragment {

    private static final int INIT_DATA = 1;
    @BindView(R.id.status_bar_height)
    View mStatusBarHeight;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.topbar_right_img)
    ImageButton mTopbarRightImg;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view)
    View mView;
    @BindView(R.id.vp_coupon)
    ViewPager mVpCoupon;
    private NewsPagerAdapter couponPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.announcement, container, false);
        ButterKnife.bind(this, view);
        initImmersionBar(mStatusBarHeight);
        EventBus.getDefault().register(this);
        mTitle.setText("要闻");
        mTitle.setVisibility(View.VISIBLE);
        mTopbarRightImg.setVisibility(View.VISIBLE);
        new Thread(new LoadDataThread(INIT_DATA)).start();
        return view;
    }

    @OnClick({R.id.topbar_right_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_right_img:
                if (App.loginStatus) {
                    startActivity(new Intent(getActivity(), SearchActivity.class));
                } else {
                    startActivity(new Intent(getActivity(), LoginInActivity.class));
                }
                break;
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        return HttpDataService.getArticleTagList("50");
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        String json = jsonObject.toString();
        Gson gson = new Gson();
        ArticleTagListInfo myPartyLogInfo = gson.fromJson(json, ArticleTagListInfo.class);
        initViewPager(myPartyLogInfo);
    }

    private void initViewPager(ArticleTagListInfo myPartyLogInfo) {
        List<String> newsClasses = new ArrayList<>();
        List<Fragment> list_fragment = new ArrayList<>();
        for (int i = 0; i < myPartyLogInfo.result.size(); i++) {
            ImportantNewsFragment mNewsFragment = new ImportantNewsFragment();
            mNewsFragment.setNclass(myPartyLogInfo.result.get(i).nId);
            list_fragment.add(mNewsFragment);
            newsClasses.add(myPartyLogInfo.result.get(i).vcTagName);
        }
        //隐藏TabLayout
        if (myPartyLogInfo.result.size() == 1) {
            mTabLayout.setVisibility(View.GONE);
            mView.setVisibility(View.GONE);
        }
        FragmentManager fm = getActivity().getSupportFragmentManager();
        couponPagerAdapter = new NewsPagerAdapter(fm, list_fragment, newsClasses);
        mVpCoupon.setAdapter(couponPagerAdapter);
        mVpCoupon.setOffscreenPageLimit(6);
        mTabLayout.setupWithViewPager(mVpCoupon);
        //设置可以滑动
        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEvent(Message msg) {
        switch (msg.what) {
            case Constans.ANNOUNCEMENT_REFRESH:
                if (getUserVisibleHint()) {
                    new Thread(new LoadDataThread(INIT_DATA)).start();
                }
                break;
        }
    }

    class NewsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> list_fragment;
        private final List<String> list_Title;

        public NewsPagerAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String>
                list_Title) {
            super(fm);
            this.list_fragment = list_fragment;
            this.list_Title = list_Title;
        }

        @Override
        public Fragment getItem(int position) {
            return list_fragment.get(position);
        }

        @Override
        public int getCount() {
            return list_fragment.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list_Title.get(position);
        }
    }
}
