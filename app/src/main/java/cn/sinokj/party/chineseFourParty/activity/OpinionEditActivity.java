package cn.sinokj.party.chineseFourParty.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;

/**
 * Created by l on 2017/11/10.
 */

public class OpinionEditActivity extends BaseActivity {
    private static final int SUBMIT = 1;
    @BindView(R.id.et_opinion)
    EditText etOpinion;
    @BindView(R.id.tv_enter)
    TextView tvEnter;

    private String vcUserId;
    private String vcContent;
    private String topicsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opinionedit);
        ButterKnife.bind(this);

        SharedPreferences shared = getSharedPreferences("user", Activity.MODE_PRIVATE);
        vcUserId = shared.getString("username", "");
        topicsId = getIntent().getStringExtra("nId");
    }


    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        if (what == SUBMIT) {
            return HttpDataService.submitOpinion(topicsId, vcContent);
        }
        return super.getDataFunction(what, arg1, arg2, obj);

    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        super.httpHandlerResultData(msg, jsonObject);
        Toast.makeText(this, jsonObject.optString("vcRes"), Toast.LENGTH_SHORT).show();
        if (jsonObject.optInt("nRes") == 1) {
            finish();
        }
    }

    @OnClick({R.id.topbar_left_img, R.id.tv_enter})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
            case R.id.tv_enter:
                vcContent = etOpinion.getText().toString().trim();
                if (TextUtils.isEmpty(vcContent)) {
                    Toast.makeText(this, "请输入内容", Toast.LENGTH_SHORT).show();
                    return;
                }
                new Thread(new LoadDataThread(SUBMIT)).start();
                break;
        }
    }
}
