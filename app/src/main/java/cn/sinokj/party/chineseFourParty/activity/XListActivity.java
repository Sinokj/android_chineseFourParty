/**
 * @Title XListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月7日 上午9:15:50
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.XListAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.IndexImage;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;
import cn.sinokj.party.chineseFourParty.view.xlist.XListView;

/**
 * @ClassName XListActivity
 * @Description 列表界面模板
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月7日 上午9:15:50
 **/
public class XListActivity extends BaseActivity {
    public static final int TO_CHANGE_READ_STATUS = 3;
    private static final int REFRESH = 0;
    private static final int INIT_DATA = 1;
    private static final int LOAD_MORE = 2;
    private static final int QUERY_SIZE = 10;
    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public ImageButton topLeftImage;
    @BindView(R.id.xlist_listview)
    public XListView dataListView;

    private int nPage = 1;
    private String functionType;
    private IndexImage indexImage;
    private XListAdapter xListAdapter;
    private List<IndexImage> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlist);
        ButterKnife.bind(this);
        functionType = getIntent().getStringExtra("functionType");
        if ("主题教育".equals(functionType)) {
            titleText.setText("专题教育");
        } else {
            titleText.setText(functionType);
        }
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);
        dataListView.setXListViewListener(ixListViewListener);
        dataListView.setOnItemClickListener(onItemClickListener);

        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread(INIT_DATA)).start();
    }

    @OnClick(R.id.topbar_left_img)
    public void onClick(View view) {
        finish();
    }

    private OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // XListView 默认position为1
            indexImage = dataList.get(position - 1);
            Intent intent = new Intent(XListActivity.this, IndexImageWebActivity.class);
            intent.putExtra("url", indexImage.url);
            intent.putExtra("icon", indexImage.vcPath);
            //intent.putExtra("title", indexImage.getVcTitle());
            intent.putExtra("title", functionType);
            intent.putExtra("nShared", indexImage.nShared);
            intent.putExtra("describe", indexImage.vcDescribe);
            startActivityForResult(intent, TO_CHANGE_READ_STATUS);
            /*startActivity(intent);*/
        }
    };

    private XListView.IXListViewListener ixListViewListener = new XListView.IXListViewListener() {
        @Override
        public void onRefresh() {
            nPage = 1;
            DialogShow.showRoundProcessDialog(XListActivity.this);
            new Thread(new LoadDataThread(REFRESH)).start();
        }

        @Override
        public void onLoadMore() {
            nPage += 1;
            new Thread(new LoadDataThread(LOAD_MORE)).start();
        }
    };

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        if (what == TO_CHANGE_READ_STATUS) {
            return HttpDataService.getSingleReadStatus(String.valueOf(indexImage.nId));//以前是indexImage.vcType
        }
        return HttpDataService.getNews(functionType, String.valueOf(nPage), String.valueOf(QUERY_SIZE),
                App.nCommitteeId,"");
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        if (msg.what == TO_CHANGE_READ_STATUS) {
            indexImage.nStatus = (jsonObject.optInt(HttpConstants.RESULT));
            xListAdapter.notifyDataSetChanged();
            return;
        }
        JSONArray jsonArray = jsonObject.optJSONArray(HttpConstants.OBJECTS);
        List<IndexImage> list = new Gson().fromJson(jsonArray.toString(),
                new TypeToken<List<IndexImage>>() {
                }.getType());
        switch (msg.what) {
            case REFRESH:
                dataListView.stopRefresh();
            case INIT_DATA:
                if (list.size() == 0) {
                    Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
                }
                dataList = new ArrayList<IndexImage>();
                dataList.addAll(list);
                xListAdapter = new XListAdapter(this, dataList);
                dataListView.setAdapter(xListAdapter);
                break;
            case LOAD_MORE:
                dataList.addAll(list);
                xListAdapter.notifyDataSetChanged();
                break;
        }
        dataListView.setPullLoadEnable(list.size() == QUERY_SIZE);
        DialogShow.closeDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (App.loginStatus && resultCode == Activity.RESULT_OK && requestCode == TO_CHANGE_READ_STATUS) {
            new Thread(new LoadDataThread(TO_CHANGE_READ_STATUS)).start();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
