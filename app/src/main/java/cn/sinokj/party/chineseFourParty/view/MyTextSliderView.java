package cn.sinokj.party.chineseFourParty.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import cn.sinokj.party.chineseFourParty.R;


/**
 * Created by l on 2017/8/7.
 */

public class MyTextSliderView extends BaseSliderView {
    public MyTextSliderView(Context context) {
        super(context);
    }
    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.render_type_text,null);
        ImageView target = (ImageView)v.findViewById(R.id.daimajia_slider_image);
        TextView description = (TextView)v.findViewById(R.id.description);
        description.setText(getDescription());
        description.setTextSize(12);
        bindEventAndShow(v, target);
        return v;
    }
}
