/**
 * @Title OpinionListAdapter.java
 * @Package cn.sinokj.party.building.adapter
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月22日 上午10:22:19
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.OpinionItemActivity;
import cn.sinokj.party.chineseFourParty.bean.OpinionListInfo;


/**
 * @ClassName OpinionListAdapter
 * @Description 意见征集容器
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月22日 上午10:22:19
 **/
public class OpinionListAdapter extends BaseAdapter {


    private Context context;
    private List<OpinionListInfo.ObjectsBean> list;

    /**
     **/
    public OpinionListAdapter(Context context, List<OpinionListInfo.ObjectsBean> list) {
        this.context = context;
        this.list = list;
    }

    /**
     * @return
     * @see android.widget.Adapter#getCount()
     **/
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItem(int)
     **/
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**
     * @param position
     * @return
     * @see android.widget.Adapter#getItemId(int)
     **/
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see Adapter#getView(int, View,
     *      ViewGroup)
     **/
    //@SuppressLint({ "InflateParams", "ViewHolder" })
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final OpinionListInfo.ObjectsBean objectsBean = list.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.opinion_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.itemTime = (TextView) convertView.findViewById(R.id.item_time);
            viewHolder.title = (TextView) convertView.findViewById(R.id.item_topic);
            viewHolder.status = (ImageView) convertView.findViewById(R.id.image_status);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.title.setText(objectsBean.vcTopic);
        viewHolder.itemTime.setText(objectsBean.dtBegin.substring(0, 10) + " - " + objectsBean.dtEnd.substring(0, 10));
        switch (objectsBean.vcstatus) {
            case "进行中":
                viewHolder.status.setImageResource(R.drawable.status1);
                break;
            case "已结束":
                viewHolder.status.setImageResource(R.drawable.status2);
                break;
            case "已提交":
                viewHolder.status.setImageResource(R.drawable.status3);
                break;
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OpinionItemActivity.class);
                intent.putExtra("nId", objectsBean.nId);
                intent.putExtra("title",objectsBean.vcTopic);
                intent.putExtra("content",objectsBean.vcContent);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    class ViewHolder {
        public TextView title;
        public TextView itemTime;
        public ImageView status;
    }
}
