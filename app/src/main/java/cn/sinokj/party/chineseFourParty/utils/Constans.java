package cn.sinokj.party.chineseFourParty.utils;

/**
 * Created by l on 2017/10/30.
 */

public interface Constans {
    String TYPE_REFRESH = "type_refresh";
    String TYPE_LOADMORE = "type_loadmore";
    int SHOW_MINE = 10001;
    int SHOW_LOGIN = 10002;
    int MINE_REFRESH = 10003;
    int HOME_REFRESH = 10004;
    int REFRESH = 10005;
    int ANNOUNCEMENT_REFRESH = 10006;
    int GET_SUB_AREA = 10007;
    int GET_LOCATION_PERMISSION = 1028;
    int SHOW_HOME = 1008;
}
