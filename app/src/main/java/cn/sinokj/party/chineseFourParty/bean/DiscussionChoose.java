/** 
 * @Title DiscussionChoose.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 上午11:53:14
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @ClassName DiscussionChoose
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 上午11:53:14
 **/
@SuppressWarnings("serial")
public class DiscussionChoose implements Serializable {
	@Expose
	private int nTopicsId;
	@Expose
	private String vcOption;
	@Expose
	private int nId;
	@Expose
	private int nScore;

	public int getnTopicsId() {
		return nTopicsId;
	}

	public void setnTopicsId(int nTopicsId) {
		this.nTopicsId = nTopicsId;
	}

	public String getVcOption() {
		return vcOption;
	}

	public void setVcOption(String vcOption) {
		this.vcOption = vcOption;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public int getnScore() {
		return nScore;
	}

	public void setnScore(int nScore) {
		this.nScore = nScore;
	}

	@Override
	public String toString() {
		return "DiscussionChoose [nTopicsId = " + nTopicsId + ", vcOption = "
				+ vcOption + ", nId = " + nId + ", nScore = " + nScore + "]";
	}

}
