/**
 * @Title ExamQuestionAnswered.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月14日 下午4:49:56
 * @version V1.0
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author azzbcc Email: azzbcc@sina.com
 * @ClassName ExamQuestionAnswered
 * @Description 已选的答案
 * @date 创建时间 2016年7月14日 下午4:49:56
 **/
@SuppressWarnings("serial")
public class ExamQuestionAnswered implements Serializable {
    @Expose
    private String vcAnswer;
    @Expose
    private int nTitleId;
    @Expose
    private boolean isSubmit;

    public ExamQuestionAnswered(int nTitleId) {
        this.nTitleId = nTitleId;
    }

    public String getVcAnswer() {
        return vcAnswer;
    }

    public void setVcAnswer(String vcAnswer) {
        this.vcAnswer = vcAnswer + ",";
    }

    public int getnTitleId() {
        return nTitleId;
    }

    public void setnTitleId(int nTitleId) {
        this.nTitleId = nTitleId;
    }

    public boolean isSubmit() {
        return isSubmit;
    }

    public void setSubmit(boolean isSubmit) {
        this.isSubmit = isSubmit;
    }

    public boolean isAnswered() {
        if (vcAnswer == null || "".equals(vcAnswer)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isChoosed(char c) {
        if (!isAnswered()) {
            return false;
        }
        return vcAnswer.indexOf(c) >= 0;
    }

    public void addVcAnswer(String vcAnswer) {
        if (isAnswered()) {
            this.vcAnswer = this.vcAnswer + vcAnswer + ",";
        } else {
            setVcAnswer(vcAnswer);
        }
    }

    public void delVcAnswer(String vcAnswer) {
        this.vcAnswer = this.vcAnswer.replace(vcAnswer + ",", "");
    }

    public boolean isTrue(String trueAnswer) {
        if (trueAnswer == null || trueAnswer.equals("")) {
            return false;
        }
        if (!isAnswered()) {
            return false;
        }
        List<String> ownList = Arrays.asList(vcAnswer.split(" "));
        List<String> trueList = Arrays.asList(trueAnswer.split(" "));

        if (ownList.size() != trueList.size()) {
            return false;
        }
        // 对两组答案排序
        Collections.sort(ownList);
        Collections.sort(trueList);

        for (int i = 0; i < ownList.size(); i++) {
            if (ownList.get(i).charAt(0) != trueList.get(i).charAt(0)) {
                return false;
            }
        }
        return true;
    }
}
