/** 
 * @Title MessageDialogUtils.java
 * @Package cn.sinokj.mobile.business.utils.message.dialog
 * @Description 显示数据列表选择数据
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年3月3日 下午2:17:22
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.view.dialog.message;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MessageDialogUtils
 * @Description 显示数据列表选择数据
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年3月3日 下午2:17:22
 **/
public class MessageDialogUtils {

	/**
	 * @Title showMessageAlertDialog
	 * @Description 显示数据列表选择数据
	 * @author azzbcc Email: azzbcc@sina.com
	 * @date 创建时间 2016年3月3日 上午9:54:21
	 * @param context
	 * @param title
	 * @param dataList
	 * @param msgDialogInterface
	 **/
	public static <T> void showMessageDialog(Context context, String title, List<T> dataList, final MessageDialogInterface<T> msgDialogInterface) {
		Builder alertDialog = new Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		List<String> arguments = new ArrayList<String>();
		for (T data : dataList) {
			arguments.add(msgDialogInterface.getDataItem(data));
		}
		alertDialog.setTitle(title);
		alertDialog.setItems(arguments.toArray(new CharSequence[arguments.size()]), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				msgDialogInterface.onItemClick(which);
			}
		});
		alertDialog.show();
	}
}
