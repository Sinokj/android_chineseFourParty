/** 
 * @Title DiscussionTopic.java
 * @Package cn.sinokj.party.building.bean
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月19日 上午11:45:28
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName DiscussionTopic
 * @Description 民主评议实体类
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月19日 上午11:45:28
 **/
@SuppressWarnings("serial")
public class DiscussionTopic implements Serializable {
	@Expose
	private String vcRegister;
	@Expose
	private String vcExamGroupName;
	@Expose
	private Date dtBegin;
	@Expose
	private List<DiscussionGread> beGreadPerson;
	@Expose
	private int nTotalScore;
	@Expose
	private Date dtReg;
	@Expose
	private String vcTitle;
	@Expose
	private List<DiscussionChoose> choices;
	@Expose
	private int nStatus;
	@Expose
	private String vcExamGroupId;
	@Expose
	private Date dtEnd;
	@Expose
	private int nId;

	public String getVcRegister() {
		return vcRegister;
	}

	public void setVcRegister(String vcRegister) {
		this.vcRegister = vcRegister;
	}

	public String getVcExamGroupName() {
		return vcExamGroupName;
	}

	public void setVcExamGroupName(String vcExamGroupName) {
		this.vcExamGroupName = vcExamGroupName;
	}

	public Date getDtBegin() {
		return dtBegin;
	}

	public void setDtBegin(Date dtBegin) {
		this.dtBegin = dtBegin;
	}

	public List<DiscussionGread> getBeGreadPerson() {
		return beGreadPerson;
	}

	public void setBeGreadPerson(List<DiscussionGread> beGreadPerson) {
		this.beGreadPerson = beGreadPerson;
	}

	public int getnTotalScore() {
		return nTotalScore;
	}

	public void setnTotalScore(int nTotalScore) {
		this.nTotalScore = nTotalScore;
	}

	public Date getDtReg() {
		return dtReg;
	}

	public void setDtReg(Date dtReg) {
		this.dtReg = dtReg;
	}

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public List<DiscussionChoose> getChoices() {
		return choices;
	}

	public void setChoices(List<DiscussionChoose> choices) {
		this.choices = choices;
	}

	public int getnStatus() {
		return nStatus;
	}

	public void setnStatus(int nStatus) {
		this.nStatus = nStatus;
	}

	public String getVcExamGroupId() {
		return vcExamGroupId;
	}

	public void setVcExamGroupId(String vcExamGroupId) {
		this.vcExamGroupId = vcExamGroupId;
	}

	public Date getDtEnd() {
		return dtEnd;
	}

	public void setDtEnd(Date dtEnd) {
		this.dtEnd = dtEnd;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	@Override
	public String toString() {
		return "DiscussionTopic [vcRegister = " + vcRegister
				+ ", vcExamGroupName = " + vcExamGroupName + ", dtBegin = "
				+ dtBegin + ", beGreadPerson = " + beGreadPerson
				+ ", nTotalScore = " + nTotalScore + ", dtReg = " + dtReg
				+ ", vcTitle = " + vcTitle + ", choices = " + choices
				+ ", nStatus = " + nStatus + ", vcExamGroupId = "
				+ vcExamGroupId + ", dtEnd = " + dtEnd + ", nId = " + nId + "]";
	}
}
