package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;

import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;

/**
 * Created by l on 2018/5/2.
 */

public class PublishSuggestionActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_suggestion);
    }
}
