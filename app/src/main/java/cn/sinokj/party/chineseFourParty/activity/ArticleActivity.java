package cn.sinokj.party.chineseFourParty.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.ArticleAdapter;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


public class ArticleActivity extends BaseActivity {
	@BindView(R.id.title)
	public TextView titleText;
	@BindView(R.id.topbar_left_img)
	public View topLeft;
	@BindView(R.id.article_list_view)
	public ListView articleListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.article);
		ButterKnife.bind(this);
		titleText.setText("三会一课");
		topLeft.setVisibility(View.VISIBLE);
		titleText.setVisibility(View.VISIBLE);
		
		DialogShow.showRoundProcessDialog(this);
		new Thread(new LoadDataThread()).start();
	}
	
	@OnClick(R.id.topbar_left_img)
	public void OnClick(View view) {
		finish();
	}

	@Override
	protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
		return HttpDataService.getPartyTypeList();
	}

	@Override
	protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
		JSONArray data = jsonObject.optJSONArray(HttpConstants.OBJECTS);
		List<String> list = new Gson().fromJson(data.toString(),
				new TypeToken<List<String>>() {}.getType());
		DialogShow.closeDialog();
		articleListView.setAdapter(new ArticleAdapter(this, list));
	}
}
