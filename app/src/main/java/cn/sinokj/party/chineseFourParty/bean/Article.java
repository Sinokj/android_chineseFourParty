package cn.sinokj.party.chineseFourParty.bean;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Article implements Serializable {
	@Expose
	private Date dtReg;
	@Expose
	private String vcTitle;
	@Expose
	private int nStatus;
	@Expose
	private String vcType;
	@Expose
	private String vcDeptName;
	@Expose
	private int nId;

	public Date getDtReg() {
		return dtReg;
	}

	public void setDtReg(Date dtReg) {
		this.dtReg = dtReg;
	}

	public String getVcTitle() {
		return vcTitle;
	}

	public void setVcTitle(String vcTitle) {
		this.vcTitle = vcTitle;
	}

	public int getnStatus() {
		return nStatus;
	}

	public void setnStatus(int nStatus) {
		this.nStatus = nStatus;
	}

	public String getVcType() {
		return vcType;
	}

	public void setVcType(String vcType) {
		this.vcType = vcType;
	}

	public String getVcDeptName() {
		return vcDeptName;
	}

	public void setVcDeptName(String vcDeptName) {
		this.vcDeptName = vcDeptName;
	}

	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}
}
