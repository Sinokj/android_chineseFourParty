/** 
 * @Title ExamListActivity.java
 * @Package cn.sinokj.party.building.activity
 * @Description TODO(用一句话描述该文件做什么)
 * @author azzbcc Email: azzbcc@sina.com  
 * @date 创建时间 2016年7月13日 上午10:14:34
 * @version V1.0  
 **/
package cn.sinokj.party.chineseFourParty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sinokj.party.chineseFourParty.R;
import cn.sinokj.party.chineseFourParty.activity.base.BaseActivity;
import cn.sinokj.party.chineseFourParty.adapter.ExamListAdapter;
import cn.sinokj.party.chineseFourParty.adapter.TopicListAdapter;
import cn.sinokj.party.chineseFourParty.app.App;
import cn.sinokj.party.chineseFourParty.bean.ExamTopic;
import cn.sinokj.party.chineseFourParty.bean.TopicListBean;
import cn.sinokj.party.chineseFourParty.service.HttpConstants;
import cn.sinokj.party.chineseFourParty.service.HttpDataService;
import cn.sinokj.party.chineseFourParty.view.dialog.DialogShow;


/**
 * @ClassName ExamListActivity
 * @Description 考试列表
 * @author azzbcc Email: azzbcc@sina.com
 * @date 创建时间 2016年7月13日 上午10:14:34
 **/
public class ExamListActivity extends BaseActivity {
    private static final int ITEM_CLICK = 1;
    @BindView(R.id.title)
    public TextView titleText;
    @BindView(R.id.topbar_left_img)
    public ImageButton topLeftImage;
    @BindView(R.id.exam_list_listview)
    public ListView examListView;

    private String functionType;
    //区分是答题还是问卷调差
    private int mType;
    private ExamListAdapter examListAdapter;
    private TopicListAdapter topicListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_list);
        ButterKnife.bind(this);
        functionType = getIntent().getStringExtra("functionType");
        mType = getIntent().getIntExtra("type", -1);
        titleText.setText(functionType);
        titleText.setVisibility(View.VISIBLE);
        topLeftImage.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        DialogShow.showRoundProcessDialog(this);
        new Thread(new LoadDataThread()).start();
        super.onResume();
    }

    @OnClick({R.id.topbar_left_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topbar_left_img:
                finish();
                break;
        }
    }

    @Override
    protected Map<String, JSONObject> getDataFunction(int what, int arg1, int arg2, Object obj) {
        switch (mType) {
            case 2:
                return HttpDataService.getTopics(functionType, App.nCommitteeId);
            case 4:
                return HttpDataService.getTopicsList();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void httpHandlerResultData(Message msg, JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.optJSONArray(HttpConstants.OBJECTS);
        switch (mType) {
            case 2:
                List<ExamTopic> examTopics = new Gson().fromJson(jsonArray.toString(),
                        new TypeToken<List<ExamTopic>>() {
                        }.getType());
                examListAdapter = new ExamListAdapter(this, examTopics);
                examListView.setAdapter(examListAdapter);

                DialogShow.closeDialog();
                if (examTopics.size() == 0) {
                    Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
                }
                break;
            case 4:
                final TopicListBean topicListBean = new Gson().fromJson(jsonObject.toString(), TopicListBean.class);
                topicListAdapter = new TopicListAdapter(this, topicListBean.objects);
                examListView.setAdapter(topicListAdapter);
                examListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(ExamListActivity.this, IndexImageWebActivity.class);
                        String url = HttpConstants.GETTOPICS_CONTENT + "?nId=" + topicListBean.objects.get(position).nId;
                        intent.putExtra("url", url);
                        intent.putExtra("title", functionType);
                        startActivity(intent);
                    }
                });
                DialogShow.closeDialog();
                if (topicListBean.objects.size() == 0) {
                    Toast.makeText(this, "没有查到相关信息", Toast.LENGTH_SHORT).show();
                }
                break;

		}
	}
}
